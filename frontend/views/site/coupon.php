<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 16.11.17
 * Time: 3:05
 *
 * @var $this yii\web\View
 * @var $offer \common\models\entity\offer\Offer
 * @var string $seoH1
 */
?>

<div class="row">
    <h1 class="mb0"><?= $seoH1 ?></h1>

    <?= $offer->description ?>
</div>


