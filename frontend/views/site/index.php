<?php

/**
 * @var $this yii\web\View
 * @var $tag \common\models\entity\tag\Tag
 * @var $city \common\models\entity\city\City
 * @var string $seoH1
 * @var string $seoSubHeader
 * @var string $searchResult
 */

$asset = \frontend\assets\KuponachAsset::register($this);

?>

<div class="row">
    <h1 class="mb0"><?= $seoH1 ?></h1>
    <strong class="text-muted">
        <?= $seoSubHeader ?>
    </strong>
</div>

<?= $searchResult ?>

<div class="gap"></div>

<script>
    dataLayerYandex.push({
        'ecommerce': {
            'detail': {
                'products': [
                    {
                        'id': '<?=$tag->slug?>',
                        'name': '<?=$tag->name?>',
                        'brand': '<?=$tag->name?>',
                        'category': '<?=$city->name?>',
                        'variant': '<?=$city->name?>'
                    }
                ]
            }
        }
    });
</script>

