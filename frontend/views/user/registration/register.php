<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\helpers\Url;

/**
 * @var $this yii\web\View
 * @var $registrationForm \frontend\models\forms\user\RegistrationForm
 * @var $module dektrium\user\Module
 */

$this->title = Yii::t('user', 'Sign up');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="panel-body">
                <?php $form = ActiveForm::begin([
                    'id' => 'registration-form',
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => false,
                ]); ?>

                <h2>Тип аккаунта</h2>

                <div class="js-registerType">
                    <?= $form->field($registrationForm, 'registerType')->radioList($registrationForm->getRegisterType()) ?>
                </div>

                <h2>Личные данные</h2>
                <?= $form->field($registrationForm, 'name') ?>

                <div class="hidden js-surname">
                    <?= $form->field($registrationForm, 'surname')->textInput() ?>
                </div>

                <?= $form->field($registrationForm, 'email') ?>

                <?= $form->field($registrationForm, 'promocode') ?>

                <p>
                    Нажимая кнопку «Зарегистрироваться», я принимаю условия <a href="<?= Url::to(['/policy/terms']) ?>">Пользовательского соглашения</a>
                    и условия <a href="<?= Url::to(['/policy/confidential']) ?>">Политики конфиденциальности</a>.
                </p>

                <?= Html::submitButton(Yii::t('user', 'Sign up'), ['class' => 'btn btn-success btn-block']) ?>

                <?php $form::end(); ?>
            </div>
        </div>
        <p class="text-center">
            <?= Html::a(Yii::t('user', 'Already registered? Sign in!'), ['/user/security/login']) ?>
        </p>
    </div>
</div>
