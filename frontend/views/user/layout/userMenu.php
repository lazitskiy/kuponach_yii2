<?php

use \yii\widgets\Menu;

/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 07.08.17
 * Time: 23:26
 * @var $user common\models\entity\user\User
 */

?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">
            <?= $user->profile->name ?>
        </h3>
    </div>
    <div class="panel-body">
        <?= Menu::widget([
            'options' => [
                'class' => 'nav nav-pills nav-stacked',
            ],
            'items' => [
                ['label' => Yii::t('user', 'Profile'), 'url' => ['/user/settings/profile']],
                ['label' => Yii::t('user', 'Account'), 'url' => ['/user/settings/account']],
            ],
        ]) ?>
    </div>

    <div class="panel-heading">
        <h3 class="panel-title">
            Услуги
        </h3>
    </div>
    <div class="panel-body">
        <?= Menu::widget([
            'options' => [
                'class' => 'nav nav-pills nav-stacked',
            ],
            'items' => [
                ['label' => 'Задать вопрос', 'url' => ['/get/question']],
            ],
        ]) ?>
    </div>

    <div class="panel-heading">
        <h3 class="panel-title">
            Архив
        </h3>
    </div>
    <div class="panel-body">
        <?= Menu::widget([
            'options' => [
                'class' => 'nav nav-pills nav-stacked',
            ],
            'items' => [
                ['label' => 'Заданные  вопросы', 'url' => ['/user/question/index']],
            ],
        ]) ?>
    </div>
</div>
