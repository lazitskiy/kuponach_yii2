<?php
/**
 * @var $this \yii\web\View
 * @var $content string
 */

use \yii\helpers\Html;
use \yii\widgets\Breadcrumbs;
use \frontend\assets\FrontendAsset;
use \common\widgets\Alert;
use \yii\helpers\Url;

$app = \Yii::$app;
$asset = FrontendAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>

    <title><?= Html::encode($this->title) ?></title>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="<?= $asset->baseUrl ?>/img/favicon/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="<?= $asset->baseUrl ?>/img/favicon/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= $asset->baseUrl ?>/img/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= $asset->baseUrl ?>/img/favicon/apple-touch-icon-114x114.png">
    <?= Html::csrfMetaTags() ?>
    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#000">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#000">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#000">
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div>

    <div class="top-line">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 col-xs-8">
                    <form class="form-search" action="#">
                        <input type="search" name="search" placeholder="Поиск по названию">
                        <span class="icon search"></span>
                    </form>
                </div>
                <div class="col-xs-7 hidden-xs">
                    <a href="#" class="punkt">
                        <span class="icon thumb-up"></span>
                        Популярные
                    </a>
                    <a href="#" class="punkt">
                        <span class="icon geschenk"></span>
                        Бесплатные купоны
                    </a>
                    <a href="#" class="punkt">
                        <span class="icon on-map"></span>
                        На карте
                    </a>
                </div>
                <div class="col-xs-2 col-xs-offset-2 col-sm-offset-0">
                    <a href="#" class="punkt login">
                        <span class="icon log-in"></span>
                        Вход или регистрация
                    </a>
                </div>
            </div>
        </div>
    </div>

    <?= $this->render('_header', [
        'asset' => $asset,
    ]) ?>

    <?= $content ?>

    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-4 col-xs-12">
                    <div class="logo">
                        <img src="<?= $asset->baseUrl ?>/img/logo-foot.png" alt="logo">
                    </div>
                </div>
                <div class="col-md-9 col-sm-8 col-xs-12">
                    <nav class="footer-nav">
                        <ul>
                            <li><a href="#">Туры и отели<span class="badge">20</span></a></li>
                            <li><a href="#">Аквапарки<span class="badge">2</span></a></li>
                            <li><a href="#">Прогулка на теплоходе</a></li>
                            <li><a href="#">Мореон<span class="badge">120</span></a></li>
                            <li><a href="#">Отдых в подмосковье<span class="badge">20</span></a></li>
                            <li><a href="#">Аквапарк карибия</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </footer>

    <div class="overlay"></div>
    <?= $this->render('_popup', [
        'asset' => $asset,
    ]) ?>

    <div>
        <div>
            <?php if ($app->getUser() && $app->getUser()->getIsGuest()): ?>
                <a href="<?= Url::to(['/user/security/login']) ?>">Вход</a>
            <?php else: ?>
                <a href="<?= Url::to(['/user']) ?>">Кабинет</a>
                <a href="<?= Url::to(['/user/security/logout']) ?>" data-method="post">Выйти</a>
            <?php endif; ?>
        </div>
    </div>

    <div>
        <ul>
            <li><a href="<?= Url::to(['/get/question']) ?>">Задать вопрос</a></li>
            <li><a href="">Заказать звонок</a></li>
        </ul>
    </div>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
    </div>
</div>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
