<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 27.09.17
 * Time: 0:03
 *
 * @var $this \yii\web\View
 * @var $asset \frontend\assets\FrontendAsset
 * @var $context \frontend\controllers\base\BaseSiteController
 */
$context = $this->context;
$cityData = $context->getCitiesAsAlphabet();
$cityCurrent = $context->getCityCurrent();

?>

<header class="header">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-4 col-xs-12">
                <a href="/" class="logo">
                    <img src="<?= $asset->baseUrl ?>/img/logo.png" alt="logo">
                </a>
                <a href="#" class="city"><?= $cityCurrent->name ?></a>
            </div>
            <div class="col-md-7 col-xs-6 hidden-xs">
                <nav class="nav">
                    <ul>
                        <li>
                            <a href="#">В вашем городе <span class="arrow"></span></a>
                            <div class="pop-menu">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="pop-block">
                                                <div class="punkts">
                                                    <ul>
                                                        <li data-punkt-id="1" class="active">Все</li>
                                                        <li data-punkt-id="2">Не все</li>
                                                    </ul>
                                                </div>
                                                <div class="pop-menu-content active" data-punkt-id="1">
                                                    Все контент
                                                </div>
                                                <div class="pop-menu-content" data-punkt-id="2">
                                                    не Все контент
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <a href="#">Путешествия <span class="arrow"></span></a>
                            <div class="pop-menu">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="pop-block">
                                                <div class="punkts">
                                                    <ul>
                                                        <li data-punkt-id="1" class="active">Все</li>
                                                        <li data-punkt-id="2">Поездки дешевле 1000</li>
                                                        <li data-punkt-id="3">Путешествуем по России</li>
                                                    </ul>
                                                </div>
                                                <div class="pop-menu-content active" data-punkt-id="1">
                                                    <div class="column">
                                                        <header>Россия</header>
                                                        <ul>
                                                            <li><a href="#">Москва и подмосковье</a></li>
                                                            <li><a href="#">Санкт-Петербург</a></li>
                                                            <li><a href="#">Золотое кольцо</a></li>
                                                            <li><a href="#">Карелия</a></li>
                                                            <li><a href="#">Алтай</a></li>
                                                            <li><a href="#">Урал</a></li>
                                                            <li><a href="#">Байкал</a></li>
                                                            <li><a href="#">Адыгея (Лаго-Наки)</a></li>
                                                            <li><a href="#">Поволжье</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="column">
                                                        <header>Черное море</header>
                                                        <ul>
                                                            <li><a href="#">Сочи</a></li>
                                                            <li><a href="#">Анапа</a></li>
                                                            <li><a href="#">Геленджик</a></li>
                                                            <li><a href="#">Крым</a></li>
                                                            <li><a href="#">Любой город</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="column">
                                                        <header>Туры за границу</header>
                                                        <ul>
                                                            <li><a href="#">Испания</a></li>
                                                            <li><a href="#">Греция</a></li>
                                                            <li><a href="#">Италия</a></li>
                                                            <li><a href="#">Белоруссия</a></li>
                                                            <li><a href="#">Армения</a></li>
                                                            <li><a href="#">Прага</a></li>
                                                            <li><a href="#">Черногория</a></li>
                                                            <li><a href="#">Болгария</a></li>
                                                            <li><a href="#">Прибалтика</a></li>
                                                            <li><a href="#">Виза шенген</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="column">
                                                        <header>По типу</header>
                                                        <ul>
                                                            <li><a href="#">Отдых на море</a></li>
                                                            <li><a href="#">Экскурсии</a></li>
                                                            <li><a href="#">Загородный отдых</a></li>
                                                            <li><a href="#">Конная прогулка</a></li>
                                                            <li><a href="#">Парк-отели</a></li>
                                                            <li><a href="#">Автобусная экскурсия</a></li>
                                                            <li><a href="#">Аренда коттеджей</a></li>
                                                            <li><a href="#">Рыбалка</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="pop-menu-content" data-punkt-id="2">
                                                    Поездки дешевле 1000
                                                    контект
                                                </div>
                                                <div class="pop-menu-content" data-punkt-id="3">
                                                    Путешествуем по России
                                                    контект
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <a href="#">Супермаркеты <span class="arrow"></span></a>
                            <div class="pop-menu">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="pop-block">
                                                <div class="punkts">
                                                    <ul>
                                                        <li data-punkt-id="1" class="active">Все</li>
                                                        <li data-punkt-id="2">Не все</li>
                                                    </ul>
                                                </div>
                                                <div class="pop-menu-content active" data-punkt-id="1">
                                                    Все контент
                                                </div>
                                                <div class="pop-menu-content" data-punkt-id="2">
                                                    не Все контент
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="col-sm-2 col-xs-12">
                <a href="#" class="add">Разместить купон</a>
            </div>
        </div>
    </div>

    <div class="pop-menu pop-city <!--active-->">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="pop-block">
                        <div class="punkts">
                            <ul>
                                <li data-punkt-id="1" class="active"><img src="<?= $asset->baseUrl ?>/img/flag1.jpg" alt="flag" class="flag">Россия</li>
                                <li data-punkt-id="2"><img src="<?= $asset->baseUrl ?>/img/flag2.jpg" alt="flag" class="flag">Украина</li>
                                <li data-punkt-id="3"><img src="<?= $asset->baseUrl ?>/img/flag3.jpg" alt="flag" class="flag">Беларусь</li>
                                <li data-punkt-id="4"><img src="<?= $asset->baseUrl ?>/img/flag4.jpg" alt="flag" class="flag">Казахстан</li>
                            </ul>
                        </div>
                        <div class="pop-menu-content active" data-punkt-id="1">

                            <div class="col-xs-12">
                                <?php foreach ($cityData as $title => $cities) : ?>
                                    <strong><?= $title ?></strong>
                                    <div>
                                        <?php foreach ($cities as $city) : /** @var \common\models\entity\city\City $city */ ?>
                                            <a href="<?= $city->getUrl() ?>"><?= $city->name ?></a>
                                        <?php endforeach; ?>
                                    </div>
                                <?php endforeach; ?>
                            </div>

                        </div>
                        <div class="pop-menu-content" data-punkt-id="2">
                            Города Украины
                        </div>
                        <div class="pop-menu-content" data-punkt-id="3">
                            Города Беларуси
                        </div>
                        <div class="pop-menu-content" data-punkt-id="4">
                            Города Казахстана
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</header>
