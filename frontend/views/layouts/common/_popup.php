<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 27.09.17
 * Time: 0:03
 *
 * @var $this \yii\web\View
 * @var $asset \frontend\assets\FrontendAsset
 */
?>

<div class="pop-time-1">
    <div class="block">
        <span class="icon close"></span>
        <div class="cont">
            <header>Все купоны со скидкой на одном сайте</header>
            <div class="desc">Получайте больше купонов каждый день</div>
            <form action="#">
                <input type="email" placeholder="Укажите ваш e-mail">
                <div class="styled-select slate">
                    <select name="city" style='font-size: 14px; color:#999;font-family: "Helvetica", Sans-Serif;'>
                        <option>Москва</option>
                        <option value="">Город1</option>
                        <option value="">Город2</option>
                        <option value="">Город3</option>
                        <option value="">Город4</option>
                        <option value="">Город5</option>
                        <option value="">Город6</option>
                        <option value="">Город7</option>
                        <option value="">Город8</option>
                        <option value="">Город9</option>
                        <option value="">Город10</option>
                    </select>
                </div>
                <input type="submit" value="Подписаться">
            </form>
            <div class="signin">
                Уже зарегистрированы?
                <a href="#">Войдите на сайт</a>
            </div>
        </div>

    </div>
</div>

<div class="pop-time-2">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-8 col-xs-12">
                <div class="block">
                    <span class="icon close-small"></span>
                    <span class="icon fire"></span>
                    <div class="text">Вы не против подписаться на <span class="red"><strong>важные новости</strong></span> от Kuponach.ru?</div>
                    <a href="#" class="btn">Нет, не видел</a>

                </div>
            </div>
        </div>
    </div>
</div>
