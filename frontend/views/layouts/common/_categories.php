<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 27.09.17
 * Time: 0:03
 *
 * @var $this \yii\web\View
 * @var $context \frontend\controllers\base\BaseSiteController
 */
$context = $this->context;
$tags = $context->getTagsOnMain();

?>

<aside class="sidebar-left">

    <ul class="nav nav-tabs nav-stacked nav-coupon-category nav-coupon-category-left">
        <?php foreach ($tags as $tag): ?>
            <li <?php if ($tag->isParentCurrent()): ?>class="active"<?php endif; ?>>
                <a href="<?= $tag->getUrl() ?>" title="<?= $tag->getUrlTitle() ?>">
                    <i class="fa <?= $tag->getIcon() ?>"></i><?= $tag->getNameUcFirst() ?><span><?= $tag->getOfferCount() ?></span>
                </a>

                <ul class="submenu">
                    <?php foreach ($tag->tagChildren as $tagChildren): ?>
                        <li><?php if ($tagChildren->isCurrent()): ?> - <?php endif; ?>
                            <a href="<?= $tagChildren->getUrl() ?>" title="<?= $tagChildren->getUrlTitle() ?>">
                                <?= $tagChildren->getNameUcFirst() ?><span><?= $tagChildren->getOfferCount() ?></span>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </li>
        <?php endforeach; ?>
    </ul>

    <div class="sidebar-box">
        <h5>Подобрать по цене</h5>
        <a href="<?= \yii\helpers\Url::current(['nedorogo' => 'nedorogo', 'besplatno' => null]) ?>">До <?= \common\models\entity\offer\Offer::PRICE_NEDOROGO ?> рублей</a><br/>
        <a href="<?= \yii\helpers\Url::current(['besplatno' => 'besplatno', 'nedorogo' => null]) ?>">Бесплатно</a><br/>
    </div>
</aside>
