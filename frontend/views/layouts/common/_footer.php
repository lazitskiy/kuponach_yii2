<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 27.09.17
 * Time: 0:03
 *
 * @var $this \yii\web\View
 * @var $asset \frontend\assets\FrontendAsset
 * @var $context \frontend\controllers\base\BaseSiteController
 */
$context = $this->context;
$tags = $context->getTagsOnMain();

?>

<div class="footer public-footer">
    <div class="container">

        <?php foreach ($tags as $tag): ?>
            <div class="c-list">
                <a href="<?= $tag->getUrl() ?>" title="<?= $tag->getUrlTitle() ?>"><?= $tag->getNameUcFirst() ?></a>:
                <?php foreach ($tag->tagChildren as $tagChildren): ?>
                    <a href="<?= $tagChildren->getUrl() ?>" title="<?= $tagChildren->getUrlTitle() ?>">
                        <?= $tagChildren->getNameUcFirst() ?>
                    </a>
                <?php endforeach; ?>
            </div>
        <?php endforeach; ?>
        <hr>
        <div id="omega-section">
            <ul class="copyright-footer">
                <li class="inline-block preserve-text margin-zero">© <?= date('Y'); ?> kuponach.ru. Все права защищены.</li>
            </ul>

            <ul class="social-links-footer">
                <li class="inline-block">
                    <!-- Yandex.Metrika informer -->
                    <a href="https://metrika.yandex.ru/stat/?id=46437063&amp;from=informer" target="_blank" rel="nofollow">
                        <img src="https://informer.yandex.ru/informer/46437063/3_1_FFFFFFFF_EFEFEFFF_0_pageviews"
                             style="width:88px; height:31px; border:0;"
                             alt="Яндекс.Метрика"
                             title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" class="ym-advanced-informer" data-cid="46437063" data-lang="ru"/></a>
                    <!-- /Yandex.Metrika informer -->
                </li>
                <li class="inline-block">
                    <!--LiveInternet counter-->
                    <script type="text/javascript">document.write("<a href='//www.liveinternet.ru/click' " + "target=_blank><img src='//counter.yadro.ru/hit?t14.5;r" + escape(document.referrer) + ((typeof(screen) == "undefined") ? "" : ";s" + screen.width + "*" + screen.height + "*" + (screen.colorDepth ? screen.colorDepth : screen.pixelDepth)) + ";u" + escape(document.URL) + ";" + Math.random() + "' alt='' title='LiveInternet: показано число просмотров за 24" + " часа, посетителей за 24 часа и за сегодня' " + "border='0' width='88' height='31'><\/a>")</script>
                    <!--/LiveInternet-->
                </li>
            </ul>
        </div>
    </div>
</div>
