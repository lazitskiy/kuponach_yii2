<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 27.09.17
 * Time: 0:03
 *
 * @var $this \yii\web\View
 * @var $asset \frontend\assets\FrontendAsset
 * @var $context \frontend\controllers\base\BaseSiteController
 */
$context = $this->context;
$cityData = $context->getCitiesAsAlphabet();
$cityCurrent = $context->getCityCurrent();
$tags = $context->getTagsOnMain();
?>


<div class="container-fluid" id="top-nav">
    <div class="col-xs-12 visible-md visible-sm visible-xs padding-zero">
        <a class="hamburger-menu" href="#"><span></span></a>
        <div class="logo">
            <a href="/">
                <img class=" display-block margin-zero-auto" src="<?= $asset->baseUrl ?>/img/logo-small-m.png" alt="Купонач">Купонач
            </a>
        </div>
    </div>
    <div class="container">
        <div class="row header-navigation">
            <div class="col-xs-12 col-sm-12 hidden-md hidden-sm hidden-xs" id="web-left-nav">
                <nav>
                    <ul>
                        <li>
                            <a href="/">
                                <img class="logo" src="<?= $asset->baseUrl ?>/img/logo-small-m.png" alt="Купонач">Купонач
                            </a>
                        </li>

                        <li class="dropdown inherit">
                            <a class="dropdown-toggle" href="#">
                                <?= $cityCurrent->name ?>
                                <span class="caret"></span>
                            </a>
                            <div class="dropdown-menu wide-menu scrollable-menu">
                                <?php foreach ($cityData as $title => $cities) : ?>
                                    <ul>
                                        <li class="menu-header"><?= $title ?></li>
                                        <li>
                                            <?php foreach ($cities as $city) : /** @var \common\models\entity\city\City $city */ ?>
                                                <a href="<?= $city->getUrl() ?>" title="<?= $city->getUrlTitle() ?>"><?= $city->name ?></a>
                                            <?php endforeach; ?>
                                        </li>
                                    </ul>
                                <?php endforeach; ?>
                            </div>
                        </li>

                        <li class="dropdown inherit">
                            <a class="dropdown-toggle" href="#">
                                Категории
                                <span class="caret"></span>
                            </a>
                            <div class="dropdown-menu wide-menu scrollable-menu">
                                <div class="flex">
                                    <?php foreach ($tags as $tag): ?>
                                        <ul>
                                            <li class="menu-header">
                                                <a href="<?= $tag->getUrl() ?>" title="<?= $tag->getUrlTitle() ?>"><?= $tag->getNameUcFirst() ?></a>
                                            </li>
                                            <?php foreach ($tag->tagChildren as $k => $tagChildren): ?>
                                                <li <?php if ($k > 17): ?> class="hidden"<?php endif; ?>>
                                                    <a role="menuitem" href="<?= $tagChildren->getUrl() ?>" title="<?= $tagChildren->getUrlTitle() ?>"><?= $tagChildren->getNameUcFirst() ?> <span><?= $tagChildren->getOfferCount() ?></span></a>
                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </li>

                        <!--      <li class="dropdown">
                                  <a class="dropdown-toggle" id="dropdownMenu3" data-toggle="dropdown">
                                      Resources
                                      <span class="caret"></span>
                                  </a>
                                  <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu3">
                                      <li><a role="menuitem" tabindex="-1" href="/technographics">Technographics</a></li>
                                      <li><a role="menuitem" tabindex="-1" href="https://resources.datanyze.com/h/c/209684-case-studies" target="_blank">Case Studies</a></li>
                                      <li><a role="menuitem" tabindex="-1" href="https://resources.datanyze.com/" target="_blank">Content Hub</a></li>
                                      <li><a role="menuitem" tabindex="-1" href="/market-share">Market Share</a></li>
                                      <li><a role="menuitem" tabindex="-1" href="/faq">FAQ</a></li>
                                  </ul>
                              </li>-->

                        <!-- <li class="dropdown">
                             <a class="dropdown-toggle" id="dropdownMenu4" data-toggle="dropdown">
                                 Free Tools
                                 <span class="caret"></span>
                             </a>
                             <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu4">
                                 <li><a role="menuitem" tabindex="-1" href="/technographic-analysis">Technographic Analysis</a></li>
                                 <li><a role="menuitem" tabindex="-1" href="/insider">Chrome Extension</a></li>
                                 <li><a role="menuitem" tabindex="-1" href="http://resources.datanyze.com/h/c/209666-webinars-and-ebooks" target="_blank">Webinars/eBooks</a></li>
                             </ul>
                         </li>

                         <li class="dropdown">
                             <a class="dropdown-toggle" id="dropdownMenu5" data-toggle="dropdown">
                                 About
                                 <span class="caret"></span>
                             </a>
                             <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu5">
                                 <li><a role="menuitem" tabindex="-1" href="/our-team">Team</a></li>
                                 <li><a role="menuitem" tabindex="-1" href="/careers">Careers</a></li>
                                 <li><a role="menuitem" tabindex="-1" href="/investors">Investors</a></li>
                                 <li><a role="menuitem" tabindex="-1" href="/press">Press</a></li>
                                 <li><a role="menuitem" tabindex="-1" href="/partners">Partners</a></li>
                             </ul>
                         </li>-->
                    </ul>
                </nav>
            </div>
            <!--<div class="col-sm-4 hidden-md hidden-sm hidden-xs text-right">

                <a href="/login/" class="login-link">Login</a>
                <a class="btn btn-teal demo-modal-open" href="#">Free demo</a>

            </div>-->
        </div>
    </div>
</div>
<div class="background-mask"></div>
<div class="mobile-menu">
    <div class="mobile-menu-header">
        <a class="hamburger-menu closed" href="#"><span></span></a>
        <p id="mobile-menu-label">Меню</p>
    </div>
    <div class="menu-container">
        <?php foreach ($tags as $tag): ?>
            <div class="mobile-menu-panel">
                <a class="panel-heading" role="button" data-toggle="collapse" href="#heading<?= $tag->id ?>" aria-controls="heading<?= $tag->id ?>">
                    <h4 class="panel-title"><?= $tag->getNameUcFirst() ?></h4>
                </a>
                <div id="heading<?= $tag->id ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?= $tag->id ?>">
                    <ul class="no-list-style site-map-ul">
                        <?php foreach ($tag->tagChildren as $tagChildren): ?>
                            <li><a href="<?= $tagChildren->getUrl() ?>" title="<?= $tagChildren->getUrlTitle() ?>"><?= $tagChildren->getNameUcFirst() ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
