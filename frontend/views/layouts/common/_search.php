<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 27.09.17
 * Time: 0:03
 *
 * @var $this \yii\web\View
 * @var $asset \frontend\assets\FrontendAsset
 * @var $context \frontend\controllers\base\BaseSiteController
 */
$context = $this->context;

?>

<form class="search-area form-group">
    <div class="container">
        <div class="row">
            <div class="col-md-8 clearfix">
                <label><i class="fa fa-search"></i><span>Я хочу найти:</span>
                </label>
                <div class="search-area-division search-area-division-input">
                    <?= \kartik\widgets\Typeahead::widget([
                        'name' => 'search',
                        //'id' => 'cityTypeahead',
                        'value' => 'Пицца',
                        'scrollable' => true,
                        'dataset' => [
                            [
                                'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                                'display' => 'name',
                                'remote' => [
                                    'ttl' => 20,
                                    'url' => \yii\helpers\Url::to(['/suggest/search']) . '?q=%QUERY',
                                    'wildcard' => '%QUERY',
                                ],
                                'limit' => 999,
                            ],
                        ],
                        'pluginOptions' => [
                            'highlight' => true,
                            'minLength' => 2,
                            'val' => '',
                        ],
                        'pluginEvents' => [
                            "typeahead:selected" => "function(obj, item) { location.href='/'+item.slug; }"
                        ],
                    ]); ?>
                </div>
            </div>
            <!--<div class="col-md-3 clearfix">
                <label><i class="fa fa-map-marker"></i><span>В</span>
                </label>
                <div class="search-area-division search-area-division-location">
                    <input class="form-control" type="text" placeholder="Boston"/>
                </div>
            </div>-->
            <!--<div class="col-md-1">
                <button class="btn btn-block btn-white search-btn" type="submit">Искать</button>
            </div>-->
        </div>
    </div>
</form>
