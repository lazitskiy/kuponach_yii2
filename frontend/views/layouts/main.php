<?php
/**
 * @var $this \yii\web\View
 * @var $content string
 * @var $context \frontend\controllers\base\BaseSiteController
 */

use \frontend\assets\KuponachAsset;

$app = \Yii::$app;
$asset = KuponachAsset::register($this);
?>
<?php $this->beginContent('@frontend/views/layouts/base/base.php'); ?>
<div class="global-wrap">
    <?= $this->render('common/_header', [
        'asset' => $asset,
    ]) ?>

    <div class="top-aread">
        <img src="<?= $asset->baseUrl ?>/img/lines/line1.jpg" alt="Купоны на сайте"  style="max-height: 170px;" />
    </div>

    <?= $this->render('common/_search', [
        'asset' => $asset,
    ]) ?>

    <div class="gap"></div>

    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <?= $this->render('common/_categories', [
                    'asset' => $asset,
                ]) ?>
            </div>
            <div class="col-md-9">
                <?= $content ?>
            </div>
        </div>
    </div>

    <?= $this->render('common/_footer', [
        'asset' => $asset,
    ]) ?>

</div>

<?php $this->endContent() ?>
