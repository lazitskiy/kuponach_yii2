<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 29.10.17
 * Time: 11:58
 *
 * @var $this \yii\web\View
 * @var $offer  \common\models\entity\offer\Offer
 * @var $click  \common\models\entity\click\Click
 */
$this->title = 'Купонач - все купоны, акции и скидки у тебя в городе';
$this->registerCssFile('@web/media/css/out/styles_out.css');
$this->registerJsFile('@web/media/js/out/out.js');
?>
<div id="frame-top">
    <div style="padding: 10px 0;">

    </div>
</div>

<!--LiveInternet counter-->
<script type="text/javascript">document.write("<a href='//www.liveinternet.ru/click' " + "target=_blank><img src='//counter.yadro.ru/hit?t14.5;r" + escape(document.referrer) + ((typeof(screen) == "undefined") ? "" : ";s" + screen.width + "*" + screen.height + "*" + (screen.colorDepth ? screen.colorDepth : screen.pixelDepth)) + ";u" + escape(document.URL) + ";" + Math.random() + "' alt='' title='' " + "border='0' width='1' height='1'><\/a>")</script>
<!--/LiveInternet-->

<script>
    var items = [{
        'id': 'P<?=$click->id?>',
        'name': '<?=$offer->operator->slug?>',
        'price': '<?=$offer->operator->cost?>',
        'quantity': '1'
    }];

    gtag('event', 'conversion', {
        'send_to': 'AW-850114947/QzFrCMDCungQg_OulQM',
        'affiliation': 'kuponach.ru',
        'transaction_id': 'T<?=$click->id?>',
        'value': '<?=$offer->operator->cost?>',
        'currency': 'RUB',
        'items': items
    });

    gtag('event', 'purchase', {
        'send_to': 'AW-850114947/QzFrCMDCungQg_OulQM',
        'affiliation': 'kuponach.ru',
        'transaction_id': 'T<?=$click->id?>',
        'value': '<?=$offer->operator->cost?>',
        'currency': 'RUB',
        'items': items
    });

    dataLayerYandex.push({
        "ecommerce": {
            "purchase": {
                "actionField": {
                    "id": 'T<?=$click->id?>',
                    "goal_id": "34573326"
                },
                "products": items
            }
        }
    });
</script>


<noindex>
    <iframe src="<?= $offer->getOfferLink() ?>" id="frame-out" rel="nofollow" frameborder="0"></iframe>
</noindex>
