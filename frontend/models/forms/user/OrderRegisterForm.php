<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 05.08.17
 * Time: 10:24
 */

namespace frontend\models\forms\user;


use common\models\entity\city\City;
use common\models\entity\user\User;
use common\traits\base\ApplicationAwareTrait;
use common\traits\base\UserAwareTrait;
use common\traits\GeoServiceAwareTrait;
use common\traits\UserServiceAwareTrait;
use dektrium\user\helpers\Password;
use frontend\models\forms\BaseForm;


/**
 * Это форма при заказе запроса или звонка. Отличается от обычной формы регистрации
 *
 * Class OrderRegisterForm
 * @package frontend\models\forms\user
 */
class OrderRegisterForm extends BaseForm
{
    use ApplicationAwareTrait;
    use UserAwareTrait;
    use UserServiceAwareTrait;
    use GeoServiceAwareTrait;

    const SCENARIO_USER_AUTHORIZED = 'user_authorized';
    const SCENARIO_USER_NEW = 'user_new';

    public $name;
    public $email;
    public $password;
    public $phone;

    /**
     * @var User
     */
    protected $user;

    public function init()
    {
        parent::init();

        $this->user = $this->getUserTrait()->getIdentity();
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * Не удалять требуется работы
     * @param array $data
     * @param null $formName
     * @return bool
     */
    public function load($data, $formName = null)
    {
        if ($this->user) {
            return true;
        } else {
            return parent::load($data, $formName);
        }
    }


    public function rules()
    {
        return [
            ['name', 'required', 'on' => static::SCENARIO_USER_NEW],
            ['name', 'string', 'length' => [2, 256], 'on' => static::SCENARIO_USER_NEW],

            ['email', 'required', 'on' => static::SCENARIO_USER_NEW],
            ['email', 'email', 'on' => static::SCENARIO_USER_NEW],

            ['phone', 'required', 'on' => static::SCENARIO_USER_NEW],
            ['phone', 'match', 'pattern' => '/[\d\s\+\(\)]/', 'on' => static::SCENARIO_USER_NEW],

            ['password', function ($attribute) {
                $password = $this->$attribute;
                $this->user = $this->getUserService()->getByUsernameOrEmail($this->email);
                if ($this->user) {
                    if (!$password) {
                        $this->addError($attribute, 'Заполните пароль');
                    }

                    if (!Password::validate($password, $this->user->password_hash)) {
                        $this->addError($attribute, 'Необходимо указать правильный пароль');
                    }
                }
            }, 'skipOnEmpty' => false, 'on' => static::SCENARIO_USER_NEW],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Ваше имя',
            'email' => 'Электронная почта',
            'password' => 'Пароль',
            'phone' => 'Телефон',
        ];
    }

    public function attributeHints()
    {
        return [
        ];
    }
}
