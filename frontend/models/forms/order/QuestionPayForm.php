<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 05.08.17
 * Time: 1:56
 */

namespace frontend\models\forms\order;


use dektrium\user\models\User;
use frontend\models\forms\BaseForm;
use yii\web\IdentityInterface;

class QuestionPayForm extends BaseForm
{
    const COST_MIN = 400;
    const COST_MAX = 4000;
    const COST_STEP = 100;
    const COST_DEFAULT_VALUE = 600;

    public $cost;
    public $responder_count;
    public $additional_options;

    /**
     * @var User
     */
    protected $user;


    public function init()
    {
        $this->responder_count = 1;

        $this->cost = $this->cost ?: static::COST_DEFAULT_VALUE;
    }

    /**
     * @return User|\common\models\entity\user\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param IdentityInterface $user
     */
    public function setUser(IdentityInterface $user)
    {
        $this->user = $user;
    }

    // Потом
    public function getResponderCountArray()
    {
        return [
            1 => 'Один практик (Беслптано)',
            2 => 'Два практика (+ 100 рублей)',
            3 => 'Три практика (+ 200 рублей)',
        ];
    }

    /**
     * @return mixed
     */
    // Потом
    public function getResponderCountCost()
    {
        $arr = [
            1 => 0,
            2 => 100,
            3 => 200,
        ];

        return $arr[$this->responder_count] ?? $arr[1];
    }

    public function attributeLabels()
    {
        return [
            'cost' => 'Установите стоимость вопроса в соответствии с его сложностью',
            'responder_count' => 'Выберите количество практиков, которые будут отвечать на ваш вопрос',
        ];
    }

    public function attributeHints()
    {
        return [
            //'cost' => 'Мнения нескольких практиков помогут вам избежать риска ошибки.',
            'responder_count' => 'Мнения нескольких практиков помогут вам избежать риска ошибки.',
        ];
    }

    public function rules()
    {
        return [
            ['cost', 'required'],
            ['cost', 'number', 'min' => static::COST_MIN, 'integerOnly' => true],

            ['responder_count', 'required'],
        ];
    }
}
