<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 05.08.17
 * Time: 1:56
 */

namespace frontend\models\forms\order;


use common\services\SettingService;
use common\traits\base\UserAwareTrait;
use common\traits\SettingServiceAwareTrait;
use dektrium\user\models\User;
use frontend\models\forms\BaseForm;
use yii\helpers\ArrayHelper;

class QuestionVariantForm extends BaseForm
{
    use UserAwareTrait;
    use SettingServiceAwareTrait;

    public $variant;

    /**
     * @var User
     */
    protected $user;

    /**
     * @var SettingService;
     */
    protected $settingService;

    /**
     * @var array
     */
    protected $variants;

    public function init()
    {
        $this->variant = 'extended';

        $this->user = $this->getUserTrait()->getIdentity();
        $this->settingService = $this->getSettingService();
        $this->variants = $this->settingService->get($this->settingService::QUESTION_VARIANTS_SYSTEM_NAME);
    }

    public function attributeLabels()
    {
        return [
            'equipment' => 'Укажите тип вопроса',
        ];
    }

    public function attributeHints()
    {
        return [
            'equipment' => 'Тип вопроса подсказка',
        ];
    }

    public function rules()
    {
        return [
            ['variant', 'required'],
            ['variant', 'in', 'range' => function () {
                $variants = $this->settingService->get($this->settingService::QUESTION_VARIANTS_SYSTEM_NAME);

                return ArrayHelper::getColumn($variants, 'systemName', false);
            }],
        ];
    }

    /**
     * @return User|\common\models\entity\user\User
     */
    public function getUser()
    {
        return $this->user;
    }

    public function getVariantStandard()
    {
        return $this->variants['standard'] ?? [];
    }

    public function getVariantExtended()
    {
        return $this->variants['extended'] ?? [];
    }

    /**
     * Это подарочный тариф. В стандартные не входит
     *
     * @return array
     */
    public function getVariantFixCost()
    {
        $fixCostQuestionCount = $this->getUser()->balance['fix_cost_question_count'];
        if (!$fixCostQuestionCount) {
            return [];
        }

        $setting = $this->settingService->get($this->settingService::USER_REGISTER_QUESTION_FIX_PRICE_SYSTEM_NAME);
        if (!$setting) {
            return [];
        }

        return $this->variants['fix_cost'] ?? [];
    }

    public function getChosenVariant()
    {
        return $this->variants[$this->variant];
    }
}
