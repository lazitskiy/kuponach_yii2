<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 13.11.17
 * Time: 15:12
 */

namespace frontend\widgets;


use common\helpers\UrlHelper;
use common\models\entity\city\City;
use common\models\entity\company\CompanyAddressCityArea;
use common\models\entity\company\CompanyAddressCityDistrict;
use common\models\entity\company\CompanyAddressCityMetro;
use common\models\entity\company\CompanyAddressCityStreet;
use common\models\entity\offer\Offer;
use common\models\entity\offer\OfferSphinx;
use common\models\entity\tag\Tag;
use yii\base\Widget;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;

class CouponWidget extends Widget
{
    const OFFER_PAGE_LIMIT = 21;
    const TAG_LIMIT = 21;

    const URL_TYPE_AREA = 'area';
    const URL_TYPE_DISTRICT = 'district';
    const URL_TYPE_STREET = 'street';
    const URL_TYPE_METRO = 'metro';

    public $cityId;

    /**
     * @var Tag
     */
    public $tagCurrent;

    /**
     * @var Tag[]
     */
    public $tags = [];

    /**
     * @var bool
     */
    public $nedorogo;

    /**
     * @var bool
     */
    public $besplatno;

    /**
     * @var array
     */
    public $filters;

    /**
     * @var int
     */
    public $page;

    /**
     * @var Offer[]
     */
    private $offers = [];

    /**
     * @var Offer[]
     */
    private $offersExtra = [];

    /**
     * @var Tag[]
     */
    private $tagsChildren = [];

    /**
     * @var Tag[]
     */
    private $tagsSimilar = [];

    /**
     * @var Tag[]
     */
    private $tagsExtra = [];

    /**
     * @var Pagination
     */
    private $pagination;

    public function init()
    {
        parent::init();

        if (!$this->cityId) {
            $this->cityId = City::CITY_ID_DEFAULT;
        }

        // Все теги <0 не учитывать при поиске, это зарезервированы под другие нужды
        $tagIds = [];
        foreach ($this->tags as $tag) {
            if ($tag->id > 0) {
                $tagIds[] = $tag->id;
            }
        }

        /** @var CompanyAddressCityArea $area */
        $area = $this->filters[CouponWidget::URL_TYPE_AREA]['values'][$this->filters[CouponWidget::URL_TYPE_AREA]['selected']] ?? null;
        $areaId = $area ? $area->id : null;

        /** @var CompanyAddressCityDistrict $district */
        $district = $this->filters[CouponWidget::URL_TYPE_DISTRICT]['values'][$this->filters[CouponWidget::URL_TYPE_DISTRICT]['selected']] ?? null;
        $districtId = $district ? $district->id : null;

        /** @var CompanyAddressCityStreet $street */
        $street = $this->filters[CouponWidget::URL_TYPE_STREET]['values'][$this->filters[CouponWidget::URL_TYPE_STREET]['selected']] ?? null;
        $streetId = $street ? $street->id : null;

        /** @var CompanyAddressCityMetro $metro */
        $metro = $this->filters[CouponWidget::URL_TYPE_METRO]['values'][$this->filters[CouponWidget::URL_TYPE_METRO]['selected']] ?? null;
        $metroId = $metro ? $metro->id : null;

        $limit = static::OFFER_PAGE_LIMIT;
        list($offerIds, $offerTotal, $arTagIdCount, $orderBy) = OfferSphinx::getRepository()
            ->getBy($this->cityId, $tagIds, $this->page, $limit, static::TAG_LIMIT, $this->nedorogo, $this->besplatno, $areaId, $districtId, $streetId, $metroId);

        $this->offers = Offer::getRepository()->getByIds($offerIds, $orderBy);

        // Сопутствующие теги
        if ($arTagIdCount) {
            $this->tagsSimilar = Tag::findAll(['id' => array_keys($arTagIdCount)]);
            ArrayHelper::multisort($this->tagsSimilar, 'offer_count', SORT_DESC);
            $this->tagsSimilar = ArrayHelper::index($this->tagsSimilar, 'id');
        }
        // Вложенные теги
        $tag = $this->tags[0];
        $this->tagsChildren = [];
        $this->tagsChildren = $tag->getTagChildren(['name' => SORT_ASC])->all();

        $this->postSearchAnalyze($tag);

        // Другое
        if (isset($this->tagsSimilar[1107])) {
            unset($this->tagsSimilar[1107]);
        }
        // Главная
        if (isset($this->tagsSimilar[-1])) {
            unset($this->tagsSimilar[-1]);
        }
        // Текущий
        if (isset($this->tagsSimilar[$tag->id])) {
            unset($this->tagsSimilar[$tag->id]);
        }


        $this->pagination = new Pagination([
            'totalCount' => $offerTotal,
            'pageSize' => $limit,
            'defaultPageSize' => $limit,
            'forcePageParam' => false,
        ]);
    }

    public function run()
    {
        parent::run();

        return $this->render('coupon/default', [
            'tagsSimilar' => $this->tagsSimilar,
            'tagsChildren' => $this->tagsChildren,
            'tagsExtra' => $this->tagsExtra,
            'tag' => $this->tagCurrent,
            'offers' => $this->offers,
            'offersExtra' => $this->offersExtra,
            'filters' => $this->filters,
            'pagination' => $this->pagination,
        ]);
    }

    public static function url($urlType, $slug)
    {
        $params = [
            $urlType => $slug,
        ];
        switch ($urlType) {
            case static::URL_TYPE_AREA:
                $params[static::URL_TYPE_DISTRICT] = null;
                $params[static::URL_TYPE_STREET] = null;
                $params[static::URL_TYPE_METRO] = null;
                break;
            case static::URL_TYPE_DISTRICT:
                $params[static::URL_TYPE_AREA] = null;
                $params[static::URL_TYPE_STREET] = null;
                $params[static::URL_TYPE_METRO] = null;
                break;
            case static::URL_TYPE_STREET:
                $params[static::URL_TYPE_AREA] = null;
                $params[static::URL_TYPE_DISTRICT] = null;
                $params[static::URL_TYPE_METRO] = null;
                break;
            case static::URL_TYPE_METRO:
                $params[static::URL_TYPE_AREA] = null;
                $params[static::URL_TYPE_DISTRICT] = null;
                $params[static::URL_TYPE_STREET] = null;

        }

        return UrlHelper::current($params);
    }

    private function postSearchAnalyze(Tag $tag)
    {
        /**
         * Постобработка, чтобы странице не казалась пустой
         */
        if (count($this->offers) <= 6 || count($this->tagsSimilar) < 3) {
            /** @var Tag $tagParent */
            $tagParent = $tag->getTagParents()->one();
            if (!$tagParent) {
                $tagParent = Tag::findOne(Tag::TAG_ID_MAIN_PAGE);
            }

            // Добавим офферов
            if (count($this->offers) <= 6) {
                $tagIds = ArrayHelper::getColumn($tagParent->tagChildren, 'id');

                $limit = static::OFFER_PAGE_LIMIT;
                list($offerIds, $offerTotal, $arTagIdCount, $orderBy) = OfferSphinx::getRepository()
                    ->getBy($this->cityId, $tagIds, $this->page, $limit, static::TAG_LIMIT, $this->nedorogo, $this->besplatno);

                $offerIdsToRemove = ArrayHelper::getColumn($this->offers, 'id');
                $offerIds = array_diff($offerIds, $offerIdsToRemove);
                $this->offersExtra = Offer::getRepository()->getByIds($offerIds);
            }

            // И тегов
            if (count($this->tagsSimilar) < 3) {
                /** @var Tag $tagParent */
                $this->tagsExtra = $this->tagsSimilar;
                $this->tagsExtra[$tagParent->id] = $tagParent;

                if ($tagChildren = $tagParent->getTagChildren()->indexBy('id')->all()) {
                    $this->tagsExtra = $tagChildren;
                }
            }

            // И тегов
            if (count($this->offers) <= 6) {
                /** @var Tag $tagParent */
                $this->tagsSimilar[$tagParent->id] = $tagParent;

                if ($tagChildren = $tagParent->getTagChildren()->indexBy('id')->all()) {
                    $this->tagsSimilar = $this->tagsSimilar + $tagChildren;
                }
            }

        }
    }
}
