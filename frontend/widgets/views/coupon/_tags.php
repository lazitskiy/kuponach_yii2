<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 13.11.17
 * Time: 23:34
 *
 * @var $tags \common\models\entity\tag\Tag[]
 * @var $title string
 */
?>
<aside>
    <div class="sidebar-box">
        <div class="sidebar-title"><?= $title ?></div>
        <?php foreach ($tags as $tag): ?>
            <a href="<?= $tag->getUrl() ?>" title="<?= $tag->getUrlTitle() ?>" style="margin-right: 8px; white-space: nowrap">
                <?= $tag->name ?>
            </a>
        <? endforeach; ?>
    </div>
</aside>
