<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 13.11.17
 * Time: 23:37
 *
 * @var $this \yii\web\View
 * @var $offers \common\models\entity\offer\Offer[]
 */

?>

<div class="row-wrap offers">
<?php foreach ($offers as $i => $offer): ?>
    <div class="col-md-4 col-xs-12 col-sm-6 offer nopadding">
        <a href="<?= $offer->getOutLink() ?>" title="<?= $offer->name ?>" target="_blank">
            <div class="product-thumb">
                <?php $images = $offer->imagesActive ?>
                <header class="product-header">
                    <?php if (isset($images[0])): ?>
                        <img src="<?= $images[0]->getPath() ?>" alt="<?= $offer->name_short ?>"/>
                    <?php endif; ?>
                </header>
                <div class="product-inner">
                    <h5 class="product-title"><?= $offer->name_short ?></h5>
                    <p class="product-desciption"><?= $offer->name ?></p>
                    <div class="product-meta">
                        <span class="product-time"><i class="fa fa-clock-o"></i> <?= $offer->getRemainingText() ?></span>
                        <ul class="product-price-list">
                            <li><span class="product-price"><?= $offer->price_discounted ?> р.</span></li>
                            <?php if ($offer->price_original): ?>
                                <li><span class="product-old-price"><?= $offer->price_original ?></span></li>
                            <?php endif; ?>
                            <li><span class="product-save">Скидка <?= $offer->discount_percent ?>%</span></li>
                        </ul>
                    </div>
                    <!--<p class="product-location"><i class="fa fa-map-marker"></i> Boston</p>-->
                    <div class="tag">
                        <?= $offer->getMainTag() ?>
                    </div>
                </div>
            </div>
        </a>
    </div>
<?php endforeach; ?>
</div>
