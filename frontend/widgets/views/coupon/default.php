<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 13.11.17
 * Time: 15:32
 *
 * @var $this \yii\web\View
 * @var $offers \common\models\entity\offer\Offer[]
 * @var $offersExtra \common\models\entity\offer\Offer[]
 * @var $tag \common\models\entity\tag\Tag
 * @var $tagsSimilar \common\models\entity\tag\Tag[]
 * @var $tagsChildren \common\models\entity\tag\Tag[]
 * @var $tagsExtra \common\models\entity\tag\Tag[]
 * @var \yii\data\Pagination $pagination
 *
 * @var $filters array
 * @var $areas \common\models\entity\company\CompanyAddressCityArea[]
 * @var $districts \common\models\entity\company\CompanyAddressCityDistrict[]
 * @var $streets \common\models\entity\company\CompanyAddressCityStreet[]
 *
 */

use \frontend\widgets\CouponWidget;

?>
<style>
    .sidebar-box ul {
        padding: 0;
    }
</style>
<div class="row">

    <?php if (($areas = $filters[CouponWidget::URL_TYPE_AREA]['values']) && YII_ENV_DEV): ?>
        <div style="height: 100px; overflow-y: scroll;" class="pull-left sidebar-box">
            <div class="sidebar-title">Округ</div>
            <ul>
                <?php foreach ($areas as $area) : ?>
                    <li><a href="<?= CouponWidget::url(CouponWidget::URL_TYPE_AREA, $area->slug) ?>"><?= $area->name_where ?></a></li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>

    <?php if (($districts = $filters[CouponWidget::URL_TYPE_DISTRICT]['values']) && YII_ENV_DEV): ?>
        <div style="height: 100px; overflow-y: scroll;" class="pull-left sidebar-box">
            <div class="sidebar-title">Район</div>
            <ul>
                <?php foreach ($districts as $district) : ?>
                    <li><a href="<?= CouponWidget::url(CouponWidget::URL_TYPE_DISTRICT, $district->slug) ?>"><?= $district->name ?></a></li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>

    <?php if (($streets = $filters[CouponWidget::URL_TYPE_STREET]['values']) && YII_ENV_DEV): ?>
        <div style="height: 100px; max-width: 338px; overflow-y: scroll;" class="pull-left sidebar-box">
            <div class="sidebar-title">Улица</div>
            <ul>
                <?php foreach ($streets as $street) : ?>
                    <li><a href="<?= CouponWidget::url(CouponWidget::URL_TYPE_STREET, $street->slug) ?>"><?= $street->name . ' ' . $street->name_full ?></a></li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>

    <?php if (($metros = $filters[CouponWidget::URL_TYPE_METRO]['values']) && YII_ENV_DEV): ?>
        <div style="height: 100px; overflow-y: scroll;" class="pull-left sidebar-box">
            <div class="sidebar-title">Метро</div>
            <ul>
                <?php foreach ($metros as $metro) : ?>
                    <li><a href="<?= CouponWidget::url(CouponWidget::URL_TYPE_METRO, $metro->slug) ?>"><?= $metro->name ?></a></li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>

    <div class="clearfix"></div>
    <?php if ($tagsSimilar): ?>
        <?= $this->render('_tags', [
            'title' => 'Сопутствующие купоны:',
            'tags' => $tagsSimilar,
        ]); ?>
    <?php endif; ?>

    <?php if ($tagsChildren && $offers): ?>
        <?= $this->render('_tags', [
            'title' => 'Уточнить выбор:',
            'tags' => $tagsChildren,
        ]); ?>
    <?php endif; ?>

    <?php if (!$offers): ?>

        <?php if ($tagsExtra): ?>
            <?= $this->render('_tags', [
                'title' => 'Уточнить выбор:',
                'tags' => $tagsExtra,
            ]); ?>
        <?php endif; ?>
        <?= $tag->getEmptyOfferText() ?>
        <?= $this->render('_offers', [
            'offers' => $offersExtra,
        ]); ?>

    <?php else: ?>

        <?= $this->render('_offers', [
            'offers' => $offers,
        ]); ?>

        <?= $this->render('_offers', [
            'offers' => $offersExtra,
        ]); ?>


    <?php endif; ?>

    <?= \yii\widgets\LinkPager::widget([
        'pagination' => $pagination,
        'registerLinkTags' => true,
        //'disableCurrentPageButton' => true
    ]) ?>

</div>
