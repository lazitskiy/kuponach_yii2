<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'frontend\controllers',
    'bootstrap' => ['log'],
    'modules' => [],
    'components' => [
        'user' => [
            'identityCookie' => [
                'name' => '_frontendIdentity',
                'httpOnly' => true,
            ],
        ],
        'session' => [
            'name' => 'FRONTENDSESSID',
            'cookieParams' => [
                'httpOnly' => true,
                'path' => '/',
            ],
        ],
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/sitemap.xml' => 'sitemap/index',
                '/out/<offerId:\d+>' => 'out/index',
                '/suggest/search' => 'suggest/search',

                '/<slug:[\w\-]+>/<nedorogo:nedorogo>/<area:[\w\-]+>-o' => 'site/index',
                '/<slug:[\w\-]+>/<besplatno:besplatno>/<area:[\w\-]+>-o' => 'site/index',
                '/<slug:[\w\-]+>/<nedorogo:nedorogo>/rayon-<district:[\w\-]+>' => 'site/index',
                '/<slug:[\w\-]+>/<besplatno:besplatno>/rayon-<district:[\w\-]+>' => 'site/index',
                '/<slug:[\w\-]+>/<nedorogo:nedorogo>/ulitsa-<street:[\w\-]+>' => 'site/index',
                '/<slug:[\w\-]+>/<besplatno:besplatno>/ulitsa-<street:[\w\-]+>' => 'site/index',
                '/<slug:[\w\-]+>/<nedorogo:nedorogo>/metro-<metro:[\w\-]+>' => 'site/index',
                '/<slug:[\w\-]+>/<besplatno:besplatno>/metro-<metro:[\w\-]+>' => 'site/index',

                '/<area:[\w\-]+>-o/<nedorogo:nedorogo>' => 'site/index',
                '/<area:[\w\-]+>-o/<besplatno:besplatno>' => 'site/index',
                '/rayon-<district:[\w\-]+>/<nedorogo:nedorogo>' => 'site/index',
                '/rayon-<district:[\w\-]+>/<besplatno:besplatno>' => 'site/index',
                '/ulitsa-<street:[\w\-]+>/<nedorogo:nedorogo>' => 'site/index',
                '/ulitsa-<street:[\w\-]+>/<besplatno:besplatno>' => 'site/index',
                '/metro-<metro:[\w\-]+>/<nedorogo:nedorogo>' => 'site/index',
                '/metro-<metro:[\w\-]+>/<besplatno:besplatno>' => 'site/index',

                '/<slug:[\w\-]+>/<nedorogo:nedorogo>' => 'site/index',
                '/<slug:[\w\-]+>/<besplatno:besplatno>' => 'site/index',

                '/<nedorogo:nedorogo>' => 'site/index',
                '/<besplatno:besplatno>' => 'site/index',

                '/<slug:[\w\-]+>/<area:[\w\-]+>-o' => 'site/index',
                '/<slug:[\w\-]+>/rayon-<district:[\w\-]+>' => 'site/index',
                '/<slug:[\w\-]+>/ulitsa-<street:[\w\-]+>' => 'site/index',
                '/<slug:[\w\-]+>/metro-<metro:[\w\-]+>' => 'site/index',

                '/<area:[\w\-]+>-o' => 'site/index',
                '/rayon-<district:[\w\-]+>' => 'site/index',
                '/ulitsa-<street:[\w\-]+>' => 'site/index',
                '/metro-<metro:[\w\-]+>' => 'site/index',

                '/<slug:[\w\-]+>' => 'site/index',

                '/kupon-na/<slug:[\w\-]+>' => 'site/coupon',
                '/' => 'site/index',
                'test' => 'test/index',
            ],
        ],
        'assetManager' => [
            'class' => \yii\web\AssetManager::class,
            'linkAssets' => false,
            'forceCopy' => true,
            'appendTimestamp' => true,
            //'bundles' => YII_ENV_DEV ? [] : require(__DIR__ . '/asset-prod.php'),
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@dektrium/user/views' => '@frontend/views/user',
                ],
            ],
        ],

        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    ],
    'params' => $params,
];
