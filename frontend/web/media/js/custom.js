(function ($) {
    "use strict";

// Sticky search
    if ($('body').hasClass('sticky-search')) {
        var theLoc = $('.search-area').position().top;
        if ($('body').hasClass('sticky-header')) {
            var header_h = $('header.main').outerHeight();
        } else {
            header_h = 0;
        }

        $(window).scroll(function () {
            if (theLoc >= $(window).scrollTop()) {
                if ($('.search-area').hasClass('fixed')) {
                    $('.search-area').removeClass('fixed').css({
                        top: 0
                    });
                }
            } else {
                if (!$('.search-area').hasClass('fixed')) {
                    $('.search-area').addClass('fixed').css({
                        top: header_h
                    });
                }
            }
        });
    }

// Sticky header
    if ($('body').hasClass('sticky-header')) {
        var theLoc = $('header.main').position().top;
        $(window).scroll(function () {
            if (theLoc >= $(window).scrollTop()) {
                if ($('header.main').hasClass('fixed')) {
                    $('header.main').removeClass('fixed');
                }
            } else {
                if (!$('header.main').hasClass('fixed')) {
                    $('header.main').addClass('fixed');
                }
            }
        });
    }


    $('.bg-parallax').each(function () {
        var $obj = $(this);

        $(window).scroll(function () {
            // var yPos = -($(window).scrollTop() / $obj.data('speed'));
            var animSpeed;
            if ($obj.hasClass('bg-blur')) {
                animSpeed = 10;
            } else {
                animSpeed = 15;
            }
            var yPos = -($(window).scrollTop() / animSpeed);
            var bgpos = '50% ' + yPos + 'px';
            $obj.css('background-position', bgpos);

        });
    });

    $(document).ready(function () {

        $(window).on('scroll touchmove', function (e) {
            $('#top-nav').toggleClass('clothed', $(document).scrollTop() > 3);
        }).scroll();

        // hamburger mobile menu
        $('.hamburger-menu').on('click', function (e) {
            e.preventDefault();
            $('.mobile-menu').toggleClass('open');
            $('.background-mask').toggleClass('active');
            $('body').toggleClass('overflow-hidden');
        });

        $('.mobile-menu-panel .panel-heading').on('click', function () {
            $(this).children('.fa-right').toggleClass('rotate');
        });

        // close mobile menu
        function closeMenu() {
            $('.mobile-menu').removeClass('open');
            $('.background-mask').removeClass('active');
            $('body').removeClass('overflow-hidden');
        }
    });


})(window.jQuery);
