﻿$(function() {

	$('.check-block').on('click', function(){
		$(this).find('.check').toggleClass('active');
	});

	$('.pop-block .punkts li').on('click', function(){
		var data_id = $(this).attr('data-punkt-id');
		var pop_block = $(this).parents('.pop-block');
		pop_block.find('.punkts li').removeClass('active');
		$(this).addClass('active');
		pop_block.find('.pop-menu-content').removeClass('active');
		$(this).parents('.pop-block').find('.pop-menu-content[data-punkt-id=' + data_id + ']').addClass('active');
	});

	//Открытие меню выбора города
	$('.header .city').on('click', function(){
		$('body').addClass('no-scroll');
		$('.overlay').addClass('active');
		$('.pop-city').addClass('active');
	});

	//Выбор города
	$('.pop-city .pop-menu-content ul li').on('click', function(){
		$('body').removeClass('no-scroll');
		$('.overlay').removeClass('active');
		$('.pop-city').removeClass('active');		
	});

	$('.pop-time-1 .close').on('click', function(){
		$('.overlay').removeClass('active');
		$('.pop-time-1').removeClass('active');
	});


	$('.pop-time-2 .close-small').on('click', function(){
		$('.pop-time-2').removeClass('active');
	});


    /*setTimeout(function(){
        $('.overlay').addClass('active');
        $('.pop-time-1').addClass('active');
    }, 2000);

    setTimeout(function(){
        $('.pop-time-2').addClass('active');
    }, 5000);*/

});
