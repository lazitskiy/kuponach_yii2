<?php

namespace frontend\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Main frontend application asset bundle.
 */
class FrontendAsset extends AssetBundle
{
    //public $sourcePath = '@frontend/views';
    //public $basePath = '@webroot/assets';
    public $baseUrl = '@web/static';

    public $publishOptions = [
        'only' => [
            '*.js',
            '*.css',
        ],
    ];

    public $css = [
        'css/main.css',
    ];
    public $js = [
        'js/main.js',
    ];
    public $jsOptions = [
        'position' => View::POS_END,
    ];
    public $depends = [
        \yii\web\YiiAsset::class,
        \yii\bootstrap\BootstrapAsset::class,
    ];
}
