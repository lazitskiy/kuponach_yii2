<?php

namespace frontend\assets;

use yii\bootstrap\BootstrapPluginAsset;
use yii\web\AssetBundle;
use yii\web\View;
use yii\web\YiiAsset;

/**
 * Main frontend application asset bundle.
 */
class KuponachAsset extends AssetBundle
{
    public $baseUrl = '@web/media';

    public $publishOptions = [
        'only' => [
            '*.js',
            '*.css',
        ],
    ];

    public $css = [
        'css/font_awesome.css',
        'css/styles.css',

        'css/custom.css',
    ];
    public $js = [
        //'js/countdown.min.js',
        //'js/fitvids.min.js',
        //'js/flexnav.min.js',
        //'js/icheck.js',
        //'js/ionrangeslider.js',
        'js/jquery.lazy.min.js',
        //'js/masonry.js',
        //'js/owl-carousel.js',

        'js/custom.js',
    ];
    public $jsOptions = [
        'position' => View::POS_END,
    ];

    public $depends = [
        YiiAsset::class,
        BootstrapPluginAsset::class,

    ];
}
