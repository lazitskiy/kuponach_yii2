<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 10.08.17
 * Time: 21:11
 */

namespace frontend\controllers;


use common\models\entity\offer\OfferSphinx;
use common\traits\base\ApplicationAwareTrait;
use common\traits\GeoServiceAwareTrait;
use frontend\controllers\base\BaseController;

class SuggestController extends BaseController
{
    use ApplicationAwareTrait;
    use GeoServiceAwareTrait;

    public function actionSearch($q)
    {
        $data = OfferSphinx::getRepository()->findTagByString($q);

        return $this->asJson($data);

    }
}
