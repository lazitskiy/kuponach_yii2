<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 05.08.17
 * Time: 1:24
 */

namespace frontend\controllers\base;


use common\helpers\UrlHelper;
use common\models\entity\city\City;
use yii\web\Controller;

class BaseController extends Controller
{
    /**
     * @var City
     */
    protected $currentCity;

    /**
     * @return City|null
     */
    public function getCityCurrent()
    {
        if ($this->currentCity) {
            return $this->currentCity;
        }

        $subdomain = UrlHelper::getSubdomain();
        if (!$subdomain) {
            $where['id'] = City::CITY_ID_DEFAULT;
        } else {
            $where['slug'] = $subdomain;
        }

        return City::getRepository()->where($where)->one() ?? City::getRepository()->where(City::CITY_ID_DEFAULT)->one();
    }
}
