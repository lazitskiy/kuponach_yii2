<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 05.08.17
 * Time: 1:24
 */

namespace frontend\controllers\base;


use common\helpers\UrlHelper;
use common\models\entity\category\Category;
use common\models\entity\city\City;
use common\models\entity\tag\Tag;
use common\traits\base\ApplicationAwareTrait;

class BaseSiteController extends BaseController
{

    use ApplicationAwareTrait;

    /**
     * @var City[]
     */
    protected $listCities;

    protected $listTags;

    protected $listTagsOnMain;

    public function init()
    {
        parent::init();
        $this->initListCities();

        $this->currentCity = $this->getCityCurrent();
    }

    /**
     * @return array
     */
    public function getCitiesAsAlphabet()
    {
        $cities = $this->listCities;
        $arCity = [];
        $arCityImportant = [];
        foreach ($cities as $city) {

            if ($city->is_important) {
                $firstLetter = City::IS_IMPORTANT_TEXT;
                $arCityImportant[$firstLetter][] = $city;
            } else {
                $firstLetter = mb_substr($city->name, 0, 1);
                $arCity[$firstLetter][] = $city;
            }

        }
        ksort($arCity);
        $arCity = array_merge($arCityImportant, $arCity);

        return $arCity;
    }

    /**
     * @return int
     */
    protected function getCityDefault()
    {
        return City::CITY_ID_DEFAULT;
    }

    /**
     * @return int
     */
    public function getCityIdCurrent()
    {
        return $this->getCityCurrent()->id;
    }


    /**
     * @param $slug
     * @return int|null
     */
    public function getTagIdBySlug($slug)
    {
        $tag = $this->getTagBySlug($slug);
        if (!$tag) {
            return null;
        }

        return $tag->id;
    }

    /**
     * @param $slug
     * @return Tag|null
     */
    public function getTagBySlug($slug)
    {
        if (isset($this->listTags[$slug])) {
            return $this->listTags[$slug];
        }

        $tag = Tag::findOne(['slug' => $slug]);
        $this->listTags[$tag->slug] = $tag;

        return $tag;
    }

    /**
     * @return Tag[]
     */
    public function getTagsOnMain()
    {
        if ($this->listTagsOnMain) {
            return $this->listTagsOnMain;
        }

        $this->listTagsOnMain = Tag::find()
            ->with('tagChildren')
            ->where(['show_on_main' => 1])
            ->orderBy(['sort_on_main' => SORT_DESC])
            ->all();

        return $this->listTagsOnMain;
    }

    public function addFlashErrors($errors = [])
    {
        foreach ($errors as $_error) {
            foreach ($_error as $error) {
                $this->getApplication()->getSession()->addFlash('error', $error);
            }
        }
    }


    private function initListCities()
    {
        if (!$this->listCities) {
            $this->listCities = City::getRepository()->getAllActive();
        }
    }

}
