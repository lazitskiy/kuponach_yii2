<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 13.08.17
 * Time: 15:24
 */

namespace frontend\controllers\user;


use frontend\controllers\base\BaseSiteController;

class BaseUserController extends BaseSiteController
{
    public function init()
    {
        parent::init();

        $this->layout = '@frontend/views/user/layout/user';
    }
}
