<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 07.08.17
 * Time: 22:54
 */

namespace frontend\controllers\user;


use common\models\entity\user\Profile;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class IndexController extends \dektrium\user\controllers\SettingsController
{
    public function init()
    {
        parent::init();

        $this->layout = '@frontend/views/user/layout/user';
    }


    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ]);
    }

    public function actionIndex()
    {
        return $this->render('/index/index');
    }

    public function actionProfile()
    {
        $model = $this->finder->findProfileById(\Yii::$app->user->identity->getId());

        if ($model == null) {
            $model = \Yii::createObject(Profile::class);
            $model->link('user', \Yii::$app->user->identity);
        }

        $event = $this->getProfileEvent($model);

        $this->performAjaxValidation($model);

        $this->trigger(self::EVENT_BEFORE_PROFILE_UPDATE, $event);
        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->getSession()->setFlash('success', \Yii::t('user', 'Your profile has been updated'));
            $this->trigger(self::EVENT_AFTER_PROFILE_UPDATE, $event);
            return $this->refresh();
        }

        return $this->render('/index/profile', [
            'model' => $model,
        ]);

    }


}
