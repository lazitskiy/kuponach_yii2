<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 29.10.17
 * Time: 11:52
 */

namespace frontend\controllers;


use common\models\entity\offer\Offer;
use common\models\entity\click\Click;
use frontend\controllers\base\BaseController;

class OutController extends BaseController
{
    public $layout = 'out';

    public function actionIndex($offerId)
    {
        /** @var Offer $offer */
        $offer = Offer::find()
            ->select(['id', 'url', 'operator_id'])
            ->with('operator')
            ->where(['id' => $offerId])
            ->one();


        if (!$offer) {

        }

        $click = new Click([
            'operator_id' => $offer->operator_id,
        ]);
        $click->save();

        // TODO Установить аналитику
        return $this->render('index', [
            'offer' => $offer,
            'click' => $click,
        ]);
    }
}
