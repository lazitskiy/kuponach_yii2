<?php

namespace frontend\controllers;

use common\models\entity\city\City;
use common\models\entity\company\CompanyAddress;
use common\models\entity\company\CompanyAddressCityArea;
use common\models\entity\company\CompanyAddressCityDistrict;
use common\models\entity\company\CompanyAddressCityMetro;
use common\models\entity\company\CompanyAddressCityStreet;
use common\models\entity\offer\Offer;
use common\models\entity\offer\OfferSphinx;
use common\models\entity\offer\OfferSphinxRepository;
use common\models\entity\tag\Tag;
use frontend\controllers\base\BaseSiteController;
use frontend\widgets\CouponWidget;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;

/**
 * Site controller
 */
class SiteController extends BaseSiteController
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * @param string $slug
     * @param string $nedorogo
     * @param string $besplatno
     * @param string $area
     * @param string $district
     * @param string $street
     * @param string $metro
     * @param int $page
     * @return string
     */
    public function actionIndex($slug = '', $nedorogo = '', $besplatno = '', $area = '', $district = '', $street = '', $metro='', $page = 1)
    {
        $city = $this->getCityCurrent();
        $tag = $this->getTagBySlug($slug);

        /**
         * Фильтры пока сделаю тут, потом перенесу
         */
        $filters = [
            'area' => [
                'values' => CompanyAddressCityArea::getRepository()->getByCityId($city->id, 'slug'),
                'selected' => $area,
            ],
            'district' => [
                'values' => CompanyAddressCityDistrict::getRepository()->getByCityId($city->id, 'slug'),
                'selected' => $district,
            ],
            'street' => [
                'values' => CompanyAddressCityStreet::getRepository()->getByCityId($city->id, 'slug'),
                'selected' => $street,
            ],
            'metro' => [
                'values' => CompanyAddressCityMetro::getRepository()->getByCityId($city->id, 'slug'),
                'selected' => $metro,
            ],
        ];

        // SEO
        $seoTitle = $tag->getSeoTitle($this->getCityCurrent(), $nedorogo, $besplatno, $filters);
        $seoH1 = $tag->getSeoH1($this->getCityCurrent(), $nedorogo, $besplatno, $filters);
        $seoSubHeader = $tag->getSeoSubHeader($this->getCityCurrent(), $tag, $nedorogo, $besplatno, $filters);
        $this->getView()->title = $seoTitle;

        $searchResult = CouponWidget::widget([
            'cityId' => $city->id,
            'tagCurrent' => $tag,
            'tags' => [$tag],
            'nedorogo' => !empty($nedorogo),
            'besplatno' => !empty($besplatno),
            'filters' => $filters,
            'page' => $page,
        ]);

        return $this->render('index', [
            'seoH1' => $seoH1,
            'seoSubHeader' => $seoSubHeader,
            'tag' => $tag,
            'city' => $city,
            'searchResult' => $searchResult,
        ]);
    }

    public function actionCoupon($slug)
    {
        $offer = Offer::findOne(['slug' => $slug]);

        // SEO
        $seoTitle = $offer->getSeoTitle($this->getCityCurrent());
        $seoH1 = $offer->getSeoH1($this->getCityCurrent());

        $this->getView()->title = $seoTitle;

        return $this->render('coupon', [
            'seoH1' => $seoH1,
            'offer' => $offer,
        ]);
    }
}
