<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 05.08.17
 * Time: 1:32
 */

namespace frontend\controllers;


use frontend\controllers\base\BaseSiteController;
use common\traits\base\ApplicationAwareTrait;

class StaticPageController extends BaseSiteController
{
    use ApplicationAwareTrait;

    public function actionGuarantees()
    {
        return $this->render('guarantees', [
            'applicationName' => $this->getApplication()->getApplicationName()
        ]);
    }
}
