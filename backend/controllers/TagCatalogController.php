<?php

namespace backend\controllers;

use backend\controllers\base\BaseCrudController;
use common\models\entity\seo\Seo;
use common\services\internal\WordstatService;
use common\services\TagService;
use cebe\markdown\Parser;
use common\helpers\StringHelper;
use common\models\entity\offer\Offer;
use common\models\entity\tag\TagPattern;
use GuzzleHttp\Client;
use kartik\grid\EditableColumnAction;
use VerbalExpressions\PHPVerbalExpressions\VerbalExpressions;
use Yii;
use common\models\entity\tag\Tag;
use common\models\entity\tag\TagSearch;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * TagCatalogController implements the CRUD actions for Tag model.
 */
class TagCatalogController extends BaseCrudController
{

    /**
     * Lists all Tag models.
     * @return mixed
     */
    public function actionIndex()
    {

        $searchModel = new TagSearch();
        list($dataProvider, $allTagsTree) = $searchModel->searchArray(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'allTagsTree' => $allTagsTree,
        ]);
    }

    public function actionSynonym($tagId, $tagIdParent)
    {
        $tagIdNews = $this->getApplication()->getRequest()->get('tagIdNew');
        $this->getApplication()->getResponse()->format = Response::FORMAT_JSON;

        foreach ($tagIdNews as $tagIdNew) {
            $tag = Tag::findOne($tagIdNew);
            $tag->synonym_tag_id = $tagId;
            $tag->save();
        }

        return [
            'status' => 'success',
            'text' => 'ok',
        ];
    }

    public function actionMove($tagId, $tagIdParent)
    {
        $tagIdNews = $this->getApplication()->getRequest()->get('tagIdNew');
        $this->getApplication()->getResponse()->format = Response::FORMAT_JSON;

        // Это тег родительского уровня
        if ($tagId == $tagIdParent) {
            foreach ($tagIdNews as $tagIdNew) {
                $sql = "INSERT INTO tag_tag(tag_id, tag_id_parent) VALUES($tagId, $tagIdNew)";
                \Yii::$app->getDb()->createCommand($sql)->execute();
            }

            return [
                'status' => 'success',
                'text' => 'ok',
            ];
        }

        // перенести в верхний уровень
        foreach ($tagIdNews as $tagIdNew) {
            if ($tagIdNew === 0) {
                $sql = "DELETE FROM tag_tag WHERE tag_id = {$tagId} AND tag_id_parent = {$tagIdParent}";
                \Yii::$app->getDb()->createCommand($sql)->execute();

                return [
                    'status' => 'success',
                    'text' => 'ok',
                ];
            }
        }

        foreach ($tagIdNews as $tagIdNew) {
            if ($tagIdNew == $tagId) {
                return [
                    'status' => 'error',
                    'text' => 'Нельзя переносить тег сам в себя',
                ];
            }
        }

        foreach ($tagIdNews as $tagIdNew) {
            $sql = "UPDATE tag_tag SET tag_id_parent = $tagIdNew WHERE tag_id = $tagId AND tag_id_parent = $tagIdParent";
            \Yii::$app->getDb()->createCommand($sql)->execute();
        }

        return [
            'status' => 'success',
            'text' => 'ok',
        ];
    }

    public function actionDelete($tagId, $tagIdParent)
    {
        $this->getApplication()->getResponse()->format = Response::FORMAT_JSON;

        $t = Tag::getRepository()
            ->with('tagParents')
            ->getById($tagId);
        $linkCount = count($t->tagParents);

        if ($linkCount >= 2) {
            $sql = "DELETE FROM tag_tag WHERE tag_id = {$tagId} AND tag_id_parent = {$tagIdParent}";
            \Yii::$app->getDb()->createCommand($sql)->execute();

            return [
                'status' => 'success',
                'text' => 'ok',
            ];
        } else {
            return [
                'status' => 'error',
                'text' => 'Этот тег привязян 1 раз. Нельзя его удалить',
            ];
        }
    }

    public function actionShowOnMain($tagId, $checked)
    {
        Tag::updateAll(
            ['show_on_main' => $checked],
            ['id' => $tagId]
        );
    }
}
