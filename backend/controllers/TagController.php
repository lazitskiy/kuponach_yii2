<?php

namespace backend\controllers;

use backend\controllers\base\BaseCrudController;
use common\services\TagService;
use cebe\markdown\Parser;
use common\helpers\StringHelper;
use common\models\entity\offer\Offer;
use common\models\entity\tag\TagPattern;
use kartik\grid\EditableColumnAction;
use VerbalExpressions\PHPVerbalExpressions\VerbalExpressions;
use Yii;
use common\models\entity\tag\Tag;
use common\models\entity\tag\TagSearch;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * TagController implements the CRUD actions for Tag model.
 */
class TagController extends BaseCrudController
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ]);
    }

    public function actions()
    {
        // СОхранялка тегов в \common\models\entity\offer\Tag::setTags
        return ArrayHelper::merge(parent::actions(), [
            'offer-save' => [                                       // identifier for your editable column action
                'class' => EditableColumnAction::class,     // action class name
                'modelClass' => Tag::class,                // the model for the record being edited
                'outputValue' => function ($offer, $attribute) {
                    /** @var Tag $offer */
                    $offer = Tag::findOne($offer->id);
                    switch ($attribute) {
                        case 'status':
                            //$string = Tag::getStatusMap($offer->status);
                            break;
                        case 'tags':
                            /*if (!$offer->tags) {
                                $string = '<span class="not-set">Не задано</span>';
                            } else {
                                $items = \yii\helpers\ArrayHelper::getColumn($offer->tags, 'name');
                                $string = Html::ul($items, [
                                    'style' => 'padding:0;list-style-type:none;',
                                    'itemOptions' => [
                                        'style' => 'white-space:nowrap',
                                    ],
                                ]);
                            }
                            break;*/
                        default:
                            $string = $offer->$attribute;
                    }

                    return $string;
                },
                'outputMessage' => function ($model, $attribute, $key, $index) {
                    return '';                                  // any custom error to return after model save
                },
                'showModelErrors' => true,                        // show model validation errors after save
                'errorOptions' => ['header' => '']                // error summary HTML options
                // 'postOnly' => true,
                // 'ajaxOnly' => true,
                // 'findModel' => function($id, $action) {},
                // 'checkAccess' => function($action, $model) {}
            ],
            'tag-save' => [                                       // identifier for your editable column action
                'class' => EditableColumnAction::class,     // action class name
                'modelClass' => Tag::class,                // the model for the record being edited
                'outputValue' => function ($tag, $attribute, $key, $index) {
                    /** @var Tag $tag */
                    $tag = Tag::findOne($tag->id);
                    switch ($attribute) {
                        case 'is_group':
                            $string = $tag->is_group ? 'Групповой' : 'Обычный';
                            break;
                        default:
                            $string = $tag->$attribute;
                    }

                    return $string;
                },
                'outputMessage' => function ($model, $attribute, $key, $index) {
                    return '';                                  // any custom error to return after model save
                },
                'showModelErrors' => true,                        // show model validation errors after save
                'errorOptions' => ['header' => '']                // error summary HTML options
                // 'postOnly' => true,
                // 'ajaxOnly' => true,
                // 'findModel' => function($id, $action) {},
                // 'checkAccess' => function($action, $model) {}
            ],
        ]);
    }

    /**
     * Lists all Tag models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TagSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $pattern
     * @param $patternType
     * @return array Автокомплит офферов для паттерна правила
     */
    public function actionPattern($pattern, $patternType)
    {
        $this->getApplication()->getResponse()->format = Response::FORMAT_JSON;

        if (!$pattern) {
            return [
                'status' => 'error',
                'text' => 'Заполните занчение',
            ];
        }

        $matches = Offer::getRepository()->getMatched($pattern, $patternType);
        if ($matches) {
            return [
                'status' => 'success',
                'count' => count($matches),
                'text' => $this->renderAjax('tag-patterns/response_table', [
                    'matches' => $matches,
                ]),
            ];
        } else {
            return [
                'status' => 'success',
                'text' => 'Совпадений не найдено',
            ];
        }
    }

    public function actionPatternSave($tagId, $pattern, $patternType)
    {
        $this->getApplication()->getResponse()->format = Response::FORMAT_JSON;
        $tagPattern = TagPattern::getRepository()
            ->where([
                'pattern' => $pattern,
                'pattern_type' => $patternType,
                'tag_id' => $tagId,
            ])->one();

        if ($tagPattern) {
            $msg = 'Ошибка. Это правило уже есть для тега: <b>' . $tagPattern->tag->name . '</b>';

            return [
                'status' => 'error',
                'text' => $msg,
            ];
        }

        $tagPattern = new TagPattern([
            'tag_id' => $tagId,
            'pattern' => $pattern,
            'pattern_type' => $patternType,
        ]);

        if (!$tagPattern->save()) {
            return [
                'status' => 'error',
                'text' => $tagPattern->getFirstErrors(),
            ];
        }

        (new TagService())->appendTag($tagId);

        return [
            'status' => 'success',
            'text' => '',
        ];
    }

    public function actionTagGroupSave()
    {
        $this->getApplication()->getResponse()->format = Response::FORMAT_JSON;

        $tagId = $_GET['tagId'];
        $tagIdChildren = $_GET['tagIdChildren'];

        if ($tagId && $tagIdChildren) {
            $tag = Tag::getRepository()->getById($tagId);
            $tag->setTagsChildren($tagIdChildren, true);

            return [
                'status' => 'success',
                'text' => '',
            ];
        } else {
            return [
                'status' => 'error',
                'text' => 'Укажите дочерние теги',
            ];
        }
    }

    /**
     * Displays a single Tag model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Tag model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Tag();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Tag model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Tag model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Tag model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tag the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tag::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
