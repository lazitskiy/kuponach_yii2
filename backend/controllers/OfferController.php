<?php

namespace backend\controllers;

use backend\controllers\base\BaseCrudController;
use common\helpers\Inflector;
use common\models\entity\tag\Tag;
use kartik\grid\EditableColumnAction;
use Yii;
use common\models\entity\offer\Offer;
use common\models\entity\offer\OfferSearch;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * OfferController implements the CRUD actions for Offer model.
 */
class OfferController extends BaseCrudController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ]);
    }

    public function actions()
    {
        // СОхранялка тегов в \common\models\entity\offer\Offer::setTags
        return ArrayHelper::merge(parent::actions(), [
            'offer-save' => [                                       // identifier for your editable column action
                'class' => EditableColumnAction::class,     // action class name
                'modelClass' => Offer::class,                // the model for the record being edited
                'outputValue' => function ($offer, $attribute, $key, $index) {
                    /** @var Offer $offer */
                    $offer = Offer::findOne($offer->id);
                    switch ($attribute) {
                        case 'status':
                            $string = Offer::getStatusMap($offer->status);
                            break;
                        case 'tags':
                            if (!$offer->tags) {
                                $string = '<span class="not-set">Не задано</span>';
                            } else {
                                $items = \yii\helpers\ArrayHelper::getColumn($offer->tags, 'name');
                                $string = Html::ul($items, [
                                    'style' => 'padding:0;list-style-type:none;',
                                    'itemOptions' => [
                                        'style' => 'white-space:nowrap',
                                    ],
                                ]);
                            }
                            break;
                        default:
                            $string = $offer->$attribute;
                    }

                    return $string;
                },
                'outputMessage' => function ($model, $attribute, $key, $index) {
                    return '';                                  // any custom error to return after model save
                },
                'showModelErrors' => true,                        // show model validation errors after save
                'errorOptions' => ['header' => '']                // error summary HTML options
                // 'postOnly' => true,
                // 'ajaxOnly' => true,
                // 'findModel' => function($id, $action) {},
                // 'checkAccess' => function($action, $model) {}
            ],
        ]);
    }

    /**
     * Lists all Offer models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OfferSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $lastWeek = (new \DateTime('-7 days'))->setTime(0, 0, 0)->format('Y-m-d H:i:s');

        $newOffers = (new Query())
            ->select(['date(created_at) date', 'COUNT(*) count'])
            ->from('offer')
            ->where(['>', 'created_at', $lastWeek])
            ->groupBy('date(created_at)')->all();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'newOffers' => $newOffers,
        ]);
    }

    public function actionDetail()
    {
        if (isset($_POST['expandRowKey'])) {
            $model = Offer::findOne($_POST['expandRowKey']);

            return $this->renderAjax('_offerDetail', ['offer' => $model]);
        } else {
            return '<div class="alert alert-danger">No data found</div>';
        }
    }

    public function actionSetTag($offerId, $tagName)
    {
        $this->getApplication()->getResponse()->format = Response::FORMAT_JSON;

        $offer = Offer::getRepository()->getById($offerId);
        $offer->setTags([$tagName]);

        return [
            'status' => 'success',
            'text' => '',
        ];
    }

    public function actionSetIsTagPatterned($offerId, $value)
    {
        $offer = Offer::getRepository()->getById($offerId);
        $offer->is_tag_patterned = $value;
        $offer->update(false);
    }

    public function actionGetTags($q = null, $id = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [
            'results' => [
                'id' => '',
                'text' => '',
            ],
        ];

        $q = trim($q);
        if ($q) {
            $tags = Tag::find()
                ->select(['id', 'name text'])
                ->where(['like', 'name', $q])
                ->asArray()
                ->all();
            $out['results'] = ($tags);
        } elseif ($id) {
            $out['results'] = ['id' => $id, 'text' => Tag::findOne(['id' => $id])->name];
        }

        return $out;
    }


    /**
     * Displays a single Offer model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Offer model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Offer();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Offer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Offer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Offer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Offer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Offer::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
