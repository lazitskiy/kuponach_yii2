<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 16.09.17
 * Time: 23:27
 */

namespace backend\controllers\base;


use common\traits\base\ApplicationAwareTrait;
use kartik\grid\EditableColumnAction;
use yii\filters\AccessControl;
use yii\web\Controller;

class BaseAdminController extends Controller
{
    use ApplicationAwareTrait;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ], [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
}
