<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 16.09.17
 * Time: 23:39
 */

namespace backend\controllers\base;


class BaseCrudController extends BaseAdminController
{
    public function getViewPath()
    {
        return '@backend/views/crud/' . $this->id;
    }

}
