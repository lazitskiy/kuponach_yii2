<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 05.10.17
 * Time: 15:03
 */

namespace backend\controllers;


use backend\controllers\base\BaseAdminController;
use RubtsovAV\YandexWordstatParser\Browser\ReactPhantomJs;
use RubtsovAV\YandexWordstatParser\Captcha\Image;
use RubtsovAV\YandexWordstatParser\Parser;
use RubtsovAV\YandexWordstatParser\Query;
use RubtsovAV\YandexWordstatParser\YandexUser;
use yii\web\Response;

class WordstatController extends BaseAdminController
{
    protected $runtimeDir;

    public function init()
    {
        parent::init();

        $this->runtimeDir = \Yii::getAlias('@runtime/phantomjs');
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionGet($word)
    {
        $captcha = $this->runtimeDir . '/captcha.jpg';
        if (file_exists($captcha)) {
            unlink($captcha);
        }

        $this->getApplication()->getResponse()->format = Response::FORMAT_JSON;
        $yandexUser = new YandexUser('anikusha88', 'Ghbytcb100Nso', $this->runtimeDir);
        //$proxy = new HttpProxy('1.179.198.17', 8080);

        $bin = YII_ENV_DEV ? '/usr/local/bin/phantomjs' : 'phantomjs';
        $browser = new ReactPhantomJs();
        $browser->setPhantomJsPath($bin);
        //$browser->setProxy($proxy); // optional
        $browser->setTimeout(20);   // in seconds (120 by default)

        $runtimeDir = $this->runtimeDir;
        $browser->setCaptchaSolver(function ($captcha) use ($runtimeDir) {

            /** @var Image $captcha */
            $image = file_get_contents($captcha->getImageUri());
            file_put_contents($runtimeDir . '/captcha.jpg', $image);
            file_put_contents($runtimeDir . '/captchaAnswer.txt', '');

            echo "The captcha image was save to captcha.jpg. Write the answer in captchaAnswer.txt\n";
            $answer = '';
            while (!$answer) {
                $answer = file_get_contents($runtimeDir . '/captchaAnswer.txt');
                $answer = trim($answer);
                sleep(1);
            }
            echo "The captcha answer is '$answer'\n";
            $captcha->setAnswer($answer);

            return true;
        });

        $parser = new Parser($browser, $yandexUser);

        $query = new Query($word);
        $result = $parser->query($query);

        return [
            'status' => 'success',
            'text' => $this->renderPartial('_wordstat', [
                'result' => $result->toArray(),
            ]),
        ];
    }

    public function actionCaptchaCheck()
    {
        $this->getApplication()->getResponse()->format = Response::FORMAT_JSON;
        $captcha = $this->runtimeDir . '/captcha.jpg';
        $base64 = '';
        if (file_exists($captcha)) {
            $base64 = base64_encode(file_get_contents($this->runtimeDir . '/captcha.jpg'));
        }

        return [
            'status' => 'success',
            'src' => $base64,
        ];
    }

    public function actionCaptchaProcess($answer)
    {
        $answer = trim($answer);
        file_put_contents($this->runtimeDir . '/captchaAnswer.txt', $answer);
    }
}
