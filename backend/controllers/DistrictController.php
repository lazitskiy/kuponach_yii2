<?php

namespace backend\controllers;

use backend\controllers\base\BaseCrudController;
use common\models\entity\company\CompanyAddressCityArea;
use common\models\entity\company\CompanyAddressCityAreaSearch;
use common\models\entity\company\CompanyAddressCityDistrict;
use common\models\entity\company\CompanyAddressCityDistrictSearch;
use kartik\grid\EditableColumnAction;
use Yii;
use yii\helpers\ArrayHelper;
use yii\filters\VerbFilter;

/**
 * TagController implements the CRUD actions for Tag model.
 */
class DistrictController extends BaseCrudController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ]);
    }

    public function actions()
    {
        // СОхранялка тегов в \common\models\entity\offer\Tag::setTags
        return ArrayHelper::merge(parent::actions(), [
            'district-save' => [                                       // identifier for your editable column action
                'class' => EditableColumnAction::class,     // action class name
                'modelClass' => CompanyAddressCityDistrict::class,                // the model for the record being edited
                'outputValue' => function ($area, $attribute, $key, $index) {
                    return $area->$attribute;
                },
                'outputMessage' => function ($model, $attribute, $key, $index) {
                    return '';                                  // any custom error to return after model save
                },
                'showModelErrors' => true,                        // show model validation errors after save
                'errorOptions' => ['header' => '']                // error summary HTML options
                // 'postOnly' => true,
                // 'ajaxOnly' => true,
                // 'findModel' => function($id, $action) {},
                // 'checkAccess' => function($action, $model) {}
            ],
        ]);
    }

    /**
     * Lists all Tag models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CompanyAddressCityDistrictSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
