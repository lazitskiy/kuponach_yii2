<?php

namespace backend\controllers;

use backend\controllers\base\BaseCrudController;
use common\traits\base\DadataAwareTrait;
use kartik\grid\EditableColumnAction;
use Yii;
use common\models\entity\city\City;
use common\models\entity\city\CitySearch;
use yii\base\Action;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * CityController implements the CRUD actions for City model.
 */
class CityController extends BaseCrudController
{
    use DadataAwareTrait;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ]);
    }

    public function actions()
    {
        return ArrayHelper::merge(parent::actions(), [
            'city-save' => [                                       // identifier for your editable column action
                'class' => EditableColumnAction::class,     // action class name
                'modelClass' => City::class,                // the model for the record being edited
                'outputValue' => function ($city, $attribute, $key, $index) {
                    /** @var City $city */

                    switch ($attribute) {
                        case 'is_active':
                            $string = $city->is_active ? '<span class="glyphicon glyphicon-ok text-success"></span>' : '<span class="glyphicon glyphicon-remove text-danger"></span>';
                            break;
                        case 'is_important':
                            $string = $city->is_important ? '<span class="glyphicon glyphicon-ok text-success"></span>' : '<span class="glyphicon glyphicon-remove text-danger"></span>';
                            break;
                        default;
                            $string = $city->$attribute;
                    }

                    return $string;
                },
                'outputMessage' => function ($model, $attribute, $key, $index) {

                    return '';                                  // any custom error to return after model save
                },
                'showModelErrors' => true,                        // show model validation errors after save
                'errorOptions' => ['header' => '']                // error summary HTML options
                // 'postOnly' => true,
                // 'ajaxOnly' => true,
                // 'findModel' => function($id, $action) {},
                // 'checkAccess' => function($action, $model) {}
            ],
        ]);
    }

    public function actionDetail()
    {
        if (isset($_POST['expandRowKey'])) {
            $city = City::findOne($_POST['expandRowKey']);

            $response = $this->getDadata()->getAddress([
                'query' => $city->name,
                'count' => 1,
            ]);

            $response2 = $this->getDadata()->getAddress([
                'query' => $city->name,
                'count' => 4,
            ]);

            if (!$response->getIsOk() && $response2->getIsOk()) {
                return $response->errorMessage . '---' . $response2->errorMessage;
            }

            return $this->render('_cityDetail', [
                'city' => $city,
                'addresses' => array_merge(
                    $response->data['suggestions'] ?? [],
                    $response2->data['suggestions'] ?? []
                ),
            ]);
        } else {
            return '<div class="alert alert-danger">No data found</div>';
        }
    }

    /**
     * Lists all City models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CitySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single City model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new City model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new City();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing City model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing City model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the City model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return City the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = City::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
