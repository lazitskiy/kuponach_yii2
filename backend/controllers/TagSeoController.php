<?php

namespace backend\controllers;

use backend\controllers\base\BaseCrudController;
use common\models\entity\seo\Seo;
use common\services\internal\WordstatService;
use common\services\TagService;
use cebe\markdown\Parser;
use common\helpers\StringHelper;
use common\models\entity\category\Category;
use common\models\entity\offer\Offer;
use common\models\entity\tag\TagPattern;
use GuzzleHttp\Client;
use kartik\grid\EditableColumnAction;
use VerbalExpressions\PHPVerbalExpressions\VerbalExpressions;
use Yii;
use common\models\entity\tag\Tag;
use common\models\entity\tag\TagSearch;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * TagController implements the CRUD actions for Tag model.
 */
class TagSeoController extends BaseCrudController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ]);
    }

    public function actions()
    {
        // СОхранялка тегов в \common\models\entity\offer\Tag::setTags
        return ArrayHelper::merge(parent::actions(), [
            'tag-save' => [                                       // identifier for your editable column action
                'class' => EditableColumnAction::class,     // action class name
                'modelClass' => Tag::class,                // the model for the record being edited
                'outputValue' => function ($tag, $attribute, $key, $index) {
                    /** @var Tag $tag */
                    $tag = Tag::findOne($tag->id);
                    switch ($attribute) {
                        case 'is_group':
                            $string = $tag->is_group ? 'Групповой' : 'Обычный';
                            break;
                        default:
                            $string = $tag->$attribute;
                    }

                    return $string;
                },
                'outputMessage' => function ($model, $attribute, $key, $index) {
                    return '';                                  // any custom error to return after model save
                },
                'showModelErrors' => true,                        // show model validation errors after save
                'errorOptions' => ['header' => '']                // error summary HTML options
                // 'postOnly' => true,
                // 'ajaxOnly' => true,
                // 'findModel' => function($id, $action) {},
                // 'checkAccess' => function($action, $model) {}
            ],
        ]);
    }

    /**
     * Lists all Tag models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TagSearch();
        $dataProvider = $searchModel->searchOriginMethod(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSeoInfo()
    {
        $client = new Client();

        $tagId = $this->getApplication()->getRequest()->post('expandRowKey',10);
        $tag = Tag::findOne($tagId);

        if ($tag->seo) {
            return $this->renderPartial('_seo-info', [
                'tag' => $tag,
                'seo' => $tag->seo
            ]);
        }

        $words = ["купон {$tag->name}", "скидка {$tag->name}", "акция {$tag->name}"];

        // 1. Получаем вордстат
        $runtimeDir = \Yii::getAlias('@backend/runtime/phantomjs');
        $wordstatService = new WordstatService($runtimeDir);

        $seoStat = [];
        foreach ($words as $word) {
            $seoStat[$word]['wordstat'] = $wordstatService->getStat($word);
        }

        // 2. Лезем https://arsenkin.ru/tools/check-top/
        // 3. Лезем https://arsenkin.ru/tools/check-h/

        $response = $client->post('https://arsenkin.ru/tools/check-top/', [
            'form_params' => [
                'yes' => 1,
                'keys' => implode("\n", $words),
                'city' => 213,
                'depth' => 10,
            ],
        ]);

        $data = $response->getBody()->getContents();

        if (preg_match_all('/<table.*?>(.*?)<\/table>/uis', $data, $matches)) {
            $tables = $matches[0];
            foreach ($tables as $k => $table) {
                $keyword = $words[$k];

                if (preg_match_all('/<tr.*?td>(\d)?<.*?(#.*?);.*?href=["\'](.*?)["\'].*?tr>/uis', $table, $matches, PREG_SET_ORDER)) {
                    foreach ($matches as $match) {
                        $url = urldecode($match[3]);
                        $seoStat[$keyword]['info'][$url]['top'] = [
                            'position' => $match[1],
                            'color' => $match[2],
                            'url' => $url,
                        ];
                    }

                    $response = $client->post('https://arsenkin.ru/tools/check-h/index.php', [
                        'form_params' => [
                            'a_mode' => 'getThis',
                            'ajax' => 'Y',
                            'key' => $keyword,
                            'urls' => '',
                            'mode' => 'false',
                            'city' => 213,
                            'depth' => 5,
                            'pause' => 100,
                            'spektr' => 'false',
                            'metacheck' => 'true',
                        ],
                    ]);
                    $data = $response->getBody()->getContents();

                    if (preg_match_all('/<tr.*?(.*?href=["\'](.*?)["\']).*?title(.*?)<\/span.*?description(.*?)<\/span.*?<\/tr>/uis', $data, $matches, PREG_SET_ORDER)) {
                        foreach ($matches as $match) {
                            $url = $match[2];
                            $title = strip_tags($match[3]);
                            $title = preg_replace('/[[:^print:]]/', "", $title);

                            $description = strip_tags($match[4]);
                            $description = preg_replace('/[[:^print:]]/', "", $description);

                            $seoStat[$keyword]['info'][$url]['meta'] = [
                                'title' => $title,
                                'description' => $description,
                                'url' => $url,
                            ];
                        }
                    }
                }
            }
        }

        $seo = new Seo();
        $seo->seo_stat = Json::encode($seoStat);
        $seo->save();

        $tag->seo_id = $seo->id;
        $tag->save();

        return $this->renderPartial('_seo-info', [
            'tag' => $tag,
            'seo' => $seo
        ]);
    }
}
