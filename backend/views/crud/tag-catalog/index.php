<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use common\helpers\StringHelper;
use common\models\entity\tag\Tag;
use common\models\entity\tag\TagPattern;

/* @var $this yii\web\View */
/* @var $searchModel common\models\entity\tag\TagSearch */
/* @var $dataProvider yii\data\ArrayDataProvider */
/* @var $allTagsTree array */

$view = $this;
$this->title = 'Tags';
$this->params['breadcrumbs'][] = $this->title;

$tree = function ($tags, $level = 0, $tagParent) use (&$tree) {
    $arItems = [];
    foreach ($tags as $tag) {

        $checkbox = '';
        if ($level == 0 && $tag['id'] > 0) {
            $checkbox = Html::checkbox('name', $tag['show_on_main'], [
                'value' => $tag['id'],
                'class' => 'js-tag-show-on-main',
                'title' => 'Показывать на главной',
            ]);
        }

        $synonyms = '';
        if ($tag['tagSynonymsChildren']) {
            $tagSynonyms = ArrayHelper::getColumn($tag['tagSynonymsChildren'], 'name');
            $synonyms = '<span class="not-set">Синонимы: (' . implode(',', $tagSynonyms) . ')</span>';
        }

        $arItems[] = $checkbox .
            '[' . $tag['id'] . '] ' . $tag['name'] . ' ' . $synonyms .
            Html::a('->', '#' . $tag['id'], [
                'title' => 'перенести',
                'class' => 'js-show-category-popup',
                'data-tag-id' => $tag['id'],
                'data-tag-id-parent' => $tagParent['id'],
            ]) . ' ' .
            Html::a('+', '#' . $tag['id'], [
                'title' => 'Добавить синоним',
                'class' => 'js-show-synonym-popup',
                'data-tag-id' => $tag['id'],
                'data-tag-id-parent' => $tagParent['id'],
            ]) . ' ' .
            Html::a('x', '#' . $tag['id'], [
                'title' => 'удалить',
                'class' => 'js-delete-tag',
                'data-tag-id' => $tag['id'],
                'data-tag-id-parent' => $tagParent['id'],
            ]);
        if (isset($tag['children'])) {
            $arItems[] = $tree($tag['children'], $level + 1, $tag);
        }
    }

    return Html::ul($arItems, ['encode' => false, 'style' => 'margin-bottom:0;list-style-type:none;' . ($level === 0 ? 'padding:0;' : '')]);
};
?>
<?php
$this->registerJs($this->render('_popup/popup.js'));
echo $this->render('_popup/popup', ['allTagsTree' => $allTagsTree]);

?>
<div class="tag-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,

        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'layout' => "{summary}\n{pager}\n{items}\n{pager}",
        'pjax' => false,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'refreshGrid' => true,
        ],
        'responsive' => false,
        'columns' => [
            [
                'attribute' => 'id',
                'width' => '70px',
            ], [
                'attribute' => 'name',
                /*'width' => '140px',*/
                'contentOptions' => ['class' => 'text-nowrap'],
                'value' => function ($tag) use ($view, $tree) {
                    $html = $tree([$tag], 0, $tag);

                    return $html;
                },
                'format' => 'raw',
            ],
        ],
    ]); ?>
</div>

<?php
$this->registerJs($this->render('_popup/popup.js'));
echo $this->render('_popup/popup', ['allTagsTree' => $allTagsTree]);

?>

