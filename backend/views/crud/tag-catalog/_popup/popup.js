(function ($) {
    "use strict";
    $("document").ready(function () {

        $('.js-delete-tag').click(function () {
            var el = $(this),
                tagId = $(el).data('tag-id'),
                tagIdParent = $(el).data('tag-id-parent');

            $.get('/tag-catalog/delete', {
                    tagId: tagId,
                    tagIdParent: tagIdParent
                },
                function (data) {
                    if (data.status == 'success') {
                        window.location.reload();
                    } else {
                        alert(data.text);
                    }
                }
            );

            return false;
        });

        $('.js-show-category-popup').click(function () {
            var el = $(this),
                tagId = $(el).data('tag-id'),
                tagIdparent = $(el).data('tag-id-parent'),
                popOverSettings = {
                    width: '400px',
                    placement: 'right',
                    html: true,
                    trigger: 'manual',
                    container: 'body',
                    content: function () {
                        return $('#popover-content');
                    }
                };
            $('#popover-content').find('#tag-id').val(tagId);
            $('#popover-content').find('#tag-id-parent').val(tagIdparent);
            $('#popover-content').find('.js-tag-save').val('Перенести').data('type', 'move');

            $(el).popover(popOverSettings);
            $(el).popover('show');
            //$('.select2').select2();

            return false;
        });

        $('.js-show-synonym-popup').click(function () {
            var el = $(this),
                tagId = $(el).data('tag-id'),
                tagIdparent = $(el).data('tag-id-parent'),
                popOverSettings = {
                    width: '400px',
                    placement: 'right',
                    html: true,
                    trigger: 'manual',
                    container: 'body',
                    content: function () {
                        return $('#popover-content');
                    }
                };
            $('#popover-content').find('#tag-id').val(tagId);
            $('#popover-content').find('#tag-id-parent').val(tagIdparent);
            $('#popover-content').find('.js-tag-save').val('Добавить синоним').data('type', 'synonym');

            $(el).popover(popOverSettings);
            $(el).popover('show');
            //$('.select2').select2();

            return false;
        });
        //#####
        // Закрыть все всплывашки
        $(document).on('click', function (e) {
            $('[data-toggle="popover"],[data-original-title]').each(function () {
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                    (($(this).popover('hide').data('bs.popover') || {}).inState || {}).click = false;  // fix for BS 3.3.6
                }
            });
        });


        $('body').on('click', '.js-tag-save', function () {
            var button = $(this);
            var tagId = button.parents('form').find('#tag-id').val();
            var tagIdParent = button.parents('form').find('#tag-id-parent').val();
            var tagIdNew = button.parents('form').find('#tag-id-new').val();
            // move|synonym
            var type = button.data('type');

            button.html('Ждите').attr('disabled', true);

            $.get('/tag-catalog/' + type, {
                    tagId: tagId,
                    tagIdParent: tagIdParent,
                    tagIdNew: tagIdNew
                },
                function (data) {
                    if (data.status == 'success') {
                        window.location.reload();
                    } else {
                        alert(data.text);
                    }
                }
            );

            return false;
        });

        //checkbox показывать на главйно
        $('.js-tag-show-on-main').click(function () {
            var tagId = $(this).val(),
                checked = +$(this).is(':checked');
            $.get('/tag-catalog/show-on-main', {
                    tagId: tagId,
                    checked: checked
                }
            );
        });
    });
}(jQuery));
