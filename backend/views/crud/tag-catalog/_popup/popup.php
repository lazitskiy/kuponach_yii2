<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 12.10.17
 * Time: 16:47
 *
 * @var array $allTagsTree
 */
?>
<style>
    .popover {
        z-index: 1;
    }

    .popover-w80 {
        max-width: 80%; /* Max Width of the popover (depending on the container!) */
        width: 80%; /* Max Width of the popover (depending on the container!) */
    }

    .select2-results__options {
        max-height: 500px !important;
    }

</style>
<?php

$tree = function ($tags, $tagParent = [], $level = 0) use (&$tree) {

    $arItems = [];
    foreach ($tags as $tag) {
        $tag['name'] = ($tagParent ? $tagParent['name'] . "<br>" . str_repeat('&nbsp;', $level * 6) . "|- " : '') . $tag['name'];
        $arItems[] = [
            'id' => $tag['id'],
            'text' => $tag['name'],
            //'children' => [],
        ];

        if (isset($tag['children'])) {
            //$groups[$tag['id']] = ['label'=>$tag['name']];
            $children = $tree($tag['children'], $tag, $level + 1);
            $arItems = array_merge($arItems, $children);
        }
    }

    return $arItems;
};

$opts = $tree($allTagsTree);
array_unshift($opts, [
    'id' => 0,
    'text' => '--- Верхний уровень ---',
]);

?>

<div id="popover-content" style="display: block; width: 600px!important;">
    <form action="">
        <div class="form-group form-inline">
            <input type="hidden" id="tag-id">
            <input type="hidden" id="tag-id-parent">
            <?= \kartik\widgets\Select2::widget([
                'name' => 'tagIdNew',
                'options' => [
                    'encode' => false,
                    'id' => 'tag-id-new',
                ],
                'pluginOptions' => [
                    'multiple' => 'true',
                    'data' => $opts,
                    'escapeMarkup' => new \yii\web\JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new \yii\web\JsExpression('function (d) {return d.text; }'),
                    'templateSelection' => new \yii\web\JsExpression('function (d) { console.log(d);return d.text; }'),
                ],
            ]) ?>
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-success js-tag-save" value="Перенести">
        </div>
    </form>
</div>
