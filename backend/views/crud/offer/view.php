<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\entity\offer\Offer */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Offers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="offer-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'category_ids',
            'city_ids:ntext',
            'is_active',
            'is_hit',
            'company_id',
            'operator_id',
            'operator_offer_id',
            'name',
            'name_short',
            'slug',
            'description:ntext',
            'description_attension:ntext',
            'description_tips:ntext',
            'description_adv:ntext',
            'description_short:ntext',
            'url:url',
            'date_start',
            'date_end',
            'discount_percent',
            'price_original',
            'price_discounted',
            'price_coupon_min',
            'status',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
