<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use common\models\entity\operator\Operator;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\entity\offer\OfferSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $newOffers array */

$this->title = 'Offers';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    img {
        width: 200px;
        height: auto;
    }
</style>
<div class="offer-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Offer', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php if ($newOffers): ?>
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">За последние 7 дней</h3>
            </div>
            <div class="panel-body">
                <?php foreach ($newOffers as $newOffer): ?>
                    <a href="<?= \yii\helpers\Url::current(['OfferSearch' => ['created_at' => $newOffer['date']]]) ?>"><?= $newOffer['date'] ?></a> <b>(<?= $newOffer['count'] ?>)</b> |
                <?php endforeach; ?>
                <a href="<?= \yii\helpers\Url::current(['OfferSearch' => ['created_at' => null]]) ?>">Сбросить</a>
            </div>
        </div>
    <?php endif; ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,

        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'refreshGrid' => true,
        ],
        'responsive' => true,
        'columns' => [
            [
                'attribute' => 'id',
                'width' => '60px',
                'vAlign' => GridView::ALIGN_MIDDLE,
                'hAlign' => GridView::ALIGN_CENTER,
            ], [
                'attribute' => 'operator_offer_id',
                'width' => '60px',
                'vAlign' => GridView::ALIGN_MIDDLE,
                'hAlign' => GridView::ALIGN_CENTER,
            ], [
                'class' => \kartik\grid\DataColumn::class,
                'attribute' => 'operator',
                'value' => 'operator.name',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(Operator::getRepository()->all(), 'id', 'name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ],
                'filterInputOptions' => ['placeholder' => 'Все'],
                'vAlign' => GridView::ALIGN_MIDDLE,
                'format' => 'raw',
            ], [
                'class' => \kartik\grid\BooleanColumn::class,
                'width' => '60px',
                'attribute' => 'is_active',
                'vAlign' => GridView::ALIGN_MIDDLE,
            ], [
                'class' => \kartik\grid\BooleanColumn::class,
                'width' => '60px',
                'attribute' => 'is_hit',
                'vAlign' => GridView::ALIGN_MIDDLE,
            ],
            // 'company_id',
            // 'operator_id',
            // 'operator_offer_id',
            [
                'class' => \kartik\grid\EditableColumn::class,
                'attribute' => 'status',
                'value' => function ($model) {
                    return \common\models\entity\offer\Offer::getStatusMap($model->status);
                },
                'width' => '150px',
                'editableOptions' => [
                    'formOptions' => [
                        'action' => ['/offer/offer-save'],
                    ],
                    //'size'=>'md',
                    //'data'
                    'inputType' => \kartik\editable\Editable::INPUT_DROPDOWN_LIST,
                    'data' => \common\models\entity\offer\Offer::getStatusMap(),
                ],

                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \common\models\entity\offer\Offer::getStatusMap(),
                'filterWidgetOptions' => [
                    'pluginOptions' => [
                        'allowClear' => true,
                        'multiple' => true,
                    ],
                ],
                'filterInputOptions' => ['placeholder' => 'Все'],
                'vAlign' => GridView::ALIGN_MIDDLE,
                'format' => 'raw',
            ], [
                'class' => \kartik\grid\EditableColumn::class,
                'attribute' => 'tags',
                'header' => 'Теги',
                'format' => 'raw',
                //'width' => '450px',
                'value' => function ($model) {
                    if (!$model->tags) {
                        return '<span class="not-set">Не задано</span>';
                    }
                    $items = \yii\helpers\ArrayHelper::getColumn($model->tags, function (\common\models\entity\tag\Tag $tag) {
                        return "[{$tag->id}] {$tag->name}";
                    });

                    $tag = Html::ul($items, [
                        'style' => 'padding:0;list-style-type:none;',
                        'itemOptions' => [
                            'style' => 'white-space:nowrap',
                        ],
                    ]);

                    return $tag;
                },
                'editableOptions' => [
                    'inputType' => \kartik\editable\Editable::INPUT_WIDGET,
                    'widgetClass' => \kartik\editable\Editable::INPUT_SELECT2,
                    'options' => [
                        'data' => \yii\helpers\ArrayHelper::map(\common\models\entity\tag\Tag::find()->all(), 'id', 'name'),
                        'options' => [
                            'multiple' => true,
                        ],
                        'pluginOptions' => [
                            'tags' => true,
                            'tokenSeparators' => [','],
                            'minimumInputLength' => 3,
                            'ajax' => [
                                'url' => '/offer/get-tags',
                                'dataType' => 'json',
                                'delay' => 300,
                                //'data' => new JsExpression('function(params) { return {q:params.term, page: params.page}; }'),
                                //'processResults' => new \yii\web\JsExpression('window.location.reload()'),
                                'cache' => true,
                            ],
                        ],
                    ],
                    'formOptions' => [
                        'action' => ['/offer/offer-save'],
                    ],
                    'editableValueOptions' => [
                        'style' => 'text-align:left;',
                    ],
                ],
                'hAlign' => GridView::ALIGN_LEFT,
            ], [
                'class' => \kartik\grid\ExpandRowColumn::class,
                'width' => '50px',
                'value' => function ($model) {
                    return GridView::ROW_COLLAPSED;
                },
                //'detailUrl' => \yii\helpers\Url::to(['detail']),
                'detail' => function ($model) {
                    return Yii::$app->controller->renderPartial('_offerDetail', ['offer' => $model]);
                },
            ], [
                'attribute' => 'name',
                'format' => 'html',
                //'width' => '450px',
                'value' => function ($model) {
                    return $model->name;
                },
            ],
            // 'name_short',
            // 'slug',
            // 'description:ntext',
            // 'description_attension:ntext',
            // 'description_tips:ntext',
            // 'description_adv:ntext',
            // 'description_short:ntext',
            // 'url:url',
            [
                'attribute' => 'created_at',
                'filterType' => \kartik\grid\GridView::FILTER_DATE,
                'filterWidgetOptions' => [
                    //'type' => \kartik\datetime\DateTimePicker::TYPE_INPUT,
                    'size' => 'md',
                    'layout' => '{input}{remove}',
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'autoclose' => true,
                    ],
                ],
                'format' => ['date', 'php:d.m.y'],
                'width' => '160px',
            ], [
                'attribute' => 'date_end',
                'filter' => false,
                'format' => ['date', 'php:d.m.y'],
                'width' => '80px',
            ],
            'discount_percent',
            'price_original',
            'price_discounted',
            'price_coupon_min',

            // 'created_at',
            // 'updated_at',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update}',
            ],
        ],
    ]); ?>
</div>
