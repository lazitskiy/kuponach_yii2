<?php

use kartik\tabs\TabsX;

/* @var $this yii\web\View */
/* @var $offer \common\models\entity\offer\Offer */
?>

<?
$tabs = [
    [
        'label' => 'Описание',
        'active' => 1,
        'content' => $offer->description,
    ], [
        'label' => 'Описание (short)',
        'content' => $offer->description_short,
    ],[
        'label' => 'Описание (tips)',
        'content' => $offer->description_tips,
    ],[
        'label' => 'Описание (adv)',
        'content' => $offer->description_adv,
    ],[
        'label' => 'Описание (short)',
        'content' => $offer->description_short,
    ]
];

echo TabsX::widget([
    'bordered' => true,
    'items' => $tabs,
]);

?>
