<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 04.10.17
 * Time: 18:21

 * @var string $patternType
 * @var \common\models\entity\tag\Tag $tag
 * @var \yii\web\View $this
 */

use yii\helpers\Html;

?>

<form action="pattern">
    <?= Html::hiddenInput('', $tag->id, ['class' => 'js-tag-id']) ?>
    <?= Html::hiddenInput('', $patternType, ['class' => 'js-patternType']) ?>
    <div class="form-group">
        <?= Html::button('Сохранить', ['disabled' => true, 'class' => 'btn btn-primary js-pattern-save']) ?>
        <?= Html::textInput('pattern', $tag->name, [
            'class' => 'form-control js-pattern',
            'placeholder' => 'Введите правило и нажмите анализ',
        ]) ?>
        <small>фотосес|конны*прогулк-макияж</small>
        <div class="form-group">
            <?= Html::submitButton('Анализ', ['class' => 'btn btn-success js-pattern-analyze']) ?>
        </div>
    </div>
</form>
