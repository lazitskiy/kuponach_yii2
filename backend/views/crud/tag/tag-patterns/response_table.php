<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 03.10.17
 * Time: 20:00
 * @var array $matches
 */

use yii\helpers\Html;
use yii\helpers\Url;

//vvd($matches);
?>


<table class="kv-grid-table table table-bordered table-striped kv-table-wrap">
    <thead>
    <tr>
        <th>Оффер</th>
        <th>Текст</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($matches as $match): ?>
        <tr>
            <td><?= Html::a($match['offer']['id'], Url::to(['/offer', 'OfferSearch' => ['id' => $match['offer']['id']]]), ['target' => '_blank']) ?></td>
            <td class="popo">
                <?php
                $arTags = [];
                foreach ($match['offer']['tags'] as $tag) {
                    $url = Url::to(['/tag', 'TagSearch' => ['id' => $tag['id']]]);
                    $arTags[] = '<span class="small"><a href="' . $url . '" target="_blanks">' . $tag['name'] . '</a></span>';
                }
                ?>

                <b class="small">Теги: </b><?= implode(' / ', $arTags) ?><br/>
                <div class="text-active">
                    <b><?= $match['offer']['name'] ?></b><br/>
                    <?= $match['offer']['text'] ?>
                </div>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
