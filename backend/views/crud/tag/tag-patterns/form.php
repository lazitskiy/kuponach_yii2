<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 03.10.17
 * Time: 18:13
 *
 * @var \common\models\entity\tag\Tag $tag
 * @var \yii\web\View $this
 */


?>


<div class="offer-form">

    <?= \kartik\tabs\TabsX::widget([
        'bordered' => true,
        'items' => [
            [
                'label' => 'В названии',
                'active' => 1,
                'content' => $this->render('_form_tabs', [
                    'tag' => $tag,
                    'patternType' => \common\models\entity\tag\TagPattern::TYPE_NAME,
                ]),
            ], [
                'label' => 'В описании',
                'content' => $this->render('_form_tabs', [
                    'tag' => $tag,
                    'patternType' => \common\models\entity\tag\TagPattern::TYPE_DESCRIPTION,
                ]),
            ],
        ],
    ])
    ?>
</div>

<div class="js-response">
</div>
