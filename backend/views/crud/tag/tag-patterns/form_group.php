<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 03.10.17
 * Time: 18:13
 *
 * @var \common\models\entity\tag\Tag $tag
 * @var \yii\web\View $this
 */

use yii\helpers\ArrayHelper;
use common\models\entity\tag\Tag;
use yii\helpers\Html;

?>

    <form action="pattern">
        <?= Html::hiddenInput('', $tag->id, ['class' => 'js-tag-id']) ?>
        <div class="form-group">
            <?= \kartik\widgets\Select2::widget([
                'name' => 'js-tag-group',
                'value' => ArrayHelper::getColumn($tag->tagChildren, 'id'),
                'data' => ArrayHelper::map(Tag::find()->all(), 'id', 'name'),
                'options' => [
                    'class' => 'js-tag-group',
                    'multiple' => true,
                ],
            ]) ?>
        </div>
        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success js-group-save']) ?>
        </div>
    </form>

<?php
$js = <<<JS
    $("document").ready(function(){
        $('.js-group-save').click(function(){
            var button = $(this);
            var tagId = button.parents('form').find('.js-tag-id').val();
            var tagIdChildren = button.parents('form').find('.js-tag-group').val();
            
            $.getJSON('/tag/tag-group-save', {
                    tagId: tagId,
                    tagIdChildren: tagIdChildren
                },
                function(data) {
                    if (data.status == 'success'){
                        location.reload();
                    } else {
                        alert(data.text);                        
                    }
                }
            );
            
            return false;
        }); 
    });
JS;

$this->registerJs($js);
