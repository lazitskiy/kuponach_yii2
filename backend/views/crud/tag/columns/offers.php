<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 12.10.17
 * Time: 15:37
 * @var $tag \common\models\entity\tag\Tag
 */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\helpers\StringHelper;
use common\models\entity\offer\Offer;

$arInfo = [];
$tabs = [];
foreach ($tag->offers as $k => $offer) {

    $arTags = [];
    $offerTags = $offer->tags;
    ArrayHelper::multisort($offerTags, 'name');
    foreach ($offerTags as $offerTag) {
        $patterns = ArrayHelper::getColumn($offerTag->tagPatterns, 'pattern');
        $url = \yii\helpers\Url::to(['', 'TagSearch' => ['id' => $offerTag->id]]);
        $arTags[] = '<span class="small"><a href="' . $url . '">[' . $offerTag->id . '] ' . $offerTag->name . '</a>' . ($patterns ? ' [' . implode(',', $patterns) . ']' : '') . '</span>';
    }

    $tagPatterns = ArrayHelper::getColumn($tag->tagPatterns, 'pattern');

    $offer->sanitizeField();
    foreach (Offer::getTextFields() as $property) {
        if ($offer->$property) {
            if ($tagPatterns) {
                $tagPatterns = array_unique($tagPatterns);
                list($match, $text) = StringHelper::isSatisfiedBy(strip_tags($offer->$property), $tagPatterns[0]);
            } else {
                $text = strip_tags($offer->$property);
            }
            $a = Html::a($offer->id, Url::to(['/offer', 'OfferSearch' => ['id' => $offer->id]]), ['target' => '_blank']);
            $checkBox = Html::checkbox('offerId', $offer->is_tag_patterned, [
                'style' => 'position: absolute; margin: -16px 0 0 -16px;',
                'onclick' => 'javascript:$.get("/offer/set-is-tag-patterned", {offerId: this.value, value:+this.checked});',
                'title' => 'Оффер отработан/нет',
                'value' => $offer->id,
            ]);

            $text = "$checkBox [$a] <span class='js-wordstat-selected js-offer-a' data-offer-id='{$offer->id}'>$text</span>";

            $arInfo[$property][] = '
                                    <div >
                                        <b class="small">Теги: </b>' . Html::ul($arTags, ['encode' => false, 'style' => 'margin-bottom:0;list-style-type:none;']) . '
                                        <p>' . ($k + 1) . '. ' . $text . '</p>
                                    </div>';
        }
    }
}

foreach ($arInfo as $property => $item) {
    $tabs[] = [
        'label' => $property,
        //'active' => $property == 'name_short',
        'content' => implode('', $item),
    ];
}

if ($tabs) {
    echo \kartik\tabs\TabsX::widget([
        'bordered' => true,
        'items' => $tabs,
    ]);
}
