<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 12.10.17
 * Time: 16:27
 * @var $tag \common\models\entity\tag\Tag
 * @var $this \yii\web\View
 */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

?>

<?php if ($tags = $tag->getTagsRelated()): ?>
    <ul style="padding: 0; list-style-type: none">
        <?php foreach ($tags as $tag): ?>
            <li style="padding-bottom:8px;">
                <div class="text-nowrap">
                    <b><?= $tag->offer_count ?></b>
                    <a href="<?= Url::to(['', 'TagSearch' => ['id' => $tag->id]]) ?>">
                        [<?= $tag->id ?>] <?= $tag->name ?>
                    </a>
                    <?php if ($tag->is_group): ?>
                        [гр]
                    <?php endif; ?>

                    <?= \kartik\popover\PopoverX::widget([
                        'placement' => \kartik\popover\PopoverX::ALIGN_BOTTOM_LEFT,
                        'header' => 'Создать правило',
                        'content' => $this->render('../tag-patterns/form', ['tag' => $tag]),
                        'footer' => Html::button('View', ['class' => 'btn btn-primary']),
                        'toggleButton' => [
                            'style' => 'font-weight:bold;cursor:pointer;margin-left:4px;',
                            'title' => 'Создать правило',
                            'tag' => 'a',
                            'label' => '+',
                        ],
                        'options' => [
                            'class' => 'popover-w40',
                        ],
                        'pluginOptions' => [
                            'container' => 'body',
                        ],
                    ]); ?>
                </div>
                <?php if ($tagPatterns = $tag->tagPatterns): ?>
                    <small>[<?= implode(', ', ArrayHelper::getColumn($tagPatterns, 'pattern')) ?>]</small>
                <?php endif; ?>
            </li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>

