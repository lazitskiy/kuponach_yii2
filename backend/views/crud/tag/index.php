<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use common\helpers\StringHelper;
use common\models\entity\tag\Tag;
use common\models\entity\tag\TagPattern;

/* @var $this yii\web\View */
/* @var $searchModel common\models\entity\tag\TagSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$view = $this;
$this->title = 'Tags';
$this->params['breadcrumbs'][] = $this->title;


?>

<style>
    img {
        width: 200px;
        height: auto;
    }
</style>
<div class="tag-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="pull-left">
        <div>
            Показывать по:
            <a href="<?= Url::current(['per-page' => 1]) ?>">1</a>
            <a href="<?= Url::current(['per-page' => 3]) ?>">3</a>
            <a href="<?= Url::current(['per-page' => 5]) ?>">5</a>
            <a href="<?= Url::current(['per-page' => 10]) ?>">10</a>
            <a href="<?= Url::current(['per-page' => 15]) ?>">15</a>
            <a href="<?= Url::current(['per-page' => 20]) ?>">20</a>
        </div>
    </div>
    <div class="pull-right">
        <?php $showOffers = !($_GET['show-offers'] ?? 0); ?>
        <?php if ($showOffers): ?>
            <a href="<?= Url::current(['show-offers' => 1]) ?>">Показать отработанные офферы</a>
        <?php else: ?>
            <a href="<?= Url::current(['show-offers' => null]) ?>">Выключить отработанные офферы</a>
        <?php endif; ?>
        <br>
        <?php $showTags = !($_GET['show-tags'] ?? 0); ?>
        <?php if ($showTags): ?>
            <a href="<?= Url::current(['show-tags' => 1]) ?>">Показать отработанные теги</a>
        <?php else: ?>
            <a href="<?= Url::current(['show-tags' => null]) ?>">Выключить отработанные теги</a>
        <?php endif; ?>
    </div>
    <div class="clearfix"></div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,

        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'layout' => "{summary}\n{pager}\n{items}\n{pager}",
        'pjax' => false,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'refreshGrid' => true,
        ],
        'responsive' => true,
        'columns' => [
            [
                'attribute' => 'id',
                'width' => '40px',
            ], [
                'class' => \kartik\grid\EditableColumn::class,
                'attribute' => 'is_group',
                'width' => '60px',
                'value' => function (Tag $tag) {
                    return $tag->is_group ? 'Групповой' : 'Обычный';
                },
                'editableOptions' => [
                    'formOptions' => [
                        'action' => ['/tag/tag-save'],
                    ],
                    'inputType' => \kartik\editable\Editable::INPUT_DROPDOWN_LIST,
                    'data' => [1 => 'Групповой', 0 => 'Обычный'],
                ],
                'filter' => [1 => 'Да', 0 => 'Нет'],
                'filterWidgetOptions' => [
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ],
                'filterInputOptions' => ['placeholder' => 'Все'],
                'vAlign' => GridView::ALIGN_TOP,
            ], [
                'attribute' => 'name',
                'header' => 'Название/Правила',
                'width' => '140px',
                'contentOptions' => ['class' => 'text-nowrap'],
                'value' => function (Tag $tag) use ($view) {
                    $value[] = "<b>({$tag->getOffers()->count()})</b> " . Html::a($tag->name, Url::to(['', 'TagSearch' => ['id' => $tag->id]]));
                    $value[] = '<div>';
                    $value[] = \kartik\popover\PopoverX::widget([
                        'placement' => \kartik\popover\PopoverX::ALIGN_BOTTOM_LEFT,
                        'header' => 'Создать групповой',
                        'content' => $view->render('tag-patterns/form_group', ['tag' => $tag]),
                        'footer' => Html::button('View', ['class' => 'btn btn-primary']),
                        'toggleButton' => [
                            'class' => 'btn btn-primary',
                            'label' => 'Создать групповой',
                        ],
                        'options' => [
                            'class' => 'popover-w40',
                        ],
                        'pluginOptions' => [
                            'container' => 'body',
                        ],
                    ]);
                    $value[] = '</div>';

                    if ($tag->tagChildren) {
                        $value[] = '<hr/>';
                        $value[] = 'Состоит из:';
                        $value[] = Html::ul(ArrayHelper::map($tag->tagChildren, 'id', 'name'), [
                            'class' => 'list-unstyled',
                            'itemOptions' => [
                                'class' => 'small',
                            ],
                        ]);
                    }
                    $value[] = '<br/>';

                    $value[] = '<b>Правила:</b>';
                    $value[] = Html::ul(ArrayHelper::map($tag->tagPatterns, 'id', function (TagPattern $tagPattern) {
                        return "{$tagPattern->pattern} [$tagPattern->pattern_type]";
                    }), [
                        'class' => 'list-unstyled',
                        'itemOptions' => [
                            'class' => 'small',
                        ],
                    ]);
                    $value[] = \kartik\popover\PopoverX::widget([
                        'placement' => \kartik\popover\PopoverX::ALIGN_BOTTOM_LEFT,
                        'header' => 'Создать правило',
                        'content' => $view->render('tag-patterns/form', ['tag' => $tag]),
                        'footer' => Html::button('View', ['class' => 'btn btn-primary']),
                        'toggleButton' => [
                            'class' => 'btn btn-primary',
                            'label' => 'Создать правило',
                        ],
                        'options' => [
                            'class' => 'popover-w40',
                        ],
                        'pluginOptions' => [
                            'container' => 'body',
                        ],
                    ]);

                    return implode('', $value);
                },
                'format' => 'raw',
            ], [
                'attribute' => 'tags_related',
                'header' => 'Связанные',
                'contentOptions' => ['style' => 'width:220px;'],
                'value' => function ($tag) use ($view) {
                    return $view->render('columns/tags_related', [
                        'tag' => $tag,
                    ]);
                },
                'format' => 'raw',
            ], [
                'attribute' => 'offers',
                'header' => 'Офферы',
                'value' => function ($tag) use ($view) {
                    return $view->render('columns/offers', [
                        'tag' => $tag,
                    ]);
                },
                'format' => 'raw',
            ],
        ],
    ]); ?>
</div>


<?php
$this->registerJs($this->render('_wordstat/wordstat.js'));
echo $this->render('_wordstat/popup');

?>
