<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use common\helpers\StringHelper;
use common\models\entity\tag\Tag;
use common\models\entity\tag\TagPattern;

/* @var $this yii\web\View */
/* @var $searchModel common\models\entity\tag\TagSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$view = $this;
$this->title = 'Tags';
$this->params['breadcrumbs'][] = $this->title;


?>

<div class="tag-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="clearfix"></div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,

        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'layout' => "{summary}\n{pager}\n{items}\n{pager}",
        'pjax' => false,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'refreshGrid' => true,
        ],
        'responsive' => false,
        'columns' => [
            [
                'attribute' => 'id',
                'width' => '70px',
            ], [
                'attribute' => 'name',
                'width' => '140px',
                'contentOptions' => ['class' => 'text-nowrap'],
                'value' => function (Tag $tag) use ($view) {
                    $value[] = "<b>({$tag->getOffers()->count()})</b> " . Html::a($tag->name, Url::to(['', 'TagSearch' => ['id' => $tag->id]]));

                    return implode('', $value);
                },
                'format' => 'raw',
            ], [
                'class' => \kartik\grid\EditableColumn::class,
                'attribute' => 'name_where',
                'width' => '140px',
                'contentOptions' => ['class' => 'text-nowrap'],
                'editableOptions' => [
                    'formOptions' => [
                        'action' => ['/tag-seo/tag-save'],
                    ],
                    'placement' => \kartik\popover\PopoverX::ALIGN_TOP_LEFT,
                    'size' => 'lg',
                ],
                'format' => 'raw',
            ], [
                'class' => \kartik\grid\ExpandRowColumn::class,
                'width' => '50px',
                'header' => '',
                'value' => function ($model) {
                    return GridView::ROW_COLLAPSED;
                },
                'detailUrl' => Url::to(['seo-info']),
            ], [
                'class' => \kartik\grid\EditableColumn::class,
                'attribute' => 'seo_title',
                /*'contentOptions' => ['style' => 'width:220px;'],*/
                'editableOptions' => function (Tag $model, $key, $index) {
                    return [
                        'formOptions' => [
                            'action' => ['/tag-seo/tag-save'],
                        ],
                        'placement' => \kartik\popover\PopoverX::ALIGN_TOP_LEFT,
                        'size' => 'lg',
                        'afterInput' => function ($form, $widget) use ($model, $index) {
                            $word1 = "Купоны на {$model->name} {{city_in}}{{extra_word1}}. Акции и скидки до 70% {{extra_word2}}на {$model->name}";

                            return
                                Html::button('1', [
                                    'class' => 'btn btn-success',
                                    'onclick' => "javascript:$(this).parent().find('.kv-editable-input').val('$word1')",
                                ]) . ' ' .
                                'Сгенерировать</br>'.
                                '{{city_in}} уже имеет предлог. Пример: в Москве, в Ростове-На-Дону';
                        },
                    ];
                },

                'format' => 'raw',
            ], [
                'class' => \kartik\grid\EditableColumn::class,
                'attribute' => 'seo_h1',
                'format' => 'raw',
            ], [
                'class' => \kartik\grid\EditableColumn::class,
                'attribute' => 'seo_h1_sub',
                'format' => 'raw',
            ],
        ],
    ]); ?>
</div>


<?php
$this->registerJs($this->render('_wordstat/wordstat.js'));
echo $this->render('_wordstat/popup');

?>



