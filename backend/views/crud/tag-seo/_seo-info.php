<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 18.10.17
 * Time: 3:35
 *
 * @var \yii\web\View $this
 * @var \common\models\entity\tag\Tag $tag
 * @var \common\models\entity\seo\Seo $seo
 */

?>

<table class="table">
    <tr>
        <?php foreach (\yii\helpers\Json::decode($seo->seo_stat) as $word => $data): ?>
            <td valign="top" class="col-md-4">
                <b><?=$word?></b>
                <?= $this->render('/wordstat/_wordstat', [
                    'result' => $data['wordstat'],
                ]) ?>
            </td>
        <?php endforeach; ?>
    </tr>
    <tr>
        <?php foreach (\yii\helpers\Json::decode($seo->seo_stat) as $word => $data): ?>
            <td valign="top">
                    <?php foreach ($data['info'] as $url => $siteData): ?>
                        <div style="background-color: <?= $siteData['top']['color'] ?? '#000000' ?>;word-break: break-all;">
                            <?= $url ?>
                        </div>
                        <?= \kartik\tabs\TabsX::widget([
                            'bordered' => true,
                            'linkOptions' => [
                                'style' => 'padding:0 12px',
                            ],
                            'items' => [
                                [
                                    'label' => 'title',
                                    'active' => 0,
                                    'content' => '<small >' . ($siteData['meta']['title'] ?? '') . '</small>',
                                ], [
                                    'label' => 'description',
                                    'active' => 0,
                                    'content' => '<small>' . ($siteData['meta']['description'] ?? '') . '</small>',
                                ],
                            ],
                        ]) ?>
                    <?php endforeach; ?>
            </td>
        <?php endforeach; ?>
    </tr>
</table>
