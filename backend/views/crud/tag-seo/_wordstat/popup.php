<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 12.10.17
 * Time: 16:47
 */
?>
<style>
    .popover-w80 {
        max-width: 80%; /* Max Width of the popover (depending on the container!) */
        width: 80%; /* Max Width of the popover (depending on the container!) */
    }

    .popover-w60 {
        max-width: 80%; /* Max Width of the popover (depending on the container!) */
        width: 60%; /* Max Width of the popover (depending on the container!) */
    }

    .popover-w40 {
        max-width: 40%; /* Max Width of the popover (depending on the container!) */
        width: 40%; /* Max Width of the popover (depending on the container!) */
    }

    .js-wordstat-result td, .js-wordstat-result th {
        white-space: nowrap;
    }
</style>

<div id="popover-content" style="display: none">
    <form action="" class="hidden js-captcha-form">
        <div class="js-wordstat-captcha form-inline">
            <img src="" id="wordstat-captcha" alt="">
            <input type="text" class="form-control js-wordstat-captcha-answer-text">

            <button type="submit" class="btn btn-success js-wordstat-captcha-answer-button">Ввести</button>
        </div>
    </form>

    <form action="">
        <div class="form-group form-inline hidden js-tag-save-group">
            <input type="hidden" class="js-wordstat-offer-id">
            <input class="form-control js-tag-word">
            <button class="btn btn-success js-tag-save">Сохр</button>
        </div>
        <div class="form-group form-inline">
            <input type="text" class="form-control js-wordstat-word" name="hui">
            <button type="button" class="btn btn-success js-tag-add">+</button>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-success js-wordstat-analyze">Анализ wordstat</button>
            <div class="js-wordstat-result">
            </div>
        </div>
    </form>
</div>
