(function ($) {
    "use strict";
    $("document").ready(function () {

        $('.js-pattern-analyze').click(function () {
            var button = $(this);
            var buttonSavePattern = button.parents('form').find('.js-pattern-save');
            var pattern = button.parents('form').find('.js-pattern').val();
            var patternType = button.parents('form').find('.js-patternType').val();
            var tabId = button.parents('div.tab-pane').attr('id');

            button.html('Ждите').attr('disabled', true);
            $.get('/tag/pattern', {
                    pattern: pattern,
                    patternType: patternType
                },
                function (data) {
                    if (data.status == 'success') {
                        var html = $('a[href="#' + tabId + '"]').html().replace(/\s\(.+/, '');
                        var html = html + ' (' + data.count + ')';
                        $('a[href="#' + tabId + '"]').html(html);
                        $('.js-response').html(data.text);
                        button.html('Анализ').prop('disabled', false);
                        buttonSavePattern.prop('disabled', false);
                    } else {
                        alert(data.text);
                    }
                }
            );

            return false;
        });

        $('.js-pattern-save').click(function () {
            var button = $(this);
            var tagId = button.parents('form').find('.js-tag-id').val();
            var pattern = button.parents('form').find('.js-pattern').val();
            var patternType = button.parents('form').find('.js-patternType').val();

            $.getJSON('/tag/pattern-save', {
                    tagId: tagId,
                    pattern: pattern,
                    patternType: patternType
                },
                function (data) {
                    console.log(data);
                    if (data.status == 'success') {
                        location.reload();
                    } else {
                        alert(data.text);
                    }
                }
            );
        });

        function getSelectionText() {
            var text = "";
            var activeEl = document.activeElement;
            var activeElTagName = activeEl ? activeEl.tagName.toLowerCase() : null;
            if (
                (activeElTagName && activeElTagName == "textarea") ||
                (activeElTagName && activeElTagName == "input" && /^(?:text|search|password|tel|url)$/i.test(activeEl.type)) && (typeof activeEl.selectionStart == "number")
            ) {
                text = activeEl.value.slice(activeEl.selectionStart, activeEl.selectionEnd);
            } else if (window.getSelection) {
                text = window.getSelection().toString();
            }
            return text;
        }

        $('body').on('mouseup', '.js-wordstat-selected', function (e) {
            var offerId = $(this).closest('.js-offer-a').data('offer-id');

            var popOverSettings = {
                placement: 'left',
                html: true,
                trigger: 'manual',
                content: function () {
                    return $('#popover-content').html();
                }
            };
            var selectedText = getSelectionText();
            if (selectedText) {
                var el = $(this);

                //$('.popover').popover('hide');

                selectedText = 'купон ' + selectedText;
                $('.js-wordstat-result').html('');
                $('.js-wordstat-word').attr('value', selectedText);
                $('.js-wordstat-offer-id').attr('value', offerId);
                $('.js-wordstat-analyze').trigger('click');
                $(el).popover(popOverSettings);
                $(el).popover('show');
            }
        });

        // Закрыть все всплывашки
        $(document).on('click', function (e) {
            $('[data-toggle="popover"],[data-original-title]').each(function () {
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                    (($(this).popover('hide').data('bs.popover') || {}).inState || {}).click = false;  // fix for BS 3.3.6
                }
            });
        });


        $('body').on('click', '.js-wordstat-analyze', function () {
            var button = $(this);
            var word = button.parents('form').find('.js-wordstat-word').val();

            button.html('Ждите').attr('disabled', true);

            var captchaChecker = setInterval(function () {
                $.ajax({
                    url: '/wordstat/captcha-check',
                    processData: false
                }).always(function (data) {
                    if (data.src) {
                        $("#wordstat-captcha").attr("src", "data:image/png;base64," + data.src);
                        $('.js-captcha-form').removeClass('hidden');
                        clearInterval(captchaChecker);
                    }
                });
            }, 1000);

            $.get('/wordstat/get', {
                    word: word
                },
                function (data) {
                    if (data.status == 'success') {
                        clearInterval(captchaChecker);
                        $('.js-wordstat-analyze').html('Анализ wordstat').prop('disabled', false);
                        $('.js-wordstat-result').html(data.text);
                    } else {
                        alert(data.text);
                    }
                }
            );

            return false;
        });

        $('body').on('click', '.js-wordstat-captcha-answer-button', function () {
            var answer = $(this).parent().find('.js-wordstat-captcha-answer-text').val();
            if (!answer) {
                alert('Введи каптчу');

                return;
            }
            $('.js-captcha-form').addClass('hidden');
            $.get('/wordstat/captcha-process', {
                answer: answer
            });

            return false;
        });

        $('body').on('click', '.js-tag-add', function () {
            var button = $(this);
            var word = button.parents('form').find('.js-wordstat-word').val().replace('купон', '').trim();
            var tagInput = button.parents('form').find('.js-tag-word');
            var tagGroup = button.parents('form').find('.js-tag-save-group');

            tagInput.val(word);
            tagGroup.removeClass('hidden');
        });

        $('body').on('click', '.js-tag-save', function () {
            var button = $(this);
            var tagName = button.parents('form').find('.js-tag-word').val();
            var offerId = button.parents('form').find('.js-wordstat-offer-id').val();
            if (!tagName) {
                alert('Укажите тег');

                return false;
            }
            $.getJSON('/offer/set-tag', {
                    tagName: tagName,
                    offerId: offerId
                },
                function (data) {
                    if (data.status == 'success') {
                        window.location.reload();
                    } else {
                        alert(data.text);
                    }
                }
            );

            return false;

        });

    });
}(jQuery));
