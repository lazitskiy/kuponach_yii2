<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use common\helpers\StringHelper;
use common\models\entity\tag\Tag;
use common\models\entity\tag\TagPattern;

/* @var $this yii\web\View */
/* @var $searchModel common\models\entity\tag\TagSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$view = $this;
$this->title = 'Area';
$this->params['breadcrumbs'][] = $this->title;


?>

<div class="tag-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="clearfix"></div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,

        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'layout' => "{summary}\n{pager}\n{items}\n{pager}",
        'pjax' => false,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'refreshGrid' => true,
        ],
        'responsive' => false,
        'columns' => [
            [
                'attribute' => 'id',
                'width' => '70px',
            ], [
                'attribute' => 'city_id',
                'width' => '70px',
            ], [
                'attribute' => 'name',
            ], [
                'class' => \kartik\grid\EditableColumn::class,
                'attribute' => 'name_where',
                'width' => '140px',
                'contentOptions' => ['class' => 'text-nowrap'],
                'editableOptions' => [
                    'formOptions' => [
                        'action' => ['/area/area-save'],
                    ],
                    'placement' => \kartik\popover\PopoverX::ALIGN_TOP_LEFT,
                    'size' => 'lg',
                ],
                'format' => 'raw',
            ], [
                'attribute' => 'slug',
                'value' => function (\common\models\entity\company\CompanyAddressCityArea $area) {
                    return $area->slug . '-o';
                },
            ],
        ],
    ]); ?>
</div>
