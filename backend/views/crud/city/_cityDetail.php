<?php

use kartik\tabs\TabsX;

/* @var $this yii\web\View */
/* @var $city \common\models\entity\city\City */
/* @var $addresses array */
?>

<?php
$tabs = [];
foreach ($addresses as $i => $address) {
    $tabs[] = [
        'label' => $address['value'],
        'active' => $i == 0,
        'content' => '<pre>' . print_r($address['data'], true) . '</pre>',
    ];
}

echo TabsX::widget([
    'bordered' => true,
    'items' => $tabs,
]);

?>
