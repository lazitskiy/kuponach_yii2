<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\entity\city\CitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cities';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="city-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create City', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,

        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'refreshGrid' => true,
        ],
        'responsive' => true,
        'columns' => [
            [
                'attribute' => 'id',
                'width' => '60px',
                'vAlign' => GridView::ALIGN_MIDDLE,
                'hAlign' => GridView::ALIGN_CENTER,
            ], [
                'class' => \kartik\grid\EditableColumn::class,
                'attribute' => 'is_active',
                'format' => 'boolean',
                'editableOptions' => [
                    'inputType' => \kartik\editable\Editable::INPUT_DROPDOWN_LIST,
                    'data' => [0 => 'Деактивировать', 1 => 'Активировать'],
                    'formOptions' => [
                        'action' => ['/city/city-save'],
                    ],
                ],
                'content' => function ($model) {
                    return $model->is_active ? '<span class="glyphicon glyphicon-ok text-success"></span>' : '<span class="glyphicon glyphicon-remove text-danger"></span>';
                },
                'width' => '60px',
                'vAlign' => GridView::ALIGN_MIDDLE,
                'hAlign' => GridView::ALIGN_CENTER,
            ], [
                'class' => \kartik\grid\EditableColumn::class,
                'attribute' => 'is_important',
                'format' => 'boolean',
                'editableOptions' => [
                    'inputType' => \kartik\editable\Editable::INPUT_DROPDOWN_LIST,
                    'data' => [0 => 'Исключить', 1 => 'Включить'],
                    'formOptions' => [
                        'action' => ['/city/city-save'],
                    ],
                ],
                'content' => function ($model) {
                    return $model->is_important ? '<span class="glyphicon glyphicon-ok text-success"></span>' : '<span class="glyphicon glyphicon-remove text-danger"></span>';
                },
                'width' => '60px',
                'vAlign' => GridView::ALIGN_MIDDLE,
                'hAlign' => GridView::ALIGN_CENTER,
            ], [
                'class' => kartik\grid\ExpandRowColumn::class,
                'width' => '50px',
                'value' => function ($model, $key, $index, $column) {
                    return GridView::ROW_COLLAPSED;
                },
                /*'detail' => function ($model, $key, $index, $column) {
                    return Yii::$app->controller->renderPartial('_cityDetail', ['city' => $model]);
                },*/
                'detailUrl' => \yii\helpers\Url::to(['detail']),
                'headerOptions' => ['class' => 'kartik-sheet-style'],
                'expandOneOnly' => true,
            ],
            'name',
            [
                'class' => \kartik\grid\EditableColumn::class,
                'attribute' => 'name_in',
                'editableOptions' => [
                    'formOptions' => [
                        'action' => ['/city/city-save'],
                    ],
                    'placement' => \kartik\popover\PopoverX::ALIGN_TOP_LEFT,
                    'size' => 'lg',
                ],
                'format' => 'raw',
            ],
            ['class' => \kartik\grid\EditableColumn::class,
            'attribute' => 'slug',
            'readonly' => function ($city, $key, $index, $widget) {
                /** @var \common\models\entity\city\City $city */

                return $city->isDefaultCity() || !empty($city->slug);
            },
            'editableOptions' => function ($city, $key, $index) {
                return [
                    'afterInput' => function ($form, $widget) use ($city) {
                        /** @var \common\models\entity\city\City $city */
                        if (!$city->isDefaultCity()) {
                            return "Предположительное значение: <b>" . \common\helpers\Inflector::slug($city->name) . "</b>";
                        }
                    },
                    'formOptions' => [
                        'action' => ['/city/city-save'],
                    ],
                ];
            },
        ],
        'fias_id',
        'region_fias_id',
        //'region_with_type',
        //'data:ntext',
        //'created_at',
        //'updated_at',

    ],
    ]); ?>
</div>
