<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 05.10.17
 * Time: 16:08
 * @var array $result
 */

use yii\helpers\Html;
use yii\helpers\Url;

$leftColumn = $result['includingPhrases'] ?? [];
$rightColumn = $result['phrasesAssociations'] ?? [];

?>

<table class="kv-grid-table table table-bordered table-striped kv-table-wrap">
    <thead>
    <tr>
        <th>(<?=$result['impressions']?>) Всего + синонимы </th>
        <th>Что ищут с этим</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>
            <?php foreach ($leftColumn as $word): ?>
                <b><?= $word['impressions'] ?></b> <?= $word['words'] ?> <br/>
            <?php endforeach; ?>
        </td>
        <td>
            <?php foreach ($rightColumn as $word): ?>
                <b><?= $word['impressions'] ?></b> <?= $word['words'] ?> <br/>
            <?php endforeach; ?>
        </td>
    </tr>
    </tbody>
</table>
