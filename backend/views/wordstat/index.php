<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 05.10.17
 * Time: 15:13
 */

$view = $this;
$this->title = 'Tags';
$this->params['breadcrumbs'][] = $this->title;
?>


<?php
$js = <<<JS
    $("document").ready(function(){
        $('.js-wordstat-captcha-answer-button').click(function(){
            var answer = $('.js-wordstat-captcha-answer-text').val();
            if (!answer) {
                alert('Введи каптчу');
                
                return;
            }
            $('.js-captcha-form').addClass('hidden');
            $.get('/wordstat/captcha-process', { 
                answer: answer 
            });
            
            return false;
        })
    });
JS;

$this->registerJs($js);
?>

<form action="" class="hidden js-captcha-form">
    <div class="js-wordstat-captcha form-inline">
        <img src="" id="wordstat-captcha" alt="">
        <input type="text" class="form-control js-wordstat-captcha-answer-text">

        <button type="submit" class="btn btn-success js-wordstat-captcha-answer-button">Ввести</button>
    </div>
</form>

<form action="">
    <div class="form-group">
        <input type="text" class="form-control js-wordstat-word" name="hui" value="кеды">
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-success js-wordstat-analyze">Анализ wordstat</button>
        <div class="js-wordstat-result">
        </div>
    </div>
</form>

<?php
$js = <<<JS
    $("document").ready(function(){
        $('.js-wordstat-analyze').click(function() {
            var button = $(this);
            var word = button.parents('form').find('.js-wordstat-word').val();
            
            button.html('Ждите').attr('disabled', true);
            
            var captchaChecker = setInterval(function () {
                $.ajax({ 
                    url : '/wordstat/captcha-check', 
                    processData : false
                }).always(function(data){
                    if (data.src) {
                        $("#wordstat-captcha").attr("src", "data:image/png;base64," + data.src);
                        $('.js-captcha-form').removeClass('hidden');
                        clearInterval(captchaChecker);
                    }
                });
            }, 1000);
            
            $.get('/wordstat/get', {
                    word: word
                },
                function(data) {
                    if (data.status == 'success') {
                        clearInterval(captchaChecker);
                        $('.js-wordstat-result').html(data.text);
                        
                        button.html('Анализ wordstat').prop('disabled', false);
                    } else {
                        alert(data.text);                        
                    }
                }
            );
            
            return false;
        });              
    });
JS;

$this->registerJs($js);
?>
