<?php
return [
    'language' => 'ru-RU',
    'timeZone' => 'UTC',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'modules' => [
        'user' => [
            'class' => \dektrium\user\Module::class,
            'modelMap' => [
                'User' => \common\models\entity\user\User::class,
            ],
            'controllerMap' => [
                //'registration' => \frontend\controllers\user\RegistrationController::class,
                //'settings' => \frontend\controllers\user\IndexController::class,
                //'question' => \frontend\controllers\user\QuestionController::class,
            ],
            'admins' => ['lazitskiy'],
            'urlRules' => [
                '/' => 'settings/index',
                'question' => 'question/index',
            ]
            //'urlPrefix' => null
        ],
    ],
    'components' => [
        'cache' => [
            'class' => \yii\caching\FileCache::class,
        ],
        'mailer' => [
            'class' => \yii\swiftmailer\Mailer::class,
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp-pulse.com',
                'username' => 'info@theta-dar.ru',
                'password' => 'WoDGKaBdmDb',
                'port' => '465',
                'encryption' => 'ssl',
            ],
        ],
        'sendpulse' => [
            'class' => \sevenfloor\sendpulse\SendPulse::class,
            'userId' => 'c45c68feda9c5c4c87fc521d92e35043',
            'secret' => 'd8a82f41f7d50bfc177d6fbf17a9677b',
            'storageType' => 'session',
        ],
        'sentry' => [
            'class' => \mito\sentry\Component::class,
            'dsn' => 'https://1e3914eaf5ed4f70a46c95dea643e54c:224817c31bf249a19f596f9cf04d0c11@sentry.io/186507', // private DSN
            //'environment' => YII_DEBUG ? 'local' : 'production', // if not set, the default is `production`
            'jsNotifier' => true, // to collect JS errors. Default value is `false`
            /*'jsOptions' => [ // raven-js config parameter
                'whitelistUrls' => [ // collect JS errors from these urls
                    'http://staging.my-product.com',
                    'https://my-product.com',
                ],
            ],*/
        ],
        // тут используется другая либа console/controllers/SystemController.php:190
        'dadataSuggestApi' => [
            'class' => \skeeks\yii2\dadataSuggestApi\DadataSuggestApi::class,
            'authorization_token' => 'e39e431365f7513da6f8bd16d5c8c23f0a0f4394',
            'timeout' => 12,
        ],
    ],
];
