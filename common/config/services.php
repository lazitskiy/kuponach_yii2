<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 05.08.17
 * Time: 3:26
 */

$container = Yii::$container;

$container->setSingleton(\common\services\UserService::class);
$container->setSingleton(\common\services\GeoService::class);
$container->setSingleton(\common\services\email\EmailService::class);
