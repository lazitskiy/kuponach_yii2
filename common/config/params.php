<?php
return [
    'adminEmail' => 'info@kuponach.ru',
    'supportEmail' => 'support@kuponach.ru',
    'user.passwordResetTokenExpire' => 3600,
    'emailService' => [
        'from' => 'Купонач. Служебное сообщение',
        'email' => 'info@kuponach.ru',
    ],
];
