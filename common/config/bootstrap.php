<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');

function vvtr($data)
{
    echo '<pre>';
    print_r($data);
}

function vvd($data)
{
    echo '<pre>';
    print_r($data);
    die();
}

function vvr($data)
{
    echo '<pre>';
    print_r(var_dump($data));
    die();
}
