<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 05.08.17
 * Time: 1:39
 */

namespace common\components;


use mito\sentry\Component;
use sevenfloor\sendpulse\SendPulse;
use skeeks\yii2\dadataSuggestApi\DadataSuggestApi;

/**
 * Class Application
 * @package common\components
 */
class Application extends \yii\web\Application
{
    /**
     * @return object|View
     */
    public function getView()
    {
        return $this->get('view');
    }

    public function getApplicationName()
    {
        return $this->name;
    }

    /**
     * @return Component
     */
    public function getSentry()
    {
        return $this->sentry;
    }

    /**
     * @return SendPulse
     */
    public function getSendpulse()
    {
        return $this->sendpulse;
    }

    /**
     * @return DadataSuggestApi
     */
    public function getDadata()
    {
        return $this->dadataSuggestApi;
    }
}
