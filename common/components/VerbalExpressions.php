<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 09.10.17
 * Time: 20:38
 */

namespace common\components;


class VerbalExpressions extends \VerbalExpressions\PHPVerbalExpressions\VerbalExpressions
{
    public function _or($value)
    {


        if (strpos($this->prefixes, '(') === false) {
            $this->prefixes .= '(?:';
        }

        if (strpos($this->suffixes, ')') === false) {
            $this->suffixes .=  ')';
        }

        $this->add( ')|(?:');

        if ($value) {
            $b1 = ($value[0] == '_') ? '\b' : '';
            $b2 = ((bool)strpos($value, '_', 1)) ? '\b' : '';
            $value = str_replace('_', '', $value);

            $this->add($b1.$value.$b2);
        }

        return $this;
    }

    public function then($value)
    {
        $b1 = ($value[0] == '_') ? '\b' : '';
        $b2 = ((bool)strpos($value, '_', 1)) ? '\b' : '';

        $value = str_replace('_', '', $value);

        return $this->add('(?:' . $b1 . self::sanitize($value) . $b2 . ')');
    }
}
