<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 05.08.17
 * Time: 10:47
 */

namespace common\components;


class ActiveForm extends \yii\widgets\ActiveForm
{
    public $enableClientValidation = false;

    public $enableAjaxValidation = true;
}
