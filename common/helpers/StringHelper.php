<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 12.09.17
 * Time: 17:22
 */

namespace common\helpers;


use common\components\VerbalExpressions;
use yii\base\ErrorException;

class StringHelper
{
    public static function normalizeUrl($url)
    {
        $url = preg_replace("#^[^:/.]*[:/]+#i", "", $url);
        $url = preg_replace('/^www\./', '', $url);
        $url = 'http://' . $url;

        return $url;
    }

    public static function highlight($text, $words = [])
    {
        if (!is_array($words)) {
            $words = (array)$words;
        }

        foreach ($words as $word) {
            try {
                $text = preg_replace('~' . preg_quote($word) . '~uis', '<span style="background-color:#FFFF66; color:#FF0000;">\\0</span>', $text);
            } catch (ErrorException $e) {
                $text = $word;
            }
        }

        return $text;
    }

    /**
     * @param $text string конной прогулки Профессиональная фотосессия в любом, двоих или компании до 4 человек с , прической
     * @param $rule string фотосес|конны*прогулк-макияж
     * @return array
     */
    public static function isSatisfiedBy($text, $rule)
    {
        $regex = new VerbalExpressions();
        $regex->addModifier('uis');


        $splits = preg_split('/([|*-])/uis', $rule, -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);

        foreach ($splits as $k => $split) {
            if ($k > 0) {
                if ($split == '-' || $splits[$k - 1] == '-' || $splits[$k - 1] == '|') {
                    continue;
                }
            }

            if ($split == '*') {
                $regex->anything();
            } elseif ($split == '|') {
                $regex->_or($splits[$k + 1]);
            } else {
                $regex->then($split);
            }
        }
        $result = $regex->test($text);
        //vvd(( [$regex, $text, $result, $regex->getRegex()]));
        if ($result) {
            // ПОдсвечиваем
            if (preg_match_all($regex->getRegex(), $text, $matches)) {
                $text = static::highlight($text, $matches[0]);
            }
        }

        if ($result) {
            /**
             * Это минус слова
             */
            foreach ($splits as $k => $split) {
                if ($split == '-') {
                    $word = $splits[$k + 1];
                    if (preg_match('/' . $word . '/uis', $text)) {
                        $result = false;
                        break;
                    }
                }
            }
        }

        return [$result, $text];
    }
}
