<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 30.09.17
 * Time: 18:53
 */

namespace common\helpers;


use yii\helpers\Url;

class UrlHelper extends Url
{
    public static function getCityIndexUrl($slug)
    {
        $hostInfo = static::getUrlManager()->getHostInfo();
        $url = parse_url($hostInfo);

        $new = $url['scheme'] . '://' . preg_replace('/^([\w+\-]+\.)?(.+\.)(.+)$/', ($slug ? $slug . '.' : '') . "$2$3", $url['host']);

        return $new;
    }

    /**
     * @return string|null
     */
    public static function getSubdomain()
    {
        $hostInfo = static::getUrlManager()->getHostInfo();
        $url = parse_url($hostInfo);
        if (preg_match('/^(.*?)\.(.*?)\.(.*)$/uis', $url['host'], $matches)) {
            return $matches[1];
        }
        return null;
    }
}
