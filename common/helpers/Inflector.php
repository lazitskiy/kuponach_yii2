<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 25.09.17
 * Time: 0:26
 */

namespace common\helpers;


class Inflector extends \yii\helpers\Inflector
{
    CONST TRANSLITERATE_RUS = 'Russian-Latin/BGN';

    public static function slug($string, $replacement = '-', $lowercase = true)
    {
        $string = static::transliterate($string, static::TRANSLITERATE_RUS);
        $string = preg_replace('/[^a-zA-Z0-9=\s—–-]+/u', '', $string);
        $string = preg_replace('/[=\s—–-]+/u', $replacement, $string);
        $string = trim($string, $replacement);

        return $lowercase ? strtolower($string) : $string;
    }

}
