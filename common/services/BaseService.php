<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 05.08.17
 * Time: 3:28
 */

namespace common\services;


use common\traits\base\AddErrorAwareTrait;
use common\traits\base\ApplicationAwareTrait;
use yii\base\BaseObject;

class BaseService extends BaseObject
{
    use AddErrorAwareTrait;
    use ApplicationAwareTrait;
}
