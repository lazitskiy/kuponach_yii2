<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 18.10.17
 * Time: 3:41
 */

namespace common\services\internal;


use common\services\BaseService;
use RubtsovAV\YandexWordstatParser\Browser\ReactPhantomJs;
use RubtsovAV\YandexWordstatParser\Captcha\Image;
use RubtsovAV\YandexWordstatParser\Parser;
use RubtsovAV\YandexWordstatParser\Query;
use RubtsovAV\YandexWordstatParser\YandexUser;

class WordstatService extends BaseService
{
    /**
     * @var Parser
     */
    protected $parser;

    /**
     * WordstatService constructor.
     * @param string $runtimeDir
     * @param array $config
     */
    public function __construct($runtimeDir, array $config = [])
    {
        parent::__construct($config);

        $captcha = $runtimeDir . '/captcha.jpg';
        if (file_exists($captcha)) {
            unlink($captcha);
        }

        $yandexUser = new YandexUser('anikusha88', 'Ghbytcb100Nso', $runtimeDir);
        //$proxy = new HttpProxy('1.179.198.17', 8080);

        $bin = YII_ENV_DEV ? '/usr/local/bin/phantomjs' : 'phantomjs';
        $browser = new ReactPhantomJs();
        $browser->setPhantomJsPath($bin);
        //$browser->setProxy($proxy); // optional
        $browser->setTimeout(20);   // in seconds (120 by default)

        $browser->setCaptchaSolver(function ($captcha) use ($runtimeDir) {

            /** @var Image $captcha */
            $image = file_get_contents($captcha->getImageUri());
            file_put_contents($runtimeDir . '/captcha.jpg', $image);
            file_put_contents($runtimeDir . '/captchaAnswer.txt', '');

            echo "The captcha image was save to captcha.jpg. Write the answer in captchaAnswer.txt\n";
            $answer = '';
            while (!$answer) {
                $answer = file_get_contents($runtimeDir . '/captchaAnswer.txt');
                $answer = trim($answer);
                sleep(1);
            }
            echo "The captcha answer is '$answer'\n";
            $captcha->setAnswer($answer);

            return true;
        });

        $this->parser = new Parser($browser, $yandexUser);
    }

    /**
     * @param $word
     * @return array
     */
    public function getStat($word)
    {
        $query = new Query($word);
        $result = $this->parser->query($query);

        return $result->toArray();
    }

}
