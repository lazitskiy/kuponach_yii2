<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 05.08.17
 * Time: 11:00
 */

namespace common\services;


use common\models\entity\user\Profile;
use common\models\entity\user\User;
use common\traits\base\ApplicationAwareTrait;
use dektrium\user\helpers\Password;

class UserService extends BaseService
{
    use ApplicationAwareTrait;

    /**
     * @var User
     */
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @param $email
     * @return User|\dektrium\user\models\User
     */
    public function getByUsernameOrEmail($email)
    {
        return $this->user->finder->findUserByUsernameOrEmail($email);
    }

    /**
     * @param $email
     * @return User|\dektrium\user\models\User
     */
    public function emailExists($email)
    {
        return $this->user->finder->findUserByEmail($email);
    }

    /**
     * @param $name
     * @param $email
     * @param $phone
     * @return bool|User
     */
    public function register($name, $email, $phone)
    {
        $user = new User([
            'email' => $email,
            'confirmed_at' => time(),
            'password' => Password::generate(8),
        ]);

        $profile = new Profile([
            'name' => $name,
            'phone' => $phone,
        ]);

        $user->setProfile($profile);

        try {
            return $this->getApplication()->getDb()->transaction(function () use ($user) {
                $user->save();

                return $user;
            });
        } catch (\Exception $e) {
            $this->addErrors([$e->getMessage()]);

            return false;
        }
    }
}
