<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 16.10.17
 * Time: 23:56
 */

namespace common\services;


use common\helpers\StringHelper;
use common\models\entity\offer\Offer;
use common\models\entity\tag\Tag;
use common\models\entity\tag\TagPattern;

class TagMatchService
{
    /**
     * @var TagPattern[]
     */
    protected $tagPatterns;

    public function __construct()
    {
        $this->tagPatterns = TagPattern::find()
            ->with(['tagParents'])
            ->all();
    }

    /**
     * К офферу прицепляем все теги, начиная с дочерних и рекурсивно вверх
     *
     * @param Offer $offer
     * @return array
     */
    public function getMatchedTagIds(Offer $offer)
    {
        $tagIds = [];

        $tree = function (array $tags) use (&$tree, &$tagIds) {
            foreach ($tags as $tag) {
                $tagIds[$tag->id] = $tag->id;
                if ($tag->tagParents) {
                    $tree($tag->tagParents);
                }
            }
        };

        $offer->sanitizeField();
        foreach ($this->tagPatterns as $tagPattern) {
            foreach (Offer::getTextFields($tagPattern->pattern_type) as $textField) {

                list($result, $textMatch) = StringHelper::isSatisfiedBy($offer->$textField, $tagPattern->pattern);
                if ($result) {
                    $tagIds[$tagPattern->tag_id] = $tagPattern->tag_id;

                    if ($tagParents = $tagPattern->tagParents) {
                        $tree($tagPattern->tagParents);
                    }
                }
            }
        }

        return $tagIds;
    }
}
