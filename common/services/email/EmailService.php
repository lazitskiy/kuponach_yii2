<?php

namespace common\services\email;


use common\models\entity\offer\Offer;
use common\models\entity\operator\Operator;
use common\services\BaseService;
use mito\sentry\Component;
use sevenfloor\sendpulse\SendPulse;
use yii\helpers\ArrayHelper;

class EmailService extends BaseService
{
    /**
     * @var SendPulse
     */
    private $sender;

    /**
     * @var Component
     */
    private $sentry;

    private $arEmail;

    private $layoutFile = __DIR__ . '/views/layout.php';
    private $viewsDir = __DIR__ . '/views';


    public function init()
    {
        $this->sender = $this->getApplication()->getSendpulse();
        $this->sentry = $this->getApplication()->getSentry();

        $this->arEmail = [
            'html' => '',
            'text' => '',
            'subject' => '',
            'from' => [
                'name' => $this->getApplication()->params['emailService']['from'],
                'email' => $this->getApplication()->params['emailService']['email'],
            ],
            'to' => [],
        ];
    }

    private static function getCorporateEmailTo()
    {
        return [
            [
                'name' => \Yii::$app->params['emailService']['from'],
                'email' => \Yii::$app->params['emailService']['email'],
            ],
        ];
    }

    private static function getLazitskiyTo()
    {
        return [
            [
                'name' => 'Василий Лазицкий',
                'email' => 'lazitskiy@gmail.com',
            ], /*[
                'name' => 'Анна Лазицкая',
                'email' => 'lazitskaya@gmail.com',
            ], [
                'name' => \Yii::$app->params['emailService']['from'],
                'email' => \Yii::$app->params['emailService']['email'],
            ],*/
        ];
    }

    private function render($view, $params = [])
    {
        $content = \Yii::$app->getView()->renderFile($this->viewsDir . '/' . $view . '.php', $params);

        return \Yii::$app->getView()->renderFile($this->layoutFile, ['content' => $content], $this);
    }

    public function newCityDetected(Operator $operator, $newCities)
    {
        $subject = $operator->name . ' Новые города';
        $message = 'Есть новые города, необходимо отработать в ручную перед парсингом <br/>';
        $str = implode('<br/>', $newCities);

        return $this->notifyAdmin($message . $str, $subject);
    }

    /**
     * @param Offer[] $offers
     * @return bool
     */
    public function sendOfferUntagged($offers)
    {
        $subject = 'Есть офферы без тегов';
        $message = 'Есть офферы без тегов, необходимо их сделать <br/>';

        $str = implode('<br/>', ArrayHelper::getColumn($offers, 'id'));

        return $this->notifyAdmin($message . $str, $subject);
    }

    /**
     * @param array $data
     * @return bool
     */
    public function sendCityNotRecognized($data)
    {
        $subject = 'Город не распознан';
        $message = 'Город в адресе из дадаты добавился в базу, проверить<br/>';

        $str = "<pre>" . print_r($data, true) . "</pre>";

        return $this->notifyAdmin($message . $str, $subject);
    }

    public function offerUntagged()
    {

    }

    /**
     * Простая уведомилялка админа
     * @param $message
     * @param $subject
     * @return bool
     */
    protected function notifyAdmin($message, $subject = 'Системное уведомление')
    {
        $templateName = 'system';

        $html = $this->render($templateName, [
            'message' => $message,
        ]);

        $this->arEmail = array_merge($this->arEmail, [
            'html' => $html,
            'subject' => $subject,
            'to' => self::getCorporateEmailTo(),
        ]);

        $request = $this->sender->smtpSendMail($this->arEmail);

        if (isset($request->is_error)) {
            $this->sentry->captureException(new SendPulseException($request->message), [
                'tags' => [
                    'subject' => $subject,
                    'message' => $message,
                ],
            ]);

            return false;
        }

        return true;
    }
}
