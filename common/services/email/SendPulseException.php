<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 11.07.17
 * Time: 20:22
 */

namespace common\services\email;


use yii\base\Exception;

class SendPulseException extends Exception
{
    /**
     * @return string the user-friendly name of this exception
     */
    public function getName()
    {
        return 'SendPulseException';
    }
}
