<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 10.08.17
 * Time: 19:49
 */

namespace common\services;


use common\models\entity\city\City;
use common\models\entity\company\CompanyAddressCityArea;
use common\models\entity\company\CompanyAddressCityDistrict;
use common\models\entity\company\CompanyAddressCityMetro;
use common\models\entity\company\CompanyAddressCityStreet;
use common\traits\base\DadataAwareTrait;
use skeeks\yii2\dadataSuggestApi\DadataSuggestApi;

class GeoService extends BaseService
{
    use DadataAwareTrait;

    // Это Москва
    const DEFAULT_DADATA_RESPONSE = '{"location":{"value":"178.219.186.12","unrestricted_value":null,"data":{"postal_code":null,"country":"Россия","region_fias_id":"0c5b2444-70a0-4932-980c-b4dc0d3f02b5","region_kladr_id":"7700000000000","region_with_type":"г Москва","region_type":"г","region_type_full":"город","region":"Москва","area_fias_id":null,"area_kladr_id":null,"area_with_type":null,"area_type":null,"area_type_full":null,"area":null,"city_fias_id":"0c5b2444-70a0-4932-980c-b4dc0d3f02b5","city_kladr_id":"7700000000000","city_with_type":"г Москва","city_type":"г","city_type_full":"город","city":"Москва","city_area":null,"city_district_fias_id":null,"city_district_kladr_id":null,"city_district_with_type":null,"city_district_type":null,"city_district_type_full":null,"city_district":null,"settlement_fias_id":null,"settlement_kladr_id":null,"settlement_with_type":null,"settlement_type":null,"settlement_type_full":null,"settlement":null,"street_fias_id":null,"street_kladr_id":null,"street_with_type":null,"street_type":null,"street_type_full":null,"street":null,"house_fias_id":null,"house_kladr_id":null,"house_type":null,"house_type_full":null,"house":null,"block_type":null,"block_type_full":null,"block":null,"flat_type":null,"flat_type_full":null,"flat":null,"flat_area":null,"square_meter_price":null,"flat_price":null,"postal_box":null,"fias_id":"0c5b2444-70a0-4932-980c-b4dc0d3f02b5","fias_level":"1","kladr_id":"7700000000000","capital_marker":"0","okato":"45000000000","oktmo":null,"tax_office":"7700","tax_office_legal":"7700","timezone":null,"geo_lat":null,"geo_lon":null,"beltway_hit":null,"beltway_distance":null,"qc_geo":null,"qc_complete":null,"qc_house":null,"history_values":null,"unparsed_parts":null,"source":null,"qc":null}}}';

    /**
     * @var DadataSuggestApi
     */
    protected $dadata;

    public function __construct()
    {
        $this->dadata = $this->getDadata();
    }

    /**
     * @param City $city
     * @param string|null $cityAreaName Округ. В Москве это ЗАО, ЮВАЮ
     * @param string|null $cityDistrictName Района
     * @param string|null $cityStreetName Улица
     * @return array
     */
    public function mapDadataToLocalIds(City $city, $cityAreaName = null, $cityDistrictName = null, $cityDistrictType = null, $cityStreetName = null, $cityStreetType = null, $metros)
    {
        $cityAreaId = null;
        if ($cityAreaName) {
            $cityArea = CompanyAddressCityArea::findOne(['name' => $cityAreaName, 'city_id' => $city->id]);
            if (!$cityArea) {
                $cityArea = new CompanyAddressCityArea([
                    'city_id' => $city->id,
                    'name' => $cityAreaName,
                ]);
                $cityArea->save();
            }
            $cityAreaId = $cityArea->id;
        }

        $cityDistrictId = null;
        if ($cityDistrictName) {
            $cityDistrict = CompanyAddressCityDistrict::findOne(['name' => $cityDistrictName, 'city_id' => $city->id]);
            if (!$cityDistrict) {
                $cityDistrict = new CompanyAddressCityDistrict([
                    'city_id' => $city->id,
                    'name' => $cityDistrictName,
                    'name_full' => $cityDistrictType,
                ]);
                $cityDistrict->save();
            }
            $cityDistrictId = $cityDistrict->id;
        }

        $cityStreetId = null;
        if ($cityStreetName) {
            $cityStreet = CompanyAddressCityStreet::findOne(['name' => $cityStreetName, 'city_id' => $city->id]);
            if (!$cityStreet) {
                $cityStreet = new CompanyAddressCityStreet([
                    'city_id' => $city->id,
                    'name' => $cityStreetName,
                    'name_full' => $cityStreetType,
                ]);
                $cityStreet->save();
            }
            $cityStreetId = $cityStreet->id;
        }

        $metroIds = [];
        if ($metros) {
            foreach ($metros as $metroData) {
                $metro = CompanyAddressCityMetro::findOne(['name' => $metroData['name'], 'city_id' => $city->id]);
                if (!$metro) {
                    $metro = new CompanyAddressCityMetro([
                        'city_id' => $city->id,
                        'name' => $metroData['name'],
                        'line' => $metroData['line'],
                    ]);
                    $metro->save();
                }
                $metroIds[$metro->id] = $metro->id;
            }
        }

        return [$cityAreaId, $cityDistrictId, $cityStreetId, array_values($metroIds)];
    }

    #####################

    /**
     * @param $ip
     * @return City
     */
    public function getCityByIp($ip)
    {
        $response = $this->dadata->detectAddressByIp($ip);

        $data = $response->data['location']['data'] ?? null;
        if ($response->isError || !$data) {
            $data = json_decode(static::DEFAULT_DADATA_RESPONSE, true)['location']['data'];
        }

        $city = City::getRepository()->getByFiasId($data['fias_id']);
        if (!$city) {
            $city = $this->saveCity($data);
        }

        return $city;
    }

    public function saveCityFromDadataString(string $dadata)
    {
        $data = json_decode($dadata, true);
        if (!$data) {
            $data = json_decode(static::DEFAULT_DADATA_RESPONSE, true)['location']['data'];
        }

        $city = City::getRepository()->getByFiasId($data['fias_id']);
        if (!$city) {
            $city = $this->saveCity($data);
        }

        return $city;
    }

    /**
     * @param $letter
     * @return array
     */
    public function getCitySuggest($letter)
    {
        $response = $this->dadata->getAddress([
            'query' => $letter,
            'count' => 15,
            'from_bound' => [
                'value' => 'city',
            ],
            'to_bound' => [
                'value' => 'settlement',
            ],
        ]);

        return $response->data;
    }

    /**
     * @param array $cityData
     * @return City
     */
    function saveCity(array $cityData)
    {
        $city = new City([
            'fias_id' => $cityData['fias_id'],
            'name' => $cityData['city_with_type'],
            'region_fias_id' => $cityData['region_fias_id'],
            'region_with_type' => $cityData['region_with_type'],
            'data' => json_encode($cityData),
        ]);
        $city->save();

        return $city;
    }
}
