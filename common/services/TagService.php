<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 04.10.17
 * Time: 19:31
 */

namespace common\services;


use common\models\entity\offer\Offer;
use common\models\entity\tag\Tag;

class TagService
{
    /**
     * Всем офферам, которые подходят по правилу прицепляем tagId
     * @param $tagId
     */
    public function appendTag($tagId)
    {
        $tagPatterns = Tag::getRepository()->getById($tagId)->tagPatterns;

        foreach ($tagPatterns as $tagPattern) {
            $offerDatas = Offer::getRepository()->getMatched($tagPattern->pattern, $tagPattern->pattern_type);

            foreach ($offerDatas as $offerData) {
                $offer = Offer::getRepository()->getById($offerData['offer']['id']);
                $offer->setTags([$tagId]);
            }
        }
    }

    /**
     * @var Tag[]
     */
    static $tags;
}
