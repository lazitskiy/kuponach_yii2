<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 10.09.17
 * Time: 17:39
 */

namespace common\traits\base;


use common\services\email\EmailService;

trait EmailServiceAwareTrait
{
    /**
     * @return object|EmailService
     */
    public function getEmailService()
    {
        return \Yii::$container->get(EmailService::class);
    }
}
