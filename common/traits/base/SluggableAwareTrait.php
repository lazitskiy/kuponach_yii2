<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 05.08.17
 * Time: 2:27
 */

namespace common\traits\base;


trait SluggableAwareTrait
{
    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'slugAttribute' => 'slug',
            ],
        ];
    }
}
