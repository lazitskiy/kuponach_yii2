<?php

namespace common\traits\base;


use skeeks\yii2\dadataSuggestApi\DadataSuggestApi;

trait DadataAwareTrait
{
    /**
     * @return object|DadataSuggestApi
     */
    public function getDadata()
    {
        return \Yii::$app->get('dadataSuggestApi');
    }
}
