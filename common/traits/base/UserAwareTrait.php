<?php

namespace common\traits\base;


use common\models\entity\user\User;

trait UserAwareTrait
{
    /**
     * @return \yii\web\User
     */
    public function getUserTrait()
    {
        return \Yii::$app->getUser();
    }

    /**
     * @return null|\yii\web\IdentityInterface|User
     */
    public function getUserIdentity()
    {
        return \Yii::$app->getUser()->getIdentity();
    }
}
