<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 05.08.17
 * Time: 16:22
 */

namespace common\traits\base;


use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

trait CreatedAtBehaviorAwareTrait
{
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => (new \DateTime())->format('Y-m-d H:i:s'),
            ],
        ];
    }
}
