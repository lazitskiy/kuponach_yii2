<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 05.08.17
 * Time: 3:25
 */

namespace common\traits;


use common\services\QuestionService;

trait QuestionServiceAwareTrait
{
    /**
     * @return object|QuestionService
     */
    public function getQuestionService()
    {
        return \Yii::$container->get(QuestionService::class);
    }
}
