<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 05.08.17
 * Time: 3:25
 */

namespace common\traits;


use common\services\UserService;

trait UserServiceAwareTrait
{
    /**
     * @return object|UserService
     */
    public function getUserService()
    {
        return \Yii::$container->get(UserService::class);
    }
}
