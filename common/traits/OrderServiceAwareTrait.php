<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 05.08.17
 * Time: 3:25
 */

namespace common\traits;


use common\services\OrderService;

trait OrderServiceAwareTrait
{
    /**
     * @return object|OrderService
     */
    public function getOrderService()
    {
        return \Yii::$container->get(OrderService::class);
    }
}
