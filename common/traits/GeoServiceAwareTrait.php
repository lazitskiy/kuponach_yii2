<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 10.08.17
 * Time: 19:16
 */

namespace common\traits;


use common\services\GeoService;

trait GeoServiceAwareTrait
{
    /**
     * @return object|GeoService
     */
    public function getGeoService()
    {
        return \Yii::$container->get(GeoService::class);
    }
}
