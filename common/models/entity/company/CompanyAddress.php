<?php

namespace common\models\entity\company;

use Yii;

/**
 * This is the model class for table "company_address".
 *
 * @property int $id
 * @property int $company_id
 * @property string $address
 * @property string $lat
 * @property string $lon
 * @property string $address_data
 * @property string $dadated_at
 * @property int $country_all
 * @property int $city_all
 * @property int $city_area_id
 * @property int $city_district_id
 * @property int $city_street_id
 * @property int $city_metro_ids
 */
class CompanyAddress extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company_address';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'city_all', 'city_area_id', 'city_district_id', 'city_street_id'], 'integer'],
            [['lat', 'lon'], 'number'],
            [['address'], 'string', 'max' => 255],
            [['address_data', 'dadated_at'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     * @return CompanyAddressRepository the active query used by this AR class.
     */
    public static function getRepository()
    {
        return new CompanyAddressRepository(get_called_class());
    }
}
