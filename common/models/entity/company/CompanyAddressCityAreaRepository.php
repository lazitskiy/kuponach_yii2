<?php

namespace common\models\entity\company;

/**
 * This is the ActiveQuery class for [[CompanyAddressCityArea]].
 *
 * @see CompanyAddressCityArea
 */
class CompanyAddressCityAreaRepository extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return CompanyAddressCityArea[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return CompanyAddressCityArea|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param int $cityId
     * @param string $indexBy
     * @return array|CompanyAddressCityArea[]
     */
    public function getByCityId(int $cityId, $indexBy = null)
    {
        $q = $this->where(['city_id' => $cityId]);
        if ($indexBy) {
            $q->indexBy($indexBy);
        }

        return $q->all();
    }
}
