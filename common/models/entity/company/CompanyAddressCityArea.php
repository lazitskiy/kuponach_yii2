<?php

namespace common\models\entity\company;

use common\behaviors\SluggableBehavior;

/**
 * This is the model class for table "company_address_city_area".
 *
 * @property int $id
 * @property int $city_id
 * @property string $name
 * @property string $name_where
 * @property string $slug
 */
class CompanyAddressCityArea extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company_address_city_area';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id'], 'integer'],
            [['name', 'name_where', 'slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     * @return CompanyAddressCityAreaRepository the active query used by this AR class.
     */
    public static function getRepository()
    {
        return new CompanyAddressCityAreaRepository(get_called_class());
    }
}
