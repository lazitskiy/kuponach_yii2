<?php

namespace common\models\entity\company;

use common\behaviors\SluggableBehavior;

/**
 * This is the model class for table "company_address_city_street".
 *
 * @property int $id
 * @property int $city_id
 * @property string $name
 * @property string $name_full
 * @property string $name_where
 * @property string $slug
 */
class CompanyAddressCityStreet extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company_address_city_street';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id'], 'integer'],
            [['name', 'name_full', 'name_where', 'slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     * @return CompanyAddressCityStreetRepository the active query used by this AR class.
     */
    public static function getRepository()
    {
        return new CompanyAddressCityStreetRepository(get_called_class());
    }
}
