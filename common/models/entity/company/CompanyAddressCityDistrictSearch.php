<?php

namespace common\models\entity\company;

use yii\base\Model;
use yii\data\ActiveDataProvider;

class CompanyAddressCityDistrictSearch extends CompanyAddressCityDistrict
{
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CompanyAddressCityDistrict::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'name_full', $this->name_full])
            ->andFilterWhere(['like', 'name_where', $this->name_where])
            ->andFilterWhere(['like', 'slug', $this->slug]);

        return $dataProvider;
    }
}
