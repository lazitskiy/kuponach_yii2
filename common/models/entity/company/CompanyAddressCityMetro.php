<?php

namespace common\models\entity\company;

use common\behaviors\SluggableBehavior;

/**
 * This is the model class for table "company_address_city_metro".
 *
 * @property int $id
 * @property int $city_id
 * @property string $name
 * @property string $name_where
 * @property string $line
 * @property string $slug
 */
class CompanyAddressCityMetro extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company_address_city_metro';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id'], 'integer'],
            [['name', 'name_where', 'line', 'slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     * @return CompanyAddressCityMetroRepository the active query used by this AR class.
     */
    public static function getRepository()
    {
        return new CompanyAddressCityMetroRepository(get_called_class());
    }
}
