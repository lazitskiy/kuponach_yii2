<?php

namespace common\models\entity\company;

/**
 * This is the ActiveQuery class for [[CompanyAddressCityMetroRepository]].
 *
 * @see CompanyAddressCityMetro
 */
class CompanyAddressCityMetroRepository extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return CompanyAddressCityMetro[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return CompanyAddressCityMetro|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param int $cityId
     * @param string $indexBy
     * @return array|CompanyAddressCityMetro[]
     */
    public function getByCityId(int $cityId, $indexBy = null)
    {
        $q = $this->where(['city_id' => $cityId])
        ->orderBy('name');
        if ($indexBy) {
            $q->indexBy($indexBy);
        }

        return $q->all();
    }
}
