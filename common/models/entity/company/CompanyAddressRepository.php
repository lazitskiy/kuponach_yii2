<?php

namespace common\models\entity\company;

/**
 * This is the ActiveQuery class for [[CompanyAddress]].
 *
 * @see CompanyAddress
 */
class CompanyAddressRepository extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return CompanyAddress[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return CompanyAddress|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
