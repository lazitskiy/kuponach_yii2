<?php

namespace common\models\entity\company;

/**
 * This is the ActiveQuery class for [[CompanyAddressCityDistrict]].
 *
 * @see CompanyAddressCityDistrict
 */
class CompanyAddressCityDistrictRepository extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return CompanyAddressCityDistrict[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return CompanyAddressCityDistrict|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param int $cityId
     * @param string $indexBy
     * @return array|CompanyAddressCityDistrict[]
     */
    public function getByCityId(int $cityId, $indexBy = null)
    {
        $q = $this->where(['city_id' => $cityId]);
        if ($indexBy) {
            $q->indexBy($indexBy);
        }

        return $q->all();
    }
}
