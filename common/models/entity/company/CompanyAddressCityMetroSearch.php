<?php

namespace common\models\entity\company;

use yii\base\Model;
use yii\data\ActiveDataProvider;

class CompanyAddressCityMetroSearch extends CompanyAddressCityMetro
{
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CompanyAddressCityMetro::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'line', $this->line])
            ->andFilterWhere(['like', 'name_where', $this->name_where])
            ->andFilterWhere(['like', 'slug', $this->slug]);

        return $dataProvider;
    }
}
