<?php

namespace common\models\entity\company;

use common\behaviors\SluggableBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "company".
 *
 * @property int $id
 * @property int $operator_id
 * @property int $operator_company_id ИД компании у оператора
 * @property string $name
 * @property string $slug
 * @property string $worktime
 * @property string $phone
 * @property string $site
 * @property string $created_at
 * @property string $updated_at
 */
class Company extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => (new \DateTime())->format('Y-m-d H:i:s'),
            ], [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['operator_id'], 'required'],
            [['operator_id', 'operator_company_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'slug', 'worktime', 'phone', 'site'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     * @return CompanyRepository the active query used by this AR class.
     */
    public static function getRepository()
    {
        return new CompanyRepository(get_called_class());
    }
}
