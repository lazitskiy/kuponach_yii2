<?php

namespace common\models\entity\offer;

/**
 * This is the ActiveQuery class for [[OfferImage]].
 *
 * @see OfferImage
 */
class OfferImageRepository extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Offer[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Offer|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param $offerId
     * @return array|OfferImage[]
     */
    public function getByOfferId($offerId)
    {
        $this->where(['offer_id' => $offerId]);

        return $this->all();
    }

    /**
     * @return array|OfferImage[]
     */
    public function getNotDownloaded()
    {
        $this->where(['status' => OfferImage::STATUS_NEW]);

        return $this->all();
    }
}
