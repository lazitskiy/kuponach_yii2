<?php

namespace common\models\entity\offer;

use common\helpers\StringHelper;
use common\models\entity\city\City;
use common\models\entity\tag\Tag;
use yii\helpers\ArrayHelper;

/**
 * This is the ActiveQuery class for [[Offer]].
 *
 * @see Offer
 */
class OfferRepository extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Offer[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Offer|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param int $offerId
     * @return Offer|null
     */
    public function getById($offerId)
    {
        $offers = $this->getByIds([$offerId]);

        return $offers[0];
    }

    /**
     * @param array $offerIds
     * @param array $orderBy
     * @return array|Offer[]
     */
    public function getByIds(array $offerIds = [], array $orderBy = [])
    {
        if (!$offerIds) {
            return [];
        }

        //$this->with('operator', 'tags', 'imagesActive');
        $this->with('operator', 'imagesActive');
        $this->select(['id', 'operator_id', 'name', 'name_short', 'url', 'slug', 'price_discounted', 'price_original', 'discount_percent', 'price_coupon_min', 'date_end']);
        $this->where(['id' => $offerIds]);
        if ($orderBy) {
            $this->orderBy($orderBy);
        }

        return $this->all();
    }

    /**
     * Берет офферы и preg_match по полям
     *
     * @param $pattern
     * @param $patternType
     * @return array
     */
    public function getMatched($pattern, $patternType, $limit = 20)
    {
        $matches = [];
        $offerQuery = Offer::getRepository()->getAllActiveQuery();

        $i = 0;
        foreach ($offerQuery->batch(20) as $offers) {
            foreach ($offers as $offer) {
                /** @var Offer $offer */
                $offer->sanitizeField();
                foreach (Offer::getTextFields($patternType) as $textField) {
                    list($result, $textMatch) = StringHelper::isSatisfiedBy($offer->$textField, $pattern);
                    if ($result) {

                        $matches[] = [
                            'field' => $textField,
                            'pattern' => $pattern,
                            'offer' => [
                                'id' => $offer->id,
                                'name' => $offer->name,
                                'text' => $textMatch,
                                'tags' => array_map(function (Tag $tag) {
                                    return [
                                        'id' => $tag->id,
                                        'name' => $tag->name,
                                    ];
                                }, $offer->tags),
                            ],
                        ];
                        $i++;
                        if ($i === $limit) {
                            return $matches;
                        }
                        continue 2;
                    }
                }
            }
        }

        return $matches;
    }

    /**
     * @return $this
     */
    public function getAllActiveQuery()
    {
        $now = (new \DateTime())->format('Y-m-d H:i:s');
        $this->with('tags')
            ->where(['>', 'date_end', $now]);

        return $this;
    }
}
