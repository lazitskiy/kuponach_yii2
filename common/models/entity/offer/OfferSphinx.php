<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 29.09.17
 * Time: 1:11
 */

namespace common\models\entity\offer;


use yii\sphinx\ActiveRecord;


/**
 * This is the model class for table "offer".
 *
 * @property integer $id
 * @property string $city_ids
 * @property bool $is_federal
 * @property string $tag_ids
 * @property string $name
 * @property string $name_short
 * @property string $date_start
 * @property string $date_end
 *
 * @property int $in_city_ids
 * @property int $in_category_ids
 * @property int $in_tag_ids
 */
class OfferSphinx extends ActiveRecord
{
    /**
     * @var int Для сфинксового типа данных json
     */
    public $in_city_ids;

    public static function indexName()
    {
        return 'offerIndex';
    }

    /**
     * @inheritdoc
     * @return OfferSphinxRepository the active query used by this AR class.
     */
    public static function getRepository()
    {
        return new OfferSphinxRepository(get_called_class());
    }

    public function afterFind()
    {
        if ($this->date_start) {
            $this->date_start = \Yii::$app->formatter->asDatetime($this->date_start, 'php:Y-m-d H:i:s');
        }
        if ($this->date_end) {
            $this->date_end = \Yii::$app->formatter->asDatetime($this->date_end, 'php:Y-m-d H:i:s');
        }
    }

}
