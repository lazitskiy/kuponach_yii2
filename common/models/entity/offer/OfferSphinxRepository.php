<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 29.09.17
 * Time: 1:11
 */

namespace common\models\entity\offer;


use common\models\entity\city\City;
use yii\helpers\ArrayHelper;
use yii\sphinx\Query;

class OfferSphinxRepository extends \yii\sphinx\ActiveQuery
{
    public function init()
    {
        parent::init();

        //$this->limit = 50000;
    }

    /**
     * @inheritdoc
     * @return Offer[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Offer|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * Главный универсальный запрос
     * @param int $cityId
     * @param array $tagIds
     * @param int $page
     * @param int $limit
     * @param int $tagLimit
     * @param bool $nedorogo
     * @param bool $besplatno
     * @param int $areaId
     * @param int $districtId
     * @param int $metroId
     * @return array
     */
    public function getBy(
        int $cityId = City::CITY_ID_DEFAULT, array $tagIds = [], $page = 1, $limit, $tagLimit, $nedorogo = false, $besplatno = false,
        $areaId = null, $districtId = null, $streetId = null, $metroId = null
    )
    {
        $query = (new Query())
            ->from(OfferSphinx::indexName())
            ->addOptions([
                'max_matches' => 1000000,
            ]);
        $queryOriginal = clone $query;

        $columns = [
            'id', 'city_ids', 'tag_ids', 'name', 'name_short', 'is_federal', 'price_discounted',
            'city_area_id', 'city_district_id', 'city_street_id', 'city_metro_ids',
        ];
        $wheres = [];
        //$orderBy = ['offer_count' => SORT_DESC];

        // Совпал город
        $columns[] = "(IN(city_ids, $cityId)=1 OR is_federal=1) as city_found";
        $wheres['city_found'] = 1;

        // Теги в JSON
        if ($tagIds) {
            $or = [];
            foreach ($tagIds as $tagId) {
                $or[] = "IN(tag_ids, $tagId)=1";
            }
            $columns[] = '(' . implode(' OR ', $or) . ') as tag_found';
            $wheres['tag_found'] = 1;
        }

        $orderBy = [];
        // Бесплатно
        if ($besplatno) {
            $wheres['price_discounted'] = 0;
        }
        // Недорого
        if ($nedorogo) {
            $price = Offer::PRICE_NEDOROGO;
            $columns[] = "(price_discounted < $price) as nedorogo";
            $wheres['nedorogo'] = 1;
            $orderBy = ['price_discounted' => SORT_DESC];
        }
        // Округ
        if ($areaId) {
            $columns[] = "(city_area_id=$areaId OR is_federal=1 OR country_all=1 OR city_all=1) as area_found";
            $wheres['area_found'] = 1;
        }
        // Район
        if ($districtId) {
            $columns[] = "(city_district_id=$districtId OR is_federal=1 OR country_all=1 OR city_all=1) as district_found";
            $wheres['district_found'] = 1;
        }
        // Улица
        if ($streetId) {
            $columns[] = "(city_street_id=$streetId OR is_federal=1 OR country_all=1 OR city_all=1) as street_found";
            $wheres['street_found'] = 1;
        }
        // Метро
        if ($metroId) {
            $columns[] = "(IN(city_metro_ids, $metroId)=1 OR is_federal=1 OR country_all=1 OR city_all=1) as metro_found";
            $wheres['metro_found'] = 1;
        }

        $offset = ($page - 1) * $limit;
        $query->limit($limit)
            ->offset($offset)
            ->select($columns)
            ->andFilterWhere($wheres);

        $offers = $query->showMeta(true)->search();
        //vvd($offers);
        //Вытащим теги
        $columns[] = 'groupby() as tag_id';
        $columns[] = 'COUNT(*) as offer_count';
        $columns[] = 'tag_ids';

        $tags = $queryOriginal
            ->select($columns)
            ->addOptions([
                'max_matches' => 10000,
            ])
            ->andFilterWhere($wheres)
            ->orderBy(['offer_count' => SORT_DESC])
            ->groupBy('tag_ids')
            ->limit($tagLimit)
            ->all();

        $arTagIdCount = [];
        if ($tags) {
            $arTagIdCount = ArrayHelper::map($tags, 'tag_id', 'offer_count');
            //unset($arTagIdCount[$tagId]);
            $arTagIdCount = array_filter($arTagIdCount, 'strlen', ARRAY_FILTER_USE_KEY);
        }

        return [
            ArrayHelper::getColumn($offers['hits'], 'id'),
            $offers['meta']['total_found'],
            $arTagIdCount,
            $orderBy,
        ];
    }

    /**
     * @param $cityId
     * @return Query
     */
    public function findByCityId($cityId)
    {
        $columns = [
            'id', 'tag_ids', 'name', 'name_short', "(IN(city_ids, $cityId) = 1 OR is_federal = 1) as city_found",
        ];
        $wheres['city_found'] = 1;

        $query = (new Query())
            ->select($columns)
            ->from(OfferSphinx::indexName())
            ->andFilterWhere($wheres)
            ->limit(5000000)
            ->addOptions([
                'max_matches' => 5000000,
            ]);

        return $query;
    }

    /**
     * Ищет теги
     * @return array $result
     */
    public function findTagByString($q)
    {
        $result = (new Query())
            ->select(['name', 'slug'])
            ->from('tagIndex')
            ->match($q)
            ->limit(1000)
            ->addOptions([
                'max_matches' => 1000,
            ])
            ->all();

        return $result;
    }

}
