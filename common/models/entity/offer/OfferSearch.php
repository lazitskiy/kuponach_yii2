<?php

namespace common\models\entity\offer;

use common\models\entity\operator\Operator;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\entity\offer\Offer;

/**
 * OfferSearch represents the model behind the search form of `common\models\entity\offer\Offer`.
 */
class OfferSearch extends Offer
{
    /**
     * @var Operator
     */
    public $operator;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_active', 'is_hit', 'company_id', 'operator_id', 'operator_offer_id', 'discount_percent', 'price_original', 'price_discounted', 'price_coupon_min'], 'integer'],
            [['operator', 'category_ids', 'city_ids', 'name', 'name_short', 'slug', 'description', 'description_attension', 'description_tips', 'description_adv', 'description_short', 'url', 'date_start', 'date_end', 'created_at', 'updated_at', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Offer::find()
            ->with(['tags', 'operator']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->sort->attributes['operator'] = [
            'asc' => ['operator.name' => SORT_ASC],
            'desc' => ['operator.name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');

            return $dataProvider;
        }
        // grid filtering conditions
        $query->andFilterWhere([
            'offer.id' => $this->id,
            'offer.is_active' => $this->is_active,
            'offer.is_hit' => $this->is_hit,
            'offer.company_id' => $this->company_id,
            'offer.operator_id' => $this->operator_id,
            'offer.operator_offer_id' => $this->operator_offer_id,
            'offer.date_start' => $this->date_start,
            'offer.date_end' => $this->date_end,
            'offer.discount_percent' => $this->discount_percent,
            'offer.price_original' => $this->price_original,
            'offer.price_discounted' => $this->price_discounted,
            'offer.price_coupon_min' => $this->price_coupon_min,
            'offer.status' => $this->status,
            //'created_at' => $this->created_at,
            //'updated_at' => $this->updated_at,
        ]);
        $query->andFilterWhere(['=', 'date(offer.created_at)', $this->created_at]);

        $query->andFilterWhere(['like', 'offer.category_ids', $this->category_ids])
            ->andFilterWhere(['like', 'offer.city_ids', $this->city_ids])
            ->andFilterWhere(['like', 'offer.name', $this->name])
            ->andFilterWhere(['like', 'offer.name_short', $this->name_short])
            ->andFilterWhere(['like', 'offer.slug', $this->slug])
            ->andFilterWhere(['like', 'offer.description', $this->description])
            ->andFilterWhere(['like', 'offer.description_attension', $this->description_attension])
            ->andFilterWhere(['like', 'offer.description_tips', $this->description_tips])
            ->andFilterWhere(['like', 'offer.description_adv', $this->description_adv])
            ->andFilterWhere(['like', 'offer.description_short', $this->description_short])
            ->andFilterWhere(['like', 'offer.url', $this->url])
            ->andFilterWhere(['=', 'operator_id', $this->operator]);

        return $dataProvider;
    }
}
