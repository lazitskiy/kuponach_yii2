<?php

namespace common\models\entity\offer;

/**
 * This is the model class for table "offer_image".
 *
 * @property int $id
 * @property int $offer_id
 * @property string $operator_image
 * @property string $image
 * @property int $status
 * @property string $reason
 *
 * @property Offer $offer
 */
class OfferImage extends \yii\db\ActiveRecord
{
    /**
     * Новая картинка
     */
    const STATUS_NEW = 0;

    /**
     * Ошибка
     */
    const STATUS_ERROR = 4;

    /**
     * Успешно
     */
    const STATUS_DONE = 5;

    public static function tableName()
    {
        return 'offer_image';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['offer_id', 'operator_image', 'image', 'status'], 'required'],
            [['operator_image', 'image', 'reason'], 'string'],
            [['offer_id', 'status'], 'integer'],
        ];
    }

    public static function getStatusMap($statusId = null)
    {

    }

    /**
     * @inheritdoc
     * @return OfferImageRepository the active query used by this AR class.
     */
    public static function getRepository()
    {
        return new OfferImageRepository(get_called_class());
    }

    public function getOffer()
    {
        return $this->hasOne(Offer::class, ['id' => 'offer_id']);
    }

    public function getPath()
    {
        return '/images/offers/' . $this->offer_id . '/' . $this->image;
    }
}
