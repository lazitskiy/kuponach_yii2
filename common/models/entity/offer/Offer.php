<?php

namespace common\models\entity\offer;

use common\helpers\StringHelper;
use common\models\entity\city\City;
use common\models\entity\operator\Operator;
use common\models\entity\tag\Tag;
use common\models\entity\tag\TagPattern;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;

/**
 * This is the model class for table "offer".
 *
 * @property int $id
 * @property string $category_ids
 * @property string $city_ids
 * @property bool $is_federal
 * @property int $is_active
 * @property int $is_hit
 * @property int $is_tag_patterned 1 - Оффер разобран на мелкие-теги
 * @property int $company_id
 * @property int $operator_id
 * @property int $operator_offer_id
 * @property string $name
 * @property string $name_short
 * @property string $slug
 * @property string $description
 * @property string $description_attension
 * @property string $description_tips
 * @property string $description_adv
 * @property string $description_short
 * @property string $url
 * @property string $date_start
 * @property string $date_end
 * @property string $valid_start
 * @property string $valid_end
 * @property int $discount_percent
 * @property int $price_original
 * @property int $price_discounted
 * @property int $price_coupon_min
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Tag[] $tags
 * @property Operator $operator
 * @property OfferImage[] $imagesActive
 */
class Offer extends \yii\db\ActiveRecord
{
    /**
     * Новая акция
     */
    const STATUS_NEW = 0;
    /**
     * Начата работа
     */
    const STATUS_WORKING = 1;
    /**
     * НЕ ясно что с ней делать, пока отложили
     */
    const STATUS_PENDING = 2;
    /**
     * Теги сделаны
     */
    const STATUS_DONE = 5;

    /**
     * Меньше скольких рублей - это недорого
     */
    const PRICE_NEDOROGO = 299;

    /**
     * @var array
     */
    public $images;

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => (new \DateTime())->format('Y-m-d H:i:s'),
            ], [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
            ],
        ];
    }

    public static function tableName()
    {
        return 'offer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_ids', 'category_ids', 'company_id', 'operator_id', 'operator_offer_id', 'date_start', 'date_end', 'discount_percent', 'price_original', 'price_discounted', 'price_coupon_min'], 'required'],
            [['city_ids', 'category_ids', 'name_short', 'description', 'description_attension', 'description_tips', 'description_adv', 'description_short'], 'string'],
            [['is_federal', 'is_active', 'is_hit', 'is_tag_patterned', 'company_id', 'operator_id', 'operator_offer_id', 'discount_percent', 'price_original', 'price_discounted', 'price_coupon_min', 'status'], 'integer'],
            [['date_start', 'date_end', 'created_at', 'updated_at', 'tags', 'images'], 'safe'],
            [['name', 'slug', 'url'], 'string', 'max' => 255],
        ];
    }

    public static function getStatusMap($statusId = null)
    {
        $statuses = [
            static::STATUS_NEW => 'Новый',
            static::STATUS_WORKING => 'В работе',
            static::STATUS_PENDING => 'Отложена',
            static::STATUS_DONE => 'Сделано',
        ];

        return is_null($statusId) ? $statuses : $statuses[$statusId];
    }

    public static function getTextFields($group = null)
    {
        if ($group == TagPattern::TYPE_NAME) {
            return ['name', 'name_short'];
        } elseif ($group == TagPattern::TYPE_DESCRIPTION) {
            return ['description', 'description_attension', 'description_tips', 'description_adv', 'description_short'];
        } else {
            return ['name', 'name_short', 'description', 'description_attension', 'description_tips', 'description_adv', 'description_short'];
        }
    }

    public function sanitizeField()
    {
        foreach (static::getTextFields() as $textField) {
            $this->$textField = strip_tags($this->$textField);
        }
    }

    /**
     * @inheritdoc
     * @return OfferRepository the active query used by this AR class.
     */
    public static function getRepository()
    {
        return new OfferRepository(get_called_class());
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        if ($this->images) {
            $offerImages = OfferImage::getRepository()->indexBy('operator_image')->getByOfferId($this->id);
            foreach ($this->images as $image) {

                if (($ext = pathinfo($image, PATHINFO_EXTENSION)) !== '') {
                    $ext = strtolower($ext);
                } else {
                    $ext = 'jpg';
                }

                if (!isset($offerImages[$image])) {
                    $imageNameNormal = preg_replace('/\?.*/uis', '', $image);
                    $offerImage = new OfferImage([
                        'offer_id' => $this->id,
                        'operator_image' => StringHelper::normalizeUrl($image),
                        'image' => md5($imageNameNormal) . '.' . $ext,
                        'status' => OfferImage::STATUS_NEW,
                    ]);
                    $offerImage->save();
                }
            }
        }

        $save = parent::save($runValidation, $attributeNames);

        return $save;
    }

    public function getOperator()
    {
        return $this->hasOne(Operator::class, ['id' => 'operator_id']);
    }

    public function getImages()
    {
        return $this->hasMany(OfferImage::class, ['offer_id' => 'id']);
    }

    public function getImagesActive()
    {
        return $this->hasMany(OfferImage::class, ['offer_id' => 'id'])
            ->andOnCondition(['status' => OfferImage::STATUS_DONE]);
    }

    public function getTags()
    {
        return $this->hasMany(Tag::class, ['id' => 'tag_id'])
            ->viaTable('offer_tag', ['offer_id' => 'id']);
    }

    public function getTag($tagId)
    {
        return $this->hasOne(Tag::class, ['id' => 'tag_id'])
            ->viaTable('offer_tag', ['offer_id' => 'id'], function ($query) use ($tagId) {
                /** @var ActiveQuery $query */
                $query->onCondition(['tag_id' => $tagId]);
            });
    }

    public function setTags($tagIds, $deleteOld = false)
    {
        if (!is_array($tagIds)) {
            $tagIds = [];
        }

        $tags = $this->tags;

        if ($tags && $deleteOld) {
            foreach ($tags as $tag) {
                if (!in_array($tag->id, $tagIds)) {
                    $sql = "DELETE FROM offer_tag WHERE offer_id = {$this->id} AND tag_id = {$tag->id}";
                    \Yii::$app->getDb()->createCommand($sql)->execute();
                }
            }
        }

        foreach ($tagIds as $tagId) {
            $tagId = trim($tagId);
            // Это если прилете новй строковой тег
            if (!is_numeric($tagId)) {
                $tag = Tag::findOne(['name' => $tagId]);
                if (!$tag) {
                    $tag = new Tag();
                    $tag->name = $tagId;
                    $tag->save();
                }
                $tagId = $tag->id;
            }
            $tagExists = $this->getTag($tagId)->one();
            if (!$tagExists) {
                $sql = "INSERT INTO offer_tag (offer_id, tag_id) VALUES({$this->id}, {$tagId})";
                \Yii::$app->getDb()->createCommand($sql)->execute();
            }
        }
    }

    public function setImages(array $images)
    {
        if (!$images) {
            return;
        }
        $images = array_unique($images);

        $offerImages = OfferImage::getRepository()->indexBy('operator_image')->getByOfferId($this->id);
        foreach ($images as $image) {

            if (($ext = pathinfo($image, PATHINFO_EXTENSION)) !== '') {
                $ext = preg_replace('/\?.*/uis', '', $ext);
                $ext = strtolower($ext);
            } else {
                $ext = 'jpg';
            }

            if (!isset($offerImages[$image])) {
                $offerImage = new OfferImage([
                    'offer_id' => $this->id,
                    'operator_image' => $image,
                    'image' => md5($image) . '.' . $ext,
                    'status' => OfferImage::STATUS_NEW,
                ]);
                $offerImage->save();
            }
        }
    }

    /**
     * @return array
     */
    public function getCategoryIds()
    {
        return Json::decode($this->category_ids);
    }

    public function getOutLink()
    {
        return Url::toRoute(['out/index', 'offerId' => $this->id], 'http');
        //return Url::toRoute(['out/index', 'offerId' => $this->id], 'http');
        //return StringHelper::normalizeUrl($this->url) . $this->operator->out_parameter;
    }

    public function getOfferLink()
    {
        return StringHelper::normalizeUrl($this->url) . $this->operator->out_parameter;
    }

    public function getMainTag()
    {
        $tags = $this->tags;
        ArrayHelper::multisort($tags, ['show_on_main', 'offer_count'], SORT_DESC);

        $tag = array_shift($tags);
        if (!$tag) {
            return '';
        }

        return Html::a($tag->getNameUcFirst(), "/kupon-na/" . $this->slug);
    }

    public function getSeoTitle(City $city)
    {
        $str = $this->name . ' ' . $city->name_in;

        return $str;
    }

    public function getSeoH1(City $city)
    {
        return $this->getSeoTitle($city);
    }

    public function getRemainingText()
    {
        try {
            $dt = new \DateTime($this->date_end);
        } catch (\Exception $e) {
            return '';
        }

        return 'до ' . \Yii::$app->getFormatter()->asDate($this->date_end);

    }
}
