<?php

namespace common\models\entity\tag;

/**
 * This is the ActiveQuery class for [[Tag]].
 *
 * @see Tag
 */
class TagRepository extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Tag[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Tag|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param $indexByColumn string
     * @return array|Tag[]
     */
    public function getAllActive($indexByColumn = null)
    {
        if ($indexByColumn) {
            $this->indexBy($indexByColumn);
        }

        return $this->all();
    }

    /**
     * @param int $tagId
     * @return Tag|null
     */
    public function getById($tagId)
    {
        $this->where(['id' => $tagId]);

        return $this->one();
    }
}
