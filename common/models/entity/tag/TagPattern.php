<?php

namespace common\models\entity\tag;

use common\behaviors\SluggableBehavior;
use common\models\entity\offer\Offer;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tag_pattern".
 *
 * @property int $id
 * @property int $tag_id
 * @property string $pattern
 * @property string $pattern_type
 * @property int $user_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Offer[] $offers
 * @property Tag $tag
 * @property Tag[] $tagParents
 */
class TagPattern extends \yii\db\ActiveRecord
{
    /**
     * Тип правила, показывает к каким полям оффера применяется
     */
    const TYPE_NAME = 'name';
    const TYPE_DESCRIPTION = 'description';

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => (new \DateTime())->format('Y-m-d H:i:s'),
            ], [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'user_id',
                'updatedByAttribute' => 'user_id',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tag_pattern';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tag_id', 'pattern', 'pattern_type'], 'required'],
            [['tag_id', 'user_id'], 'integer'],
            [['pattern', 'pattern_type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     * @return TagPatternRepository the active query used by this AR class.
     */
    public static function getRepository()
    {
        return new TagPatternRepository(get_called_class());
    }

    public function getTag()
    {
        return $this->hasOne(Tag::class, ['id' => 'tag_id']);
    }

    public function getTagParents()
    {
        return $this->hasMany(Tag::class, ['id' => 'tag_id_parent'])
            ->viaTable('tag_tag', ['tag_id' => 'tag_id']);
    }
}
