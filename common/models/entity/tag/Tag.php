<?php

namespace common\models\entity\tag;

use common\behaviors\SluggableBehavior;
use common\models\entity\city\City;
use common\models\entity\company\CompanyAddressCityArea;
use common\models\entity\offer\Offer;
use common\models\entity\seo\Seo;
use common\traits\base\ApplicationAwareTrait;
use frontend\widgets\CouponWidget;
use yii\behaviors\AttributeBehavior;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Query;

/**
 * This is the model class for table "tag".
 *
 * @property int $id
 * @property int $synonym_tag_id
 * @property string $name
 * @property string $name_where
 * @property string $slug
 * @property int $is_group
 * @property int $show_on_main
 * @property int $sort_on_main
 * @property int $offer_count
 * @property string $seo_title
 * @property string $seo_h1
 * @property string $seo_h1_sub
 * @property int $seo_id
 * @property int $user_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Offer[] $offers
 * @property TagPattern[] $tagPatterns
 * @property Tag[] $tagChildren
 * @property Tag[] $tagParents
 * @property Tag[] $tagSynonymsChildren
 * @property Tag $tagSynonymsParent
 * @property Seo $seo
 */
class Tag extends \yii\db\ActiveRecord
{
    use ApplicationAwareTrait;

    const TAG_ID_MAIN_PAGE = -1;

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => (new \DateTime())->format('Y-m-d H:i:s'),
            ], [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
            ], [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'user_id',
                'updatedByAttribute' => 'user_id',
            ], [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'is_group',
                ],
                'value' => function ($event) {
                    return 0;
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tag';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['synonym_tag_id', 'is_group', 'show_on_main'], 'integer'],
            [['name', 'name_where', 'slug', 'seo_title', 'seo_h1', 'seo_h1_sub'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     * @return TagRepository the active query used by this AR class.
     */
    public static function getRepository()
    {
        return new TagRepository(get_called_class());
    }

    /**
     * @return ActiveQuery
     */
    public function getOffers()
    {
        return $this->hasMany(Offer::class, ['id' => 'offer_id'])
            ->viaTable('offer_tag', ['tag_id' => 'id']);
    }

    public function getTagPatterns()
    {
        return $this->hasMany(TagPattern::class, ['tag_id' => 'id']);
    }

    /**
     * @param array $orderBy ['name'=>SORT_ASC]
     * @return ActiveQuery
     */
    public function getTagChildren($orderBy = [])
    {
        $q = $this->hasMany(Tag::class, ['id' => 'tag_id'])
            ->viaTable('tag_tag', ['tag_id_parent' => 'id']);
        if ($orderBy) {
            $q->orderBy($orderBy);
        }

        return $q;
    }

    /**
     * @return ActiveQuery
     */
    public function getTagParents()
    {
        return $this->hasMany(Tag::class, ['id' => 'tag_id_parent'])
            ->viaTable('tag_tag', ['tag_id' => 'id']);
    }

    public function getSeo()
    {
        return $this->hasOne(Seo::class, ['id' => 'seo_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getTagSynonymsChildren()
    {
        return $this->hasMany(static::class, ['synonym_tag_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getTagSynonymsParent()
    {
        return $this->hasOne(static::class, ['id' => 'synonym_tag_id']);
    }

    /**
     * @return Tag[]
     */
    public function getTagsRelated()
    {
        $offerIds = (new Query())
            ->select('offer_id')
            ->from('offer_tag')
            ->where(['tag_id' => $this->id])
            ->column();

        if (!$offerIds) {
            return [];
        }

        $tagOffer = (new Query())
            ->select(['tag_id', 'COUNT(*) as offer_count'])
            ->from('offer_tag')
            ->where(['offer_id' => $offerIds])
            ->andWhere(['!=', 'tag_id', $this->id])
            ->groupBy('tag_id')
            ->orderBy(['offer_count' => SORT_DESC])
            ->indexBy('tag_id')
            ->all();

        if (!$tagOffer) {
            return [];
        }
        $tagIds = array_keys($tagOffer);

        $tags = Tag::getRepository()
            ->alias('t')
            ->with(['tagPatterns'])
            ->select(['t.*', '(SELECT COUNT(*) FROM offer_tag WHERE tag_id = t.id) offer_count'])
            ->where(['t.id' => $tagIds])
            ->orderBy(['id' => SORT_DESC])
            ->all();

        return $tags;
    }

    public function getUrl()
    {
        return '/' . $this->slug;
    }

    public function isParentCurrent()
    {
        $slug = substr($this->getApplication()->getRequest()->getUrl(), 1);
        $slug = preg_replace('/\?.*/uis', '', $slug);

        if ($this->slug == $slug) {
            return true;
        }

        foreach ($this->tagChildren as $tag) {
            if ($tag->slug == $slug) {
                return true;
            }
        }

        return false;
    }

    public function isCurrent()
    {
        $slug = substr($this->getApplication()->getRequest()->getUrl(), 1);
        $slug = preg_replace('/\?.*/uis', '', $slug);

        return $this->slug == $slug;
    }

    public function getIcon()
    {
        return '';
    }

    public function getNameUcFirst()
    {
        return mb_strtoupper(mb_substr($this->name, 0, 1)) . mb_substr($this->name, 1);
    }

    public function getUrlTitle()
    {
        return 'Купоны на ' . $this->name;
    }

    public function getOfferCount()
    {
        return $this->offer_count;
    }

    private function getSeoWord($type, $filters)
    {
        $str = '';
        $object = $filters[$type]['values'][$filters[$type]['selected']] ?? null;
        if ($object) {
            $str = ' ' . $object->name_where;
        }

        return $str;
    }

    public function getSeoTitle(City $city, $nedorogo, $besplatno, $filters)
    {
        $areaStr = $this->getSeoWord(CouponWidget::URL_TYPE_AREA, $filters);
        $districtStr = $this->getSeoWord(CouponWidget::URL_TYPE_DISTRICT, $filters);
        $streetStr = $this->getSeoWord(CouponWidget::URL_TYPE_STREET, $filters);
        $metroStr = $this->getSeoWord(CouponWidget::URL_TYPE_METRO, $filters);

        $str = str_replace('{{city_in}}', $city->name_in . $areaStr . $districtStr . $streetStr . $metroStr, $this->seo_title);
        if ($nedorogo) {
            $str = str_replace(['{{extra_word1}}', '{{extra_word2}}'], [' недорого', 'на недорогие купоны '], $str);
        } elseif ($besplatno) {
            $str = str_replace(['{{extra_word1}}', '{{extra_word2}}'], [' бесплатно', 'на бесплатные купоны '], $str);
        } else {
            $str = str_replace(['{{extra_word1}}', '{{extra_word2}}'], ['', ''], $str);
        }

        return $str;
    }

    public function getSeoH1(City $city, $nedorogo, $besplatno, $filters = [])
    {
        $areaStr = $this->getSeoWord(CouponWidget::URL_TYPE_AREA, $filters);
        $districtStr = $this->getSeoWord(CouponWidget::URL_TYPE_DISTRICT, $filters);
        $streetStr = $this->getSeoWord(CouponWidget::URL_TYPE_STREET, $filters);
        $metroStr = $this->getSeoWord(CouponWidget::URL_TYPE_METRO, $filters);

        $str = str_replace('{{city_in}}', $city->name_in . $areaStr . $districtStr . $streetStr . $metroStr, $this->seo_h1 ?? $this->seo_title);

        if ($nedorogo) {
            $str = str_replace(['{{extra_word1}}', '{{extra_word2}}'], [' недорого', 'на недорогие купоны '], $str);
        } elseif ($besplatno) {
            $str = str_replace(['{{extra_word1}}', '{{extra_word2}}'], [' бесплатно', 'на бесплатные купоны '], $str);
        } else {
            $str = str_replace(['{{extra_word1}}', '{{extra_word2}}'], ['', ''], $str);
        }

        return $str;
    }

    public function getSeoSubHeader(City $city, Tag $tag, $nedorogo, $besplatno, $filters = [])
    {
        $areaStr = $this->getSeoWord(CouponWidget::URL_TYPE_AREA, $filters);
        $districtStr = $this->getSeoWord(CouponWidget::URL_TYPE_DISTRICT, $filters);
        $streetStr = $this->getSeoWord(CouponWidget::URL_TYPE_STREET, $filters);
        $metroStr = $this->getSeoWord(CouponWidget::URL_TYPE_METRO, $filters);

        $extra = '';
        $extra1 = '';
        if ($nedorogo) {
            $extra = 'недорогих ';
            $extra1 = 'недорогие ';
        }
        if ($besplatno) {
            $extra = 'бесплатных ';
            $extra1 = 'бесплатные ';
        }
        if ($tag->isMainPage()) {
            $str = "Агрегатор всех {$extra}купонов {$city->name_in}{$areaStr}{$districtStr}{$streetStr}{$metroStr}. Каталог акций, распродаж и больших скидок до 70%. Найти или подобрать {$extra1}купон на скидку{$areaStr}{$districtStr}{$streetStr}{$metroStr}, сравнить цены.";
        } else {
            $str = "Агрегатор всех {$extra}купонов на {$tag->name_where} {$city->name_in}{$areaStr}{$districtStr}{$streetStr}{$metroStr}. Каталог акций, распродаж и больших скидок до 70%. Найти или подобрать {$extra1}купоны на {$tag->name_where}{$areaStr}{$districtStr}{$streetStr}{$metroStr}, сравнить цены.";
        }

        return $str;
    }

    public function getEmptyOfferText()
    {
        $str = "Купоны именно на <strong>{$this->name_where}</strong> сегодня отсутствуют. Они были раньше и, вероятно, появятся еще. <br>
Если вы искали {$this->name_where}, возможно, вам понравятся схожие купоны.";

        return $str;
    }

    public function setTagsChildren($tagIds, $deleteOld = false)
    {
        if (!is_array($tagIds)) {
            $tagIds = [];
        }

        $tags = $this->tagChildren;

        if ($tags && $deleteOld) {
            foreach ($tags as $tag) {
                if (!in_array($tag->id, $tagIds)) {
                    $sql = "DELETE FROM tag_tag WHERE tag_id_parent = {$this->id} AND tag_id = {$tag->id}";
                    \Yii::$app->getDb()->createCommand($sql)->execute();
                }
            }
        }

        foreach ($tagIds as $tagId) {
            $tagId = trim($tagId);
            // Это если прилете новй строковой тег
            if (!is_numeric($tagId)) {
                continue;
            }
            $sql = "SELECT * FROM tag_tag WHERE tag_id_parent = {$this->id} AND tag_id = {$tagId}";
            $tagExists = \Yii::$app->getDb()->createCommand($sql)->queryOne();

            if (!$tagExists) {
                $sql = "INSERT INTO tag_tag (tag_id_parent, tag_id) VALUES({$this->id}, {$tagId})";
                \Yii::$app->getDb()->createCommand($sql)->execute();
            }
        }
    }

    private function isMainPage()
    {
        return $this->id == static::TAG_ID_MAIN_PAGE;
    }
}
