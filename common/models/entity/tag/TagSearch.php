<?php

namespace common\models\entity\tag;

use common\models\entity\tag\Tag;
use dosamigos\arrayquery\ArrayQuery;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\db\ActiveQuery;

/**
 * TagSearch represents the model behind the search form of `common\models\entity\offer\Offer`.
 */
class TagSearch extends Tag
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_group'], 'integer'],
            [['name', 'name_where', 'slug',], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $showOffers = $params['show-offers'] ?? 0;
        $showTags = $params['show-tags'] ?? 0;

        $query = Tag::find()
            ->select('tag.*')
            ->addSelect('(SELECT COUNT(*) FROM offer WHERE id IN (
                    SELECT offer_id FROM offer_tag WHERE tag_id = tag.id
                ) AND is_tag_patterned = 0) as offer_count_runtime')
            ->with([
                'tagPatterns',
                'offers' => function (ActiveQuery $q) use ($showOffers) {
                    if (!$showOffers) {
                        $q->andWhere(['is_tag_patterned' => 0]);
                    }
                    $q->limit(10);
                    $q->with(['tags' => function (ActiveQuery $q) {
                        $q->with(['tagPatterns']);
                    }]);
                }]);
        if (!$showTags) {
            $query->andFilterHaving(['>', 'offer_count_runtime', 0]);
        }
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');

            return $dataProvider;
        }
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'is_group' => $this->is_group,
            //'name' => $this->name
            //'created_at' => $this->created_at,
            //'updated_at' => $this->updated_at,
        ]);

        $query
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchOriginMethod($params)
    {

        $query = Tag::find()
            ->select('tag.*');
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');

            return $dataProvider;
        }
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'is_group' => $this->is_group,
            //'name' => $this->name
            //'created_at' => $this->created_at,
            //'updated_at' => $this->updated_at,
        ]);

        $query
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'name_where', $this->name_where])
    ;

        return $dataProvider;
    }

    public function searchArray($params)
    {
        $tree = function (array $data, $removeFromRoot = true) {
            $data = array_column($data, null, 'id');

            foreach ($data as &$node) {
                if (!$node['tagParents']) {
                    //record has no parents - null or empty array
                    continue; //skip
                }
                foreach ($node['tagParents'] as $id) {
                    if (!isset($data[$id])) { // make sure parent exists
                        throw new \RuntimeException(
                            sprintf(
                                'Child id %d is orphaned, no parent %d found',
                                $node['id'], $id
                            )
                        );
                    }
                    if (!isset($data[$id]['children'])) {
                        $data[$id]['children'] = [];
                    }
                    $data[$id]['children'][] = &$node;
                }
            }

            $clean = function (&$data) use (&$clean) {
                foreach ($data as $node) {
                    if (isset($node['children'])) {
                        $ids = array_column($node['children'], 'id');
                        foreach ($ids as $id) {
                            unset($data[$id]);
                        }
                        $clean($node['children']);
                    }
                }
            };

            if ($removeFromRoot) {
                $clean($data);
            }

            return $data;
        };

        //$tagId = $this->getApplication()->getRequest()->get('tagId');
        $tags = Tag::find()
            ->with(['tagParents', 'tagSynonymsChildren'])
            ->indexBy('id')
            ->asArray(true)
            ->all();
        array_walk($tags, function (&$tag) {
            $tag['tagParents'] = $tag['tagParents'] ? array_column($tag['tagParents'], 'id') : [];
        });
        $tags = $tree($tags);

        $query = new ArrayQuery($tags);

        $this->load($params);
        if ($this->name) {
            $query->addCondition('name', '~' . $this->name);
        }
        if ($this->id) {
            $query->addCondition('id', $this->id);
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $query->find(),
            'pagination' => [
                'pageSize' => ceil(count($query->find()) / 2),
            ],
            'sort' => [
                'attributes' => ['id', 'name'],
            ],
        ]);


        return [$dataProvider, $tags];
//vvd($dataProvider);


        //if($tagId){
        //  $tags = [$tags[$tagId]];
        //}

    }
}
