<?php

namespace common\models\entity\seo;

use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "seo".
 *
 * @property int $id
 * @property string $title
 * @property string $h1
 * @property string $h1_sub
 * @property string $seo_stat
 */
class Seo extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => (new \DateTime())->format('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'seo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'h1'], 'string', 'max' => 255],
            [['h1_sub', 'seo_stat'], 'safe'],
        ];
    }
}
