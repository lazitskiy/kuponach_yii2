<?php

namespace common\models\entity\orderQuestion;

use common\traits\base\CreatedAtBehaviorAwareTrait;
use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "order_question".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $category_id
 * @property string $title
 * @property string $text
 * @property int $cost
 * @property string $variant 'standard','extended','vip', 'fix_cost'
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 */
class Question extends \yii\db\ActiveRecord
{
    use CreatedAtBehaviorAwareTrait;

    const STATUS_NEW = 'new';
    const STATUS_WAITING_PAYMENT = 'waiting_payment';
    const STATUS_PAYED = 'payed';
    const STATUS_MODERATE = 'moderate';
    const STATUS_ACTIVE = 'active';
    const STATUS_CLOSED = 'closed';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_question';
    }

    /**
     * @inheritdoc
     * @return QuestionRepository the active query used by this AR class.
     */
    public static function getRepository()
    {
        return new QuestionRepository(get_called_class());
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        $url = [''];

        switch ($this->status) {
            case static::STATUS_NEW:
                $url = ['/get/question-variant', 'questionId' => $this->id];
                break;
            case static::STATUS_WAITING_PAYMENT:
                $url = ['/get/question-pay', 'questionId' => $this->id];
                break;
            case static::STATUS_PAYED:
            case static::STATUS_MODERATE:
            case static::STATUS_ACTIVE:
            case static::STATUS_CLOSED:
                $url = ['/question/index', 'questionId' => $this->id];
                break;
        }

        return Url::to($url);
    }
}
