<?php

namespace common\models\entity\operator;

use common\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "operator".
 *
 * @property int $id
 * @property string $name
 * @property string $site
 * @property string $slug
 * @property string $cost
 * @property string $out_parameter
 * @property string $created_at
 * @property string $updated_at
 *
 */
class Operator extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => (new \DateTime())->format('Y-m-d H:i:s'),
            ], [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'operator';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'site', 'slug', 'cost', 'out_parameter'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     * @return OperatorRepository the active query used by this AR class.
     */
    public static function getRepository()
    {
        return new OperatorRepository(get_called_class());
    }
}
