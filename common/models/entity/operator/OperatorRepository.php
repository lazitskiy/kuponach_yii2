<?php

namespace common\models\entity\operator;
use common\models\entity\city\CityOperatorMap;

/**
 * This is the ActiveQuery class for [[Operator]].
 *
 * @see Operator
 */
class OperatorRepository extends \yii\db\ActiveQuery
{
    public $id;

    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Operator[]|array
     */
    public function all($db = null)
    {
        return parent::all();
    }

    /**
     * @inheritdoc
     * @return Operator|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param $slug
     * @return Operator|null
     */
    public function getBySlug($slug)
    {
        return $this->where(['slug' => $slug])->one();
    }

    public function getCityMap()
    {

    }
}
