<?php

namespace common\models\entity\billing;
use common\traits\base\CreatedAtBehaviorAwareTrait;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "billing_history".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $summ
 * @property string $reason
 * @property string $type
 * @property string $created_at
 * @property string $updated_at
 */
class BillingHistory extends ActiveRecord
{
    use CreatedAtBehaviorAwareTrait;

    /**
     * @inheritdoc
     * @return BillingHistoryRepository the active query used by this AR class.
     */
    public static function getRepository()
    {
        return new BillingHistoryRepository(get_called_class());
    }
}
