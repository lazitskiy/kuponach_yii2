<?php

namespace common\models\entity\billing;

/**
 * This is the model class for table "billing_balance".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $summ
 * @property integer $fix_cost_question_count Количество вопросов по фиксированной цене. Цена в common\components\Application::USER_REGISTER_QUESTION_FIX_PRICE_COST
 */
class BillingBalance extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     * @return BillingBalanceRepository the active query used by this AR class.
     */
    public static function getRepository()
    {
        return new BillingBalanceRepository(get_called_class());
    }
}
