<?php

namespace common\models\entity\billing;

/**
 * This is the ActiveQuery class for [[BillingHistory]].
 *
 * @see BillingHistory
 */
class BillingHistoryRepository extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return BillingHistory[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return BillingHistory|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
