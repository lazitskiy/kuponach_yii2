<?php

namespace common\models\entity\city;

use common\models\entity\operator\Operator;

/**
 * This is the ActiveQuery class for [[City]].
 *
 * @see City
 */
class CityRepository extends \yii\db\ActiveQuery
{
    protected function active()
    {
        return $this->andWhere('[[is_active]]=1');
    }

    protected function withOperatorMap($operatorId)
    {
        $this->with(['operatorMap' => function (\yii\db\ActiveQuery $query) use ($operatorId) {
            $query->andOnCondition(['operator_id' => $operatorId]);
        }]);
    }

    /**
     * @inheritdoc
     * @return City[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return City|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param Operator|null $operator
     * @return array|City[]
     */
    public function getAllActive(Operator $operator = null)
    {
        if ($operator) {
            $this->withOperatorMap($operator->id);
        }

        return $this->active()->all();
    }

    /**
     * @param Operator $operator
     * @return array|CityOperatorMap[]
     */
    public function getOperatorCities(Operator $operator)
    {
        return CityOperatorMap::getRepository()->getByOperatorId($operator->id);
    }

    /**
     * @param $name
     * @return City|null
     */
    public function getByName($name)
    {
        return $this->where(['name' => $name])->one();
    }
}
