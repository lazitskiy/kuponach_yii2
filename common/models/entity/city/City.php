<?php

namespace common\models\entity\city;

use common\helpers\UrlHelper;
use common\traits\base\CreatedAtBehaviorAwareTrait;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Url;

/**
 * This is the model class for table "city".
 *
 * @property integer $id
 * @property string $fias_id
 * @property string $name
 * @property string $name_in
 * @property string $slug
 * @property string $region_fias_id
 * @property string $region_with_type
 * @property string $data
 * @property boolean $is_active
 * @property boolean $is_important
 * @property string $created_at
 * @property string $updated_at
 */
class City extends \yii\db\ActiveRecord
{
    // Moscow
    const CITY_ID_DEFAULT = 1;
    const IS_IMPORTANT_TEXT = 'Большие города';

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => (new \DateTime())->format('Y-m-d H:i:s'),
            ],
        ];
    }

    public static function tableName()
    {
        return 'city';
    }

    public function rules()
    {
        return [
            [['id', 'is_active', 'is_important'], 'integer'],
            [['fias_id', 'name', 'name_in', 'slug', 'region_fias_id', 'region_with_type', 'data'], 'string'],
            [['name', 'slug', 'region_with_type'], 'string', 'max' => 255],
            [['fias_id', 'slug', 'region_fias_id'], 'string', 'max' => 64],
            [['slug'], 'unique'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     * @return CityRepository the active query used by this AR class.
     */
    public static function getRepository()
    {
        return new CityRepository(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperatorMap()
    {
        return $this->hasOne(CityOperatorMap::class, ['city_id' => 'id']);
    }

    public function getUrl()
    {
        $url = UrlHelper::getCityIndexUrl($this->slug);

        return $url;
    }

    public function getUrlTitle()
    {
        return 'Купоны в ' . $this->name_in;
    }


    public function isDefaultCity()
    {
        return $this->id == static::CITY_ID_DEFAULT;
    }
}
