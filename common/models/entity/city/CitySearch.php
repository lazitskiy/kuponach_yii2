<?php

namespace common\models\entity\city;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\entity\city\City;

/**
 * CitySearch represents the model behind the search form of `common\models\entity\city\City`.
 */
class CitySearch extends City
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_active', 'is_important'], 'integer'],
            [['fias_id', 'name', 'slug', 'region_fias_id', 'region_with_type', 'data', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = City::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'is_active' => $this->is_active,
            'is_important' => $this->is_important,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'fias_id', $this->fias_id])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'region_fias_id', $this->region_fias_id])
            ->andFilterWhere(['like', 'region_with_type', $this->region_with_type])
            ->andFilterWhere(['like', 'data', $this->data]);

        return $dataProvider;
    }
}
