<?php

namespace common\models\entity\city;

/**
 * This is the ActiveQuery class for [[CityOperatorMap]].
 *
 * @see CityOperatorMap
 */
class CityOperatorMapRepository extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/
    protected function withCity()
    {
        return $this->with('city');
    }

    /**
     * @inheritdoc
     * @return CityOperatorMap[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return CityOperatorMap|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param $operatorId
     * @return array|CityOperatorMap[]
     */
    public function getByOperatorId($operatorId)
    {
        return $this->withCity()
            ->indexBy('operator_city_name')
            ->where(['operator_id' => $operatorId])
            ->all();
    }
}
