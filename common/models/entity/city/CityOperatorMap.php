<?php

namespace common\models\entity\city;

use Yii;

/**
 * This is the model class for table "city_operator_map".
 *
 * @property int $id
 * @property int $city_id
 * @property int $operator_id
 * @property int $operator_city_id
 * @property string $operator_city_name
 */
class CityOperatorMap extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'city_operator_map';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id', 'operator_id',], 'required'],
            [['city_id', 'operator_id', 'operator_city_id'], 'integer'],
            [['operator_city_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     * @return CityOperatorMapRepository the active query used by this AR class.
     */
    public static function getRepository()
    {
        return new CityOperatorMapRepository(get_called_class());
    }

    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }
}
