<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 05.08.17
 * Time: 15:06
 */

namespace common\models\entity\user;

use common\models\entity\billing\BillingBalance;
use common\models\entity\orderQuestion\Question;


/**
 * Class Profile
 * @package common\models\entity\user
 *
 * @property $questions Question[]
 * @property $question Question
 * @property $profile Profile Активный вопрос(последний)
 * @property $balance BillingBalance Баланс пользователя
 */
class User extends \dektrium\user\models\User
{
    public function getQuestions()
    {
        return $this->hasMany(Question::class, ['user_id' => 'id']);
    }

    public function getQuestion()
    {
        return $this->hasOne(Question::class, ['user_id' => 'id'])
            ->orderBy(['id' => SORT_DESC]);
    }

    public function getBalance()
    {
        return $this->hasOne(BillingBalance::class, ['user_id' => 'id']);
    }
}
