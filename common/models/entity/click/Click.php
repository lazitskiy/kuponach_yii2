<?php

namespace common\models\entity\click;

use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "click".
 *
 * @property int $id
 * @property int $operator_id
 * @property string $created_at
 */
class Click extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'created_at',
                ],
                'value' => function ($event) {
                    return date('Y-m-d');
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'click';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['operator_id'], 'integer'],
            [['created_at'], 'safe'],
        ];
    }
}
