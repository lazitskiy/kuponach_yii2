<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 25.09.17
 * Time: 0:32
 */

namespace common\behaviors;


use common\helpers\Inflector;

class SluggableBehavior extends \yii\behaviors\SluggableBehavior
{
    public $immutable = true;

    protected function generateSlug($slugParts)
    {
        return Inflector::slug(implode('-', $slugParts));
    }

}
