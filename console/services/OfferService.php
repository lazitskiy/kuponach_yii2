<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 07.10.17
 * Time: 17:23
 */

namespace console\services;


use common\helpers\Inflector;
use common\models\entity\city\CityOperatorMap;
use common\models\entity\company\Company;
use common\models\entity\company\CompanyAddress;
use common\models\entity\operator\Operator;
use console\controllers\ImportController;
use console\models\import\AbstractBaseOffer;
use console\models\import\MegakuponOffer;
use console\models\operator\helpers\StringHelper;

class OfferService
{
    CONST PARAM_OPERATOR_CITY_IDS = 'param_operator_city_ids';

    CONST PARAM_OPERATOR_CITY_MAP = 'param_operator_city_map';

    protected $params;

    /**
     * @var array
     */
    protected $cache = [
        'operatorCityMap' => [],
    ];


    /**
     * @var Operator
     */
    protected $operator;

    public function __construct(Operator $operator)
    {
        $this->operator = $operator;
    }

    /**
     * @return Operator
     */
    public function getOperator(): Operator
    {
        return $this->operator;
    }

    /**
     * @param $name
     * @return mixed
     * @throws \Exception
     */
    public function getParam($name)
    {
        if (!$this->params[$name]) {
            throw new \Exception("Param not exists [$name]");
        }

        return $this->params[$name];
    }

    /**
     * @param $name
     * @param $value
     */
    public function setParam($name, $value)
    {
        $this->params[$name] = $value;
    }

    /**
     * @param array $operatorCityIds Массим из ИДшнико городов ОПЕРАТОРА!
     * @return array
     */
    public function getCityIdsFromOperatorIds($operatorCityIds = null)
    {
        $cityIds = [];
        if (!$operatorCityIds) {
            return [];
        }

        if (!$this->cache['operatorCityMap']) {
            $this->cache['operatorCityMap'] = CityOperatorMap::getRepository()->getByOperatorId($this->operator->id);
        }
        /** @var CityOperatorMap[] $operatorCityMap */
        $operatorCityMap = $this->cache['operatorCityMap'];
        foreach ($operatorCityIds as $operatorCityId) {
            foreach ($operatorCityMap as $item) {
                if ($item->operator_city_id == $operatorCityId) {
                    $cityIds[] = $item->city_id;
                }
            }
        }

        return $cityIds;
    }

    /**
     * @param array $operatorCityNames Массим из городов ОПЕРАТОРА!
     * @return array
     */
    public function getCityIdsFromOperatorNames($operatorCityNames = [])
    {
        $cityIds = [];
        if (!$operatorCityNames) {
            return [];
        }

        if (!$this->cache['operatorCityMap']) {
            $this->cache['operatorCityMap'] = CityOperatorMap::getRepository()->getByOperatorId($this->operator->id);
        }

        /** @var CityOperatorMap[] $operatorCityMap */
        $operatorCityMap = $this->cache['operatorCityMap'];
        foreach ($operatorCityNames as $operatorCityName) {
            foreach ($operatorCityMap as $item) {
                if ($item->operator_city_name == $operatorCityName) {
                    $cityIds[] = $item->city_id;
                }
            }
        }

        return $cityIds;
    }


    /**
     * @param $operatorCompanyName string
     * @param $operatorCompanyId int
     * @param array $params
     * @return int|null
     * @throws \Exception
     */
    public function getCompanyIdFromOperatorName($operatorCompanyName, $operatorCompanyId = null, $params = [])
    {
        $wheres['operator_id'] = $this->operator->id;
        if ($operatorCompanyId) {
            $wheres['operator_company_id'] = $operatorCompanyId;
        } else {
            $wheres['slug'] = Inflector::slug($operatorCompanyName);
        }

        $company = Company::findOne($wheres);

        if (!$company) {
            $company = new Company();
            $company->operator_id = $this->operator->id;
            $company->operator_company_id = $operatorCompanyId;
            $company->name = $operatorCompanyName;

            if (!empty($params['worktime'])) {
                $company->worktime = $params['worktime'];
            }
            if (!empty($params['phone'])) {
                $company->phone = $params['phone'];
            }
            if (!empty($params['site'])) {
                $company->site = StringHelper::normalizeUrl($params['site']);
            }

            if (!$company->save()) {
                throw new \Exception(print_r($company->getFirstErrors(), true));
            }
        }

        if (!empty($params['address'])) {
            foreach ($params['address'] as $addr) {
                $address = $addr['address'] ?? null;
                if ($address) {
                    $companyAddress = CompanyAddress::findOne([
                        'company_id' => $company->id,
                        'address' => $address,
                    ]);
                    if (!$companyAddress) {
                        $companyAddress = new CompanyAddress();
                        $companyAddress->company_id = $company->id;
                        $companyAddress->address = $address;
                        if (!empty($addr['lat'])) {
                            $companyAddress->lat = $addr['lat'];
                        }
                        if (!empty($addr['lon'])) {
                            $companyAddress->lon = $addr['lon'];
                        }
                        if (!$companyAddress->save()) {
                            throw new \Exception(print_r($companyAddress->getFirstErrors(), true));
                        }
                    }
                }
            }
        }

        return $company->id;

    }

    /**
     * @param $offer
     * @return AbstractBaseOffer
     */
    public function populateOffer($offer)
    {
        $className = ImportController::OPERATORS[$this->operator->slug]['className'] . 'Offer';
        /** @var AbstractBaseOffer $offer */
        $offer = new $className($offer, $this);

        return $offer;
    }
}
