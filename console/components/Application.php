<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 10.09.17
 * Time: 13:49
 */


namespace console\components;

use mito\sentry\Component;
use sevenfloor\sendpulse\SendPulse;
use yii\mutex\Mutex;

/**
 * Class Application
 * @package console\components
 *
 * @property Mutex $mutex
 * @property SendPulse $sendpulse
 * @property Component $sentry
 *
 */
class Application extends \yii\console\Application
{
    /**
     * @return Component
     */
    public function getSentry()
    {
        return $this->sentry;
    }

    /**
     * @return SendPulse
     */
    public function getSendpulse()
    {
        return $this->sendpulse;
    }
}
