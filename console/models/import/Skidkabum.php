<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 14.09.17
 * Time: 21:51
 */

namespace console\models\import;


use common\models\entity\city\City;
use common\models\entity\city\CityOperatorMap;
use common\models\entity\offer\Offer;
use common\services\TagMatchService;
use console\models\operator\helpers\StringHelper;
use console\services\OfferService;
use DiDom\Document;
use yii\base\InvalidParamException;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

class Skidkabum extends AbstractBaseOperator
{
    public function process()
    {
        //Сначала обработаем списко городов
        $newCities = $this->processCities();
        if ($newCities) {
            $message = "Есть новые города, необходимо отработать в ручную перед парсингом [{$this->operator->name}]";
            $this->logCritical($message . print_r($newCities, true));
            $this->emailService->newCityDetected($this->operator, $newCities);

            return 0;
        }

        $this->processOffers();
    }

    protected function processCities()
    {
        $filename = $this->getFileOffers();

        $document = new Document();
        $document->loadXmlFile($filename);
        $cities = $document->first('cities')->find('city');

        $operatorCities = [];
        foreach ($cities as $cityXml) {
            $operatorCityId = StringHelper::toNorm($cityXml->getAttribute('id'));
            $operatorCityName = StringHelper::toNorm($cityXml->text());

            $operatorCities[$operatorCityName] = [
                'operatorCityId' => $operatorCityId,
                'operatorCityName' => $operatorCityName,
            ];
        }

        $newCities = $this->createNewCityIfNeed($operatorCities);

        return $newCities;
    }

    /**
     * @return bool
     */
    protected function processOffers()
    {
        $filename = $this->getFileOffers('default', 'offers.json');

        try {
            $file = file_get_contents($filename);
            $decoded = Json::decode($file);
        } catch (InvalidParamException $e) {
            $msg = self::ERROR_JSON_FILE_CORRUPT . ' ' . $filename . ' ' . $e->getMessage();
            $this->logCritical($msg);

            return false;
        }

        $offers = $decoded['actions'] ?? [];
        if (!$offers) {
            $msg = self::ERROR_JSON_HAS_NO_OFFERS . ' ' . $filename;
            $this->logCritical($msg);

            return false;
        }

        $offerService = new OfferService($this->operator);
        $tagMatchService = new TagMatchService();

        foreach ($offers as $offerData) {
            try {
                $this->app->getDb()->transaction(function () use ($offerData, $offerService, $tagMatchService, &$mails) {
                    $data = $offerService->populateOffer($offerData);
                    $offer = Offer::findOne([
                        'operator_id' => $this->operator->id,
                        'operator_offer_id' => $data->getOperatorOfferId(),
                    ]);
                    if (!$offer) {
                        $offer = new Offer();
                    }
                    $offer->setAttributes($data->getAttributes());
                    $offer->save();

                    $offer->setImages($data->getImages());
                    $tagIds = $tagMatchService->getMatchedTagIds($offer);
                    if ($tagIds) {
                        $offer->setTags($tagIds);
                    }

                    if ($errors = $offer->getFirstErrors()) {
                        throw new \Exception(print_r($errors, true));
                    }
                });
            } catch (\Throwable $e) {
                $msg = "Новый оффер {$offerData['url']} не сохранен [{$e->getMessage()}]";
                $this->logError($msg);
            }
        }

        return true;
    }

}
