<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 10.09.17
 * Time: 14:13
 */

namespace console\models\import;

use common\models\entity\city\CityOperatorMap;
use common\models\entity\company\Company;
use common\models\entity\company\CompanyAddress;
use common\models\entity\operator\Operator;
use common\services\email\EmailService;
use common\models\entity\city\City;
use common\traits\base\EmailServiceAwareTrait;
use console\models\exceptions\ImportException;
use console\models\operator\helpers\StringHelper;
use console\traits\ApplicationConsoleAwareTrait;
use console\traits\LoggerHelperAwareTrait;
use GuzzleHttp\Client;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;


/**
 *
 * Class AbstractOperator
 * @package console\models\import\operator
 */
abstract class AbstractBaseOperator
{
    const ERROR_JSON_FILE_CORRUPT = 'json_file_corrupt';
    const ERROR_JSON_HAS_NO_OFFERS = 'json_has_no_offers';

    use ApplicationConsoleAwareTrait;
    use EmailServiceAwareTrait;
    use LoggerHelperAwareTrait;
    /**
     * @var ApplicationConsoleAwareTrait
     */
    protected $app;

    /**
     * @var Client
     */
    protected $httpClient;

    /**
     * @var Operator
     */
    protected $operator;

    /**
     * @var string
     */
    protected $operatorName;

    /**
     * @var array
     */
    protected $operatorImportParams;

    /**
     * @var EmailService
     */
    protected $emailService;

    /**
     * @var array
     */
    protected $cache = [
        'operatorCityMap' => [],
    ];

    /**
     * AbstractBaseOperator constructor.
     * @param $slug string
     * @param $operatorImportParams array
     * @throws ImportException
     */
    public function __construct($slug, $operatorImportParams)
    {
        $this->operator = Operator::getRepository()->getBySlug($slug);
        if (!$this->operator) {
            throw new ImportException("Оператор $slug не найден в базе. Создайте тут http://xxx.kuponach.ru/operator");
        }

        $this->operatorImportParams = $operatorImportParams;

        $this->app = $this->getApplicationConsole();

        $this->httpClient = new Client([
            'timeout' => 180,
        ]);

        $this->emailService = $this->getEmailService();

        FileHelper::createDirectory($this->getDownloadDir());
    }

    protected function getDownloadUrl($type = 'default')
    {
        return $this->operatorImportParams['url'][$type] ?? null;
    }

    protected function getDownloadDir()
    {
        return \Yii::getAlias($this->operatorImportParams['downloadFileDir'] . '/' . $this->operator->slug);
    }

    abstract public function process();

    abstract protected function processCities();

    abstract protected function processOffers();

    /**
     * Из XML все положить
     * @return bool
     */
    public function import()
    {
        try {
            $this->process();

            return true;
        } catch (\Exception $e) {
            $this->logCritical($e->getMessage());

            return false;
        }
    }

    /**
     * 1. Качает файл сюда console/runtime/import/{operatorName}/$toName
     *
     * @param string $type
     * @param string $toName
     * @param bool $force
     * @return string
     */
    protected function getFileOffers($type = 'xml', $toName = 'offers.xml', $force = true)
    {
        $url = $this->getDownloadUrl($type);
        $filename = $this->getDownloadDir() . '/' . $toName;

        if (!file_exists($filename) || $force) {
            $this->httpClient->get($url, [
                'sink' => $filename,
            ]);
        }

        return $filename;
    }

    /**
     * 2. Создает города у нас в базе
     *
     * @param array $operatorCityData
     * Array (
     *     [Искитим] => Array
     *     (
     *         [operatorCityName] => Искитим
     *         [operatorCityId] => 321
     *     )
     * )
     * @return array
     */
    protected function createNewCityIfNeed(array $operatorCityData)
    {
        $newCities = [];
        $cityOperators = City::getRepository()->getOperatorCities($this->operator);
        $cityOperators = ArrayHelper::index($cityOperators, 'operator_city_name');

        foreach ($operatorCityData as $data) {
            $operatorCityName = $data['operatorCityName'];
            $operatorCityId = $data['operatorCityId'] ?? null;

            if (!isset($cityOperators[$operatorCityName])) {
                try {
                    $this->app->getDb()->transaction(function () use ($operatorCityId, $operatorCityName, $newCities) {
                        $operatorId = $this->operator->id;

                        $city = City::getRepository()->getByName($operatorCityName);
                        if (!$city) {
                            $city = new City();
                            $city->name = $operatorCityName;
                            $city->is_active = 0;
                            $city->save();

                            $newCities[] = $operatorCityName;
                        }

                        $cityOperator = new CityOperatorMap();
                        $cityOperator->city_id = $city->id;
                        $cityOperator->operator_id = $operatorId;
                        $cityOperator->operator_city_name = $operatorCityName;
                        $cityOperator->operator_city_id = $operatorCityId;
                        $cityOperator->save();

                        $errors = array_merge($city->getFirstErrors(), $cityOperator->getFirstErrors());
                        if ($errors) {
                            throw new \Exception(print_r($errors, true));
                        }
                    });
                } catch (\Throwable $e) {
                    $msg = "Новый город {$operatorCityName} не сохранен [{$e->getMessage()}]";
                    $this->logError($msg);
                }
            }
        }

        return $newCities;
    }
}
