<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 07.10.17
 * Time: 16:47
 */

namespace console\models\import;


use console\services\OfferService;
use DiDom\Element;

abstract class AbstractBaseOffer
{
    /**
     * @var Element|array
     */
    protected $offerData;

    /**
     * @var OfferService
     */
    protected $offerService;

    public function __construct($offerData, OfferService $offerService)
    {
        $this->offerData = $offerData;
        $this->offerService = $offerService;
        $this->images = [];

        $this->setCityIds();
        $this->setCategoryIds();
        $this->setIsActive();
        $this->setIsHit();
        $this->setCompanyId();
        $this->setOperatorId();
        $this->setOperatorOfferId();
        $this->setName();
        $this->setDescription();
        $this->setUrl();
        $this->setDateStart();
        $this->setDateEnd();
        $this->setValidStart();
        $this->setValidEnd();
        $this->setDiscountPercent();
        $this->setPriceOriginal();
        $this->setPriceDiscounted();
        $this->setPriceCouponMin();
        $this->setImages();
    }

    /**
     * @var string [1,2,3,4]
     */
    public $cityIds;

    /**
     * @var bool
     */
    public $isFederal;

    /**
     * @var string [1,2,3,4]
     */
    protected $categoryIds;

    /**
     * @var bool
     */
    protected $isActive;

    /**
     * @var bool
     */
    protected $isHit;

    /**
     * @var int
     */
    protected $companyId;

    /**
     * @var int
     */
    protected $operatorId;

    /**
     * @var int
     */
    protected $operatorOfferId;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $nameShort;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var string
     */
    protected $descriptionShort;

    /**
     * @var string
     */
    protected $descriptionAttension;

    /**
     * @var string
     */
    protected $descriptionTips;

    /**
     * @var string
     */
    protected $descriptionAdv;

    /**
     * @var string
     */
    protected $url;

    /**
     * @var \DateTime
     */
    protected $dateStart;

    /**
     * @var \DateTime
     */
    protected $dateEnd;

    /**
     * @var \DateTime
     */
    protected $validStart;

    /**
     * @var \DateTime
     */
    protected $validEnd;

    /**
     * @var int
     */
    protected $discountPercent;

    /**
     * @var int
     */
    protected $priceOriginal;

    /**
     * @var int
     */
    protected $priceDiscounted;

    /**
     * @var int
     */
    protected $priceCouponMin;

    /**
     * @var array
     */
    protected $images;


    abstract protected function setCityIds();

    abstract protected function setCategoryIds();

    abstract protected function setIsActive();

    abstract protected function setIsHit();

    abstract protected function setCompanyId();

    abstract protected function setOperatorOfferId();

    abstract protected function setName();

    abstract protected function setDescription();

    abstract protected function setUrl();

    abstract protected function setDateStart();

    abstract protected function setValidStart();

    abstract protected function setDateEnd();

    abstract protected function setValidEnd();

    abstract protected function setDiscountPercent();

    abstract protected function setPriceOriginal();

    abstract protected function setPriceDiscounted();

    abstract protected function setPriceCouponMin();

    abstract protected function setImages();

    protected function setOperatorId()
    {
        $this->operatorId = $this->offerService->getOperator()->id;
    }

    /**
     * @return int
     */
    public function getOperatorOfferId(): int
    {
        return $this->operatorOfferId;
    }

    /**
     * @return array
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @return array
     */
    public function getAttributes()
    {
        return [
            'city_ids' => $this->cityIds,
            'is_federal' => $this->isFederal,
            'category_ids' => $this->categoryIds,
            'is_active' => $this->isActive,
            'is_hit' => $this->isHit,
            'company_id' => $this->companyId,
            'operator_id' => $this->operatorId,
            'operator_offer_id' => $this->operatorOfferId,
            'name' => $this->name,
            'name_short' => $this->nameShort,
            'description' => $this->description,
            'description_short' => $this->descriptionShort,
            'description_attension' => $this->descriptionAttension,
            'description_tips' => $this->descriptionTips,
            'description_adv' => $this->descriptionAdv,
            'url' => $this->url,
            'date_start' => $this->dateStart ? $this->dateStart->format('Y-m-d H:i:s') : null,
            'date_end' => $this->dateEnd ? $this->dateEnd->format('Y-m-d H:i:s') : null,
            'valid_start' => $this->validStart ? $this->validStart->format('Y-m-d H:i:s') : null,
            'valid_end' => $this->validEnd ? $this->validEnd->format('Y-m-d H:i:s') : null,
            'discount_percent' => $this->discountPercent,
            'price_original' => $this->priceOriginal,
            'price_discounted' => $this->priceDiscounted,
            'price_coupon_min' => $this->priceCouponMin,
            'images' => $this->images,
        ];
    }
}
