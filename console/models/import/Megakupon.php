<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 14.09.17
 * Time: 21:51
 */

namespace console\models\import;


use common\models\entity\city\City;
use common\models\entity\city\CityOperatorMap;
use common\models\entity\offer\Offer;
use common\services\TagMatchService;
use console\models\operator\helpers\DomHelper;
use console\models\operator\helpers\StringHelper;
use console\services\OfferService;
use DiDom\Document;
use yii\base\InvalidParamException;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

class Megakupon extends AbstractBaseOperator
{
    static $operatorCities;

    public function process()
    {
        //Сначала обработаем списко городов
        $newCities = $this->processCities();
        if ($newCities) {
            $message = "Есть новые города, необходимо отработать в ручную перед парсингом [{$this->operator->name}]";
            $this->logCritical($message . print_r($newCities, true));
            $this->emailService->newCityDetected($this->operator, $newCities);

            return 0;
        }

        $this->processOffers();
    }

    protected function processCities()
    {
        $filename = $this->getFileOffers('xml', 'offers.xml');

        $document = new Document();
        $document->loadXmlFile($filename);
        $regions = $document->find('region');

        $operatorCities = [];
        foreach ($regions as $region) {
            $operatorCityName = StringHelper::toNorm($region->text());

            $operatorCities[$operatorCityName] = [
                'operatorCityName' => $operatorCityName,
            ];
        }

        static::$operatorCities = $operatorCities;

        $newCities = $this->createNewCityIfNeed($operatorCities);

        return $newCities;
    }

    /**
     * @return bool
     */
    protected function processOffers()
    {
        $filename = $this->getFileOffers('xml', 'offers.xml', false);

        $document = new Document();
        $document->loadXmlFile($filename);
        $offers = $document->find('offer');

        if (!$offers) {
            $this->logCritical(self::ERROR_JSON_HAS_NO_OFFERS . ' ' . $filename);

            return false;
        }

        $offerService = new OfferService($this->operator);
        $offerService->setParam(OfferService::PARAM_OPERATOR_CITY_IDS, static::$operatorCities);
        $tagMatchService = new TagMatchService();

        foreach ($offers as $offerData) {
            try {
                $this->app->getDb()->transaction(function () use ($offerData, $offerService, $tagMatchService) {
                    $data = $offerService->populateOffer($offerData);
                    $offer = Offer::findOne([
                        'operator_id' => $this->operator->id,
                        'operator_offer_id' => $data->getOperatorOfferId(),
                    ]);
                    if (!$offer) {
                        $offer = new Offer();
                    }
                    $offer->setAttributes($data->getAttributes());
                    $offer->save();

                    $offer->setImages($data->getImages());
                    $tagIds = $tagMatchService->getMatchedTagIds($offer);
                    if ($tagIds) {
                        $offer->setTags($tagIds);
                    }

                    if ($errors = $offer->getFirstErrors()) {
                        throw new \Exception(print_r($errors, true));
                    }
                });
            } catch (\Throwable $e) {
                $url = $offerData->first('url')->text();
                $msg = "Новый оффер {$url} не сохранен [{$e->getMessage()}]";
                $this->logError($msg);
            }
        }

        return true;
    }
}
