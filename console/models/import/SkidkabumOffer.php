<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 07.10.17
 * Time: 16:47
 */

namespace console\models\import;


use console\models\operator\helpers\StringHelper;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

class SkidkabumOffer extends AbstractBaseOffer
{
    protected function setCityIds()
    {
        $cityIds = [];

        $operatorCityIds = $this->offerData['cities'];
        if (count($operatorCityIds) > 30) {
            $this->isFederal = 1;
        } else {
            $cityIds = $this->offerService->getCityIdsFromOperatorIds($operatorCityIds);
            $this->isFederal = 0;
        }

        $this->cityIds = Json::encode($cityIds);
    }

    protected function setCategoryIds()
    {
        $this->categoryIds = Json::encode([]);
    }

    protected function setIsActive()
    {
        $this->isActive = 1;
    }

    protected function setIsHit()
    {
        $this->isHit = trim($this->offerData['bestseller']);
    }

    protected function setCompanyId()
    {
        $id = trim($this->offerData['company']['id']) ?? null;
        $name = trim($this->offerData['company']['name']) ?? null;


        $companyId = $this->offerService->getCompanyIdFromOperatorName($name, $id, [
                'worktime' => $this->offerData['worktime'] ?? null,
                'phone' => $this->offerData['phoneNumber'] ?? null,
                'site' => $this->offerData['site'] ?? null,
                'address' => array_map(function ($data) {
                    return [
                        'lat' => $data['geoY'] ?? null,
                        'lon' => $data['geoX'] ?? null,
                        'address' => $data['address'] ?? null,
                    ];
                }, $this->offerData['address']),
            ]
        );

        $this->companyId = $companyId;
    }

    protected function setOperatorOfferId()
    {
        $operatorOfferId = trim($this->offerData['id']) ?? null;
        $this->operatorOfferId = $operatorOfferId;
    }

    protected function setName()
    {
        $name = trim($this->offerData['name']);
        $this->name = $name;

        $nameShort = trim($this->offerData['shortName']);
        $this->nameShort = $nameShort;
    }

    protected function setDescription()
    {
        $description = StringHelper::absolutizeImgSrc($this->offerService->getOperator(), $this->offerData['describe'] ?? null);
        $this->description = $description;

        $descriptionAttension = trim($this->offerData['describeAttention']) ?? null;
        $this->descriptionAttension = $descriptionAttension;

        $descriptionTips = trim($this->offerData['describeTips']) ?? null;
        $this->descriptionTips = $descriptionTips;

        $descriptionAdv = trim($this->offerData['describeAdvertisment']) ?? null;
        $this->descriptionAdv = $descriptionAdv;

        $descriptionShort = trim($this->offerData['describeShort']) ?? null;
        $this->descriptionShort = $descriptionShort;
    }

    protected function setUrl()
    {
        $url = trim($this->offerData['url']);
        $this->url = $url;
    }

    protected function setDateStart()
    {
        $dateStart = trim($this->offerData['dateStart']);
        $this->dateStart = new \DateTime($dateStart);
    }

    protected function setDateEnd()
    {
        $dateEnd = trim($this->offerData['dateEnd']);
        $this->dateEnd = new \DateTime($dateEnd);
    }

    protected function setValidStart()
    {
        $this->validStart = null;
    }

    protected function setValidEnd()
    {
        $this->validEnd = null;
    }

    protected function setDiscountPercent()
    {
        $discountPercent = trim($this->offerData['discountPercent']);
        $this->discountPercent = $discountPercent;
    }

    protected function setPriceOriginal()
    {
        $priceOriginal = trim($this->offerData['priceBeforeDiscount']);
        $this->priceOriginal = $priceOriginal;
    }

    protected function setPriceDiscounted()
    {
        $priceDiscounted = trim($this->offerData['price']);
        $this->priceDiscounted = $priceDiscounted;
    }

    protected function setPriceCouponMin()
    {
        $priceCoupon = !empty($this->offerData['coupons']) ? min(ArrayHelper::getColumn($this->offerData['coupons'], 'price')) : 0;
        $this->priceCouponMin = $priceCoupon;
    }

    protected function setImages()
    {
        $main = $this->offerData['mainPhoto'] ?? null;
        if ($main) {
            $this->images[] = StringHelper::normalizeUrl($main);
        }
        if (preg_match_all('/\<img.*?src=\"(.*?)\"/uis', $this->description, $matches)) {
            foreach ($matches[1] as $match) {
                $this->images[] = StringHelper::normalizeUrl($match);
            }
        }
    }
}
