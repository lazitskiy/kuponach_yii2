<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 07.10.17
 * Time: 16:47
 */

namespace console\models\import;


use console\models\operator\helpers\StringHelper;
use console\services\OfferService;
use DiDom\Element;
use yii\helpers\Json;

class MegakuponOffer extends AbstractBaseOffer
{
    protected function setCityIds()
    {
        $operatorCities = $this->offerService->getParam(OfferService::PARAM_OPERATOR_CITY_IDS);
        $cityNames = $this->offerData->find('region') ?? [];

        $operatorCityNames = [];
        foreach ($cityNames as $cityName) {
            foreach ($operatorCities as $operatorCity) {
                if ($operatorCity['operatorCityName'] == trim($cityName->text())) {
                    $operatorCityNames[] = $operatorCity['operatorCityName'];
                }
            }
        }

        $cityIds = $this->offerService->getCityIdsFromOperatorNames($operatorCityNames);

        $this->cityIds = Json::encode($cityIds);
    }

    protected function setCategoryIds()
    {
        $this->categoryIds = Json::encode([]);
    }

    protected function setIsActive()
    {
        $this->isActive = 1;
    }

    protected function setIsHit()
    {
        $this->isHit = 0;
    }

    protected function setCompanyId()
    {
        $supplier = $this->offerData->first('supplier');
        $name = trim($supplier->first('name')->text());
        $site = trim($supplier->first('url')->text());
        $phone = trim($supplier->first('tel')->text());

        $address = $supplier->find('address');
        $companyId = $this->offerService->getCompanyIdFromOperatorName($name, null, [
                'phone' => $phone,
                'site' => $site,
                'address' => array_map(function (Element $el) {
                    $cityNames = $this->offerData->find('region') ?? [];

                    $cityName = '';
                    if (count($cityNames) == 1) {
                        $cityName = trim($cityNames[0]->text()) . ' ';
                    }
                    $address = trim($el->first('name')->text());
                    $coords = trim($el->first('coordinates')->text());
                    list($lon, $lat) = $coords ? explode(',', $coords) : [null, null];

                    return [
                        'lat' => $lat,
                        'lon' => $lon,
                        'address' => $cityName . $address,
                    ];
                }, $address),
            ]
        );

        $this->companyId = $companyId;
    }

    protected function setOperatorOfferId()
    {
        $operatorOfferId = trim($this->offerData->first('id')->text());
        $this->operatorOfferId = $operatorOfferId;
    }

    protected function setName()
    {
        $name = trim($this->offerData->first('name')->text());
        $this->name = $name;

        $description = trim($this->offerData->first('description')->text());
        $this->nameShort = $description;
    }

    protected function setDescription()
    {
        $description = trim($this->offerData->first('description')->text());
        $this->description = $description;
    }

    protected function setUrl()
    {
        $url = trim($this->offerData->first('url')->text());
        $this->url = $url;
    }

    protected function setDateStart()
    {
        $dateStart = trim($this->offerData->first('beginsell')->text());
        $this->dateStart = new \DateTime($dateStart);
    }

    protected function setDateEnd()
    {
        $dateEnd = trim($this->offerData->first('endsell')->text());
        $this->dateEnd = new \DateTime($dateEnd);
    }

    protected function setValidStart()
    {
        $validStart = trim($this->offerData->first('beginvalid')->text());
        $this->validStart = new \DateTime($validStart);
    }

    protected function setValidEnd()
    {
        $validEnd = trim($this->offerData->first('endvalid')->text());
        $this->validEnd = new \DateTime($validEnd);
    }

    protected function setDiscountPercent()
    {
        $discountPercent = trim($this->offerData->first('discount')->text());
        $this->discountPercent = $discountPercent;
    }

    protected function setPriceOriginal()
    {
        $this->priceOriginal = 0;
    }

    protected function setPriceDiscounted()
    {
        $priceDiscounted = trim($this->offerData->first('pricecoupon')->text());
        $this->priceDiscounted = $priceDiscounted;
    }

    protected function setPriceCouponMin()
    {
        $this->priceCouponMin = $this->priceDiscounted;
    }

    protected function setImages()
    {
        $main = StringHelper::normalizeUrl($this->offerData->first('picture')->text());
        $this->images[] = $main;
    }

}
