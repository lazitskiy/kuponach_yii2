<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 13.09.17
 * Time: 22:33
 */

namespace console\models\operator;


use console\models\dtos\category\CategoryCollection;
use console\models\dtos\CityDto;
use console\models\operator\helpers\StringHelper;
use DiDom\Document;

class Vigoda extends AbstractOperator
{
    const URL_BASE = 'http://vigoda.ru/';

    protected function buildUrlCity($string)
    {
        // TODO: Implement buildUrlCity() method.
    }

    protected function buildUrlCategory($cityDto, $string)
    {
        // TODO: Implement buildUrlCategory() method.
    }

    protected function getCitiesList()
    {
        $response = $this->httpClient->get(static::URL_BASE);

        $document = Document::create($response->getBody()->getContents());
        $cities = $document->find('.cities .cities-td a');

        $arCities = [];
        foreach ($cities as $city) {
            $cityDto = new CityDto();

            $cityName = StringHelper::toNorm($city->text());
            $cityDto->setName($cityName);

            $cityUrl = $this->buildUrlCity($city->getAttribute('href'));
            $cityDto->setUrl($cityUrl);
            $arCities[] = $cityDto;
        }

        return $arCities;
    }

    protected function parseCategories($categoryCollection, $cityDto, $html)
    {
        // TODO: Implement parseCategories() method.
    }

}
