<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 13.09.17
 * Time: 22:14
 */

namespace console\models\operator;


use console\models\dtos\category\CategoryCollection;
use console\models\dtos\category\CategoryDto;
use console\models\dtos\CityDto;
use console\models\operator\helpers\StringHelper;
use DiDom\Document;

class Biglion extends AbstractOperator
{
    const URL_BASE = 'http://biglion.ru';

    protected function buildUrlCity($string)
    {
        // TODO: Implement buildUrlCity() method.
    }

    protected function buildUrlCategory($cityDto, $string)
    {
        return $string;
    }

    protected function getCitiesList()
    {
        $letters = ['А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ж', 'З', 'И', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Э', 'Ю', 'Я'];
        foreach ($letters as $letter) {

        }
        $response = $this->httpClient->get(static::URL_BASE);

        $document = Document::create($response->getBody()->getContents());
        $cities = $document->find('.top-cities li a');

        $arCities = [];
        foreach ($cities as $city) {
            $cityDto = new CityDto();

            $cityName = StringHelper::toNorm($city->text());
            $cityDto->setName($cityName);

            $cityUrl = $this->buildUrlCity($city->getAttribute('href'));
            $cityDto->setUrl($cityUrl);
            $arCities[] = $cityDto;
        }

        return $arCities;
    }

    protected function parseCategories($categoryCollection, $cityDto, $html)
    {
        $document = Document::create($html);
        $as = $document->find('.menu_containera .submenu-col li a');

        foreach ($as as $k => $a) {

            $name = StringHelper::toNorm($a->text());

            $href = $a->getAttribute('href');
            $href = $this->buildUrlCategory($cityDto, $href);

            $class = $a->getAttribute('class');

            vvd([$name, $href, $class]);

            $document = Document::create($response->getBody()->getContents());
            $divs = $document->find('.offers-subtabs .placement-category-item-wrapper');

            foreach ($divs as $div) {
                $a = $div->first('a');
                $categoryDto = new CategoryDto();

                $name = StringHelper::toNorm($a->text());
                $href = $a->getAttribute('href');
                $href = $this->buildUrlCategory($cityDto, $href);

                $categoryDto->setName($name);
                $categoryDto->setUrl($href);
                $categoryDto->setCityIdLocal($cityDto->getCityIdLocal());

                $subcats = $div->find('.offers-subcategories-item-link');
                if ($subcats) {
                    foreach ($subcats as $subcat) {
                        $categoryTagDto = new CategoryTagDto();

                        $name = StringHelper::toNorm($subcat->text());
                        $categoryTagDto->setNameRu($name);

                        $href = $subcat->getAttribute('href');
                        $href = $this->buildUrlCategory($cityDto, $href);
                        $categoryTagDto->setUrl($href);

                        $categoryDto->setTag($categoryTagDto);
                    }
                }
                $categoryCollection->append($categoryDto);
            }
        }
    }

}
