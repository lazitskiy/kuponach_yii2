<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 12.09.17
 * Time: 10:45
 */

namespace console\models\operator;


use console\models\dtos\category\CategoryCollection;
use console\models\dtos\category\CategoryDto;
use console\models\dtos\category\CategoryTagDto;
use console\models\dtos\CityDto;
use DiDom\Document;
use yii\db\Exception;

class Kuponmania extends AbstractOperator
{
    const URL_BASE = 'http://kuponmania.ru';

    protected function buildUrlCity($string)
    {
        return static::URL_BASE . $string;
    }

    protected function buildUrlCategory($cityDto, $string)
    {
        // TODO: Implement buildUrlCategory() method.
    }

    protected function getCitiesList()
    {
        $response = $this->httpClient->get(static::URL_BASE);

        $document = Document::create($response->getBody()->getContents());
        $cities = $document->find('.head-cityes-list a');

        $arCities = [];
        foreach ($cities as $city) {
            $cityDto = new CityDto();

            $cityName = trim($city->text());
            $cityDto->setName($cityName);

            $cityUrl = $this->buildUrlCity($city->getAttribute('href'));
            $cityDto->setUrl($cityUrl);
            $arCities[] = $cityDto;
        }

        return $arCities;
    }

    protected function parseCategories($categoryCollection, $cityDto, $html)
    {
        $document = Document::create($html);
        $as = $document->find('.main-products-grid a');

        foreach ($as as $a) {
            $categoryDto = new CategoryDto();

            $name = $a->first('.main-pr-text')->text();
            $name = trim($name);
            $categoryDto->setName($name);

            $href = $a->getAttribute('href');
            $categoryDto->setUrl($href);
            $categoryDto->setCityIdLocal($cityDto->getCityIdLocal());

            try {
                $response = $this->httpClient->get($href);
            } catch (Exception $e) {
                $this->logError($e->getMessage());
                continue;
            }

            $document = Document::create($response->getBody()->getContents());
            $els = $document->find('.blue-box li a');
            if (!$els) {
                continue;
            }

            foreach ($els as $el) {
                $categoryTagDto = new CategoryTagDto();

                $name = $el->text();
                $name = trim($name);
                $categoryTagDto->setNameRu($name);

                $href = $el->getAttribute('href');
                $categoryTagDto->setUrl($href);

                $categoryDto->setTag($categoryTagDto);
            }

            $categoryCollection->append($categoryDto);
        }
    }

}
