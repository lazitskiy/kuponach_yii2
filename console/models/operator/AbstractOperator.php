<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 10.09.17
 * Time: 14:13
 */

namespace console\models\operator;

use common\services\email\EmailService;
use common\models\entity\city\City;
use common\traits\base\EmailServiceAwareTrait;
use console\models\dtos\category\CategoryCollection;
use console\models\dtos\CityDto;
use console\traits\ApplicationConsoleAwareTrait;
use GuzzleHttp\Client;


/**
 *
 * Class AbstractOperator
 * @package console\models\operator
 */
abstract class AbstractOperator
{
    use ApplicationConsoleAwareTrait;
    use EmailServiceAwareTrait;

    /**
     * @var ApplicationConsoleAwareTrait
     */
    protected $app;

    /**
     * @var Client
     */
    protected $httpClient;

    /**
     * @var
     */
    protected $operatorName;

    /**
     * @var EmailService
     */
    protected $emailService;

    public function __construct($operatorName)
    {//http://api.skidkabum.ru/actions/get/?request={"userID": 14,"pass":"ku.yTBtpnlXB6","format":"json","countItems":30}
        $this->app = $this->getApplicationConsole();

        $this->httpClient = new Client([
            'timeout' => 30,
        ]);
        $this->operatorName = $operatorName;

        $this->emailService = $this->getEmailService();
    }

    /**
     * Формирователь урлов для городов.
     * @param string $string
     * @return string
     */
    abstract protected function buildUrlCity($string);

    /**
     * Формирователь урлов для категорий.
     * @param CityDto $cityDto
     * @param string $string
     * @return string
     */
    abstract protected function buildUrlCategory($cityDto, $string);

    /**
     * Получаем список городов для парсинга
     *
     * @return CityDto[]
     */
    abstract protected function getCitiesList();

    /**
     * @param CategoryCollection $categoryCollection
     * @param CityDto $cityDto
     * @param string $html
     */
    abstract protected function parseCategories($categoryCollection, $cityDto, $html);

    protected function logInfo($message)
    {
        \Yii::info($message, 'info');
    }

    protected function logError($message)
    {
        \Yii::error($message, 'error');
    }

    protected function logCritical($message)
    {
        \Yii::error($message, 'critical');
    }

    /**
     * 1. Получить список городов
     * 2. Для каждого города получитиь список категория
     * 3. Пройтись собрать офферы
     */
    public function parse()
    {
        /**
         * Получаем города
         */
        $cityDtos = [];
        try {
            $cityDtos = $this->getCitiesList();
        } catch (\Exception $e) {
            $this->logCritical($e->getMessage());
        }

        if (!$cityDtos) {
            $this->logCritical('Список городов пуст');
        } else {
            $this->logInfo('Города получены');
        }

        /**
         * TODO Вынести отправку письма сюда
         * Теперь нужно запаммить города на наши
         */
        $this->citiesMap($cityDtos);

        $categoryCollection = $this->getCategoryList($cityDtos);

        vvd($categoryCollection->getUnique());
        /**
         * Ходим по категориям, забираем офферы
         */
    }

    /**
     * Засовывает наш ИДшник
     * @param CityDto[] $cityDtos
     */
    private function citiesMap(array $cityDtos)
    {
        $cityModels = City::getRepository()
            ->select('id, name')
            ->indexBy('name')
            ->asArray()
            ->all();

        $cityNotExists = [];
        foreach ($cityDtos as $cityDto) {
            $cityLocal = $cityModels[$cityDto->getName()] ?? null;
            if (!$cityLocal) {
                $cityNotExists[] = $cityDto;
            } else {
                $cityDto->setCityIdLocal($cityLocal['id']);
            }
        }

        if ($cityNotExists) {
            $subject = $this->operatorName . ' Новые города';
            $message = 'Есть новые города, необходимо их добавить в ручную перед парсингом';
            $str = '';
            /** @var CityDto $cityNotExist */
            foreach ($cityNotExists as $cityNotExist) {
                $str .= "<br/>{$cityNotExist->getName()} [{$cityNotExist->getUrl()}]";
            }
            $this->emailService->notifyAdmin($message . $str, $subject);

            $this->logCritical($message . print_r($cityNotExists, true));
        }
    }

    /**
     * @param CityDto[] $cityDtos
     * @return CategoryCollection $categoryCollection
     */
    private function getCategoryList($cityDtos)
    {
        //$cityDtos = array_slice($cityDtos, 0, 2);
        $categoryCollection = new CategoryCollection();
        foreach ($cityDtos as $cityDto) {
            $this->logInfo("Получение категорий для города: {$cityDto->getName()} [{$cityDto->getUrl()}]");
            try {
                $response = $this->httpClient->get($cityDto->getUrl());
                $this->parseCategories($categoryCollection, $cityDto, $response->getBody()->getContents());
            } catch (\Exception $e) {
                $this->logError("{$cityDto->getName()}[{$cityDto->getUrl()}] {$e->getMessage()}");

                continue;
            }
        }

        return $categoryCollection;
    }
}
