<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 12.09.17
 * Time: 15:34
 */

namespace console\models\operator;


use console\models\dtos\category\CategoryCollection;
use console\models\dtos\category\CategoryDto;
use console\models\dtos\category\CategoryTagDto;
use console\models\dtos\CityDto;
use console\models\operator\helpers\StringHelper;
use DiDom\Document;

class Weclever extends AbstractOperator
{
    const URL_BASE = 'http://weclever.ru';

    protected function buildUrlCity($string)
    {
        return $string;
    }

    protected function buildUrlCategory($cityDto, $string)
    {
        return $cityDto->getUrl() . $string;
    }

    protected function getCitiesList()
    {
        $response = $this->httpClient->get(static::URL_BASE);

        $document = Document::create($response->getBody()->getContents());
        $cities = $document->find('#weclever_city_select li a');

        $arCities = [];
        foreach ($cities as $city) {
            $cityDto = new CityDto();

            $cityName = trim($city->text());
            $cityDto->setName($cityName);

            $cityUrl = $this->buildUrlCity($city->getAttribute('href'));
            $cityDto->setUrl($cityUrl);
            $arCities[] = $cityDto;
        }

        return $arCities;
    }

    protected function parseCategories($categoryCollection, $cityDto, $html)
    {
        $document = Document::create($html);
        $as = $document->find('#weclever_menu_categories a');

        foreach ($as as $k => $a) {
            // первую пропускаем
            if ($k == 0) {
                continue;
            }

            $categoryDto = new CategoryDto();

            $name = StringHelper::toNorm($a->text());
            $href = $a->getAttribute('href');
            $href = $this->buildUrlCategory($cityDto, $href);

            $categoryDto->setName($name);
            $categoryDto->setUrl($href);
            $categoryDto->setCityIdLocal($cityDto->getCityIdLocal());

            try {
                $response = $this->httpClient->get($href);
            } catch (\Exception $e) {
                $this->logError($e->getMessage());
                continue;
            }

            $document = Document::create($response->getBody()->getContents());
            $els = $document->find('.weclever_subcat li a');
            if (!$els) {
                continue;
            }

            foreach ($els as $el) {
                $categoryTagDto = new CategoryTagDto();

                $name = StringHelper::toNorm($el->text());
                $categoryTagDto->setNameRu($name);

                $href = $el->getAttribute('href');
                $href = $this->buildUrlCategory($cityDto, $href);
                $categoryTagDto->setUrl($href);

                $categoryDto->setTag($categoryTagDto);
            }

            $categoryCollection->append($categoryDto);
        }
    }

}
