<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 12.09.17
 * Time: 12:36
 */

namespace console\models\operator;


use console\models\dtos\category\CategoryCollection;
use console\models\dtos\category\CategoryDto;
use console\models\dtos\category\CategoryTagDto;
use console\models\dtos\CityDto;
use DiDom\Document;

/**
 * Одинакого с Fun2mass
 *
 * Class Ladykupon
 * @package console\models\operator
 */
class Ladykupon extends AbstractOperator
{
    const URL_BASE = 'http://ladykupon.ru';
    const URL = 'http://api.ladykupon.ru/multiple/?cities=/cities';

    protected function buildUrlCity($string)
    {
        return "http://api.ladykupon.ru/multiple/?categories=/cities/$string/categories";
    }

    protected function buildUrlCategory($cityDto, $string)
    {
        return "http://api.ladykupon.ru/cities/{$cityDto->getAlias()}/categories/$string?order=new";
    }

    protected function getCitiesList()
    {
        $response = $this->httpClient->get(static::URL);
        $document = Document::create($response->getBody()->getContents());

        $json = json_decode($document->text(), true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            $this->logCritical('Битый JSON при парсинге городов');

            return false;
        }

        $cities = $json['cities'] ?? null;
        if (!$cities) {
            $this->logCritical('В JSON нет ключа cities');

            return false;
        }

        $arCities = [];
        foreach ($cities as $city) {
            $cityDto = new CityDto();
            $cityDto->setName($city['name']);
            $cityDto->setAlias($city['alias']);
            $url = $this->buildUrlCity($city['alias']);
            $cityDto->setUrl($url);
            $arCities[] = $cityDto;
        }

        return $arCities;
    }

    protected function parseCategories($categoryCollection, $cityDto, $html)
    {
        $document = Document::create($html);
        $json = json_decode($document->text(), true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            $this->logCritical('Битый JSON при парсинге категорий');

            return false;
        }

        $categories = $json['categories'] ?? null;
        if (!$categories) {
            $this->logCritical('В JSON нет ключа categories');

            return false;
        }

        foreach ($categories as $category) {
            $categoryDto = new CategoryDto();

            $name = trim($category['name']);
            $categoryDto->setName($name);

            $href = $this->buildUrlCategory($cityDto, $category['alias']);
            $categoryDto->setUrl($href);
            $categoryDto->setCityIdLocal($cityDto->getCityIdLocal());

            $childrens = $category['children'] ?? null;
            if ($childrens) {
                foreach ($childrens as $children) {
                    $categoryTagDto = new CategoryTagDto();
                    $categoryTagDto->setNameRu($children['name']);
                    $href = $this->buildUrlCategory($cityDto, $children['alias']);
                    $categoryTagDto->setUrl($href);
                    $categoryDto->setTag($categoryTagDto);
                }
            }
            $categoryCollection->append($categoryDto);
        }
    }

}
