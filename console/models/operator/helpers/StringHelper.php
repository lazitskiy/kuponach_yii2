<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 12.09.17
 * Time: 17:22
 */

namespace console\models\operator\helpers;


use common\models\entity\operator\Operator;

class StringHelper extends \common\helpers\StringHelper
{
    /**
     * 18+ 7 => 18+
     *       Для дома     128 => Для дома
     * @param string $string
     * @return string
     */
    public static function toNorm($string)
    {
        return trim(preg_replace('/\s+\d+.*/uis', '', $string));
    }

    const PATTERN_MAP = [
        'skidkabum' => '/img/',
    ];

    public static function absolutizeImgSrc(Operator $operator, $text)
    {
        $pattern = static::PATTERN_MAP[$operator->slug];
        if (!$pattern) {
            return $text;
        }

        $pattern = '#(<img.*?src=")(' . $pattern . ')#uis';
        $replace = '$1http://' . $operator->site . '/img/';

        $text = preg_replace($pattern, $replace, $text);

        return $text;
    }
}
