<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 10.09.17
 * Time: 14:19
 */

namespace console\models\operator;


use console\models\dtos\category\CategoryCollection;
use console\models\dtos\category\CategoryDto;
use console\models\dtos\category\CategoryTagDto;
use console\models\dtos\CityDto;
use DiDom\Document;

class Boombate extends AbstractOperator
{
    const URL_BASE = 'http://boombate.com';

    protected function buildUrl($string)
    {
        // TODO: Implement buildUrl() method.
    }


    protected function getCitiesList()
    {
        $response = $this->httpClient->get(static::URL_BASE);

        $document = Document::create($response->getBody()->getContents());
        $cities = $document->find('#city-list-all .city-list-item');
        $arCities = [];
        foreach ($cities as $city) {
            $cityDto = new CityDto();
            $cityDto->setName(trim($city->text()));
            $cityDto->setUrl(static::URL_BASE . $city->firstChild()->getAttribute('href'));
            $arCities[] = $cityDto;
        }

        return $arCities;
    }

    /**
     * @param CityDto[] $cityDtos
     * @return CategoryCollection $categoryCollection
     */
    protected function getCategoryList($cityDtos)
    {
        //$cityDtos = array_slice($cityDtos, 0, 2);
        $categoryCollection = new CategoryCollection();
        foreach ($cityDtos as $cityDto) {
            $this->logInfo('Получение категория для города: ' . $cityDto->getName());
            try {
                $response = $this->httpClient->get($cityDto->getUrl());
                $this->parseCategories($categoryCollection, $cityDto, $response->getBody()->getContents());
            } catch (\Exception $e) {
                $this->logError("{$cityDto->getName()}[{$cityDto->getUrl()}] {$e->getMessage()}");

                continue;
            }
        }

        return $categoryCollection;
    }


    const IGNORE_PATTERNS = '/BOOMagazine/uis';

    /**
     * @param CategoryCollection $categoryCollection
     * @param CityDto $cityDto
     * @param $html
     */
    private function parseCategories($categoryCollection, $cityDto, $html)
    {
        $document = Document::create($html);
        $lis = $document->find('.categories .categories-item');

        foreach ($lis as $li) {
            $parentCategoryEl = $li->first('.categories-title');
            $subCategoryEls = $li->find('.categories-sub .categories-link');
            if (!$subCategoryEls) {
                continue;
            }

            $parentCategoryName = trim($parentCategoryEl->text());
            if (preg_match(static::IGNORE_PATTERNS, $parentCategoryName)) {

                continue;
            }

            foreach ($subCategoryEls as $k => $subCategoryEl) {
                $categoryName = trim($subCategoryEl->text());
                $categoryName = preg_replace('/\d/', '', $categoryName);
                $categoryHref = trim($subCategoryEl->getAttribute('href'));

                $categoryDto = new CategoryDto();
                $categoryDto->setUrl(static::URL_BASE . $categoryHref);
                $categoryDto->setCityIdLocal($cityDto->getCityIdLocal());

                if ($k == 0) {
                    $categoryDto = new CategoryDto();
                    $categoryDto->setUrl(static::URL_BASE . $categoryHref);
                    $categoryDto->setCityIdLocal($cityDto->getCityIdLocal());
                    $categoryDto->setName($parentCategoryName);
                    $categoryCollection->append($categoryDto);
                } else {
                    $categoryTagDto = new CategoryTagDto();
                    $categoryTagDto->setNameRu($categoryName);
                    $categoryTagDto->setUrl(static::URL_BASE . $categoryHref);

                    /** @var CategoryDto $prev */
                    $prev = $categoryCollection->offsetGet($categoryCollection->count() - 1);
                    $prev->setTag($categoryTagDto);
                }
            }
        }
    }
}
