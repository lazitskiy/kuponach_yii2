<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 14.09.17
 * Time: 15:53
 */

namespace console\models\operator;


use console\models\dtos\category\CategoryCollection;
use console\models\dtos\category\CategoryDto;
use console\models\dtos\category\CategoryTagDto;
use console\models\dtos\CityDto;
use console\models\operator\helpers\StringHelper;
use DiDom\Document;

class Kupikupon extends AbstractOperator
{
    const URL_BASE = 'http://kupikupon.ru';

    protected function buildUrlCity($string)
    {
        return $string;
    }

    protected function buildUrlCategory($cityDto, $string)
    {
        return substr($cityDto->getUrl(), 0, -1) . $string;
    }

    protected function getCitiesList()
    {
        $response = $this->httpClient->get(static::URL_BASE);

        $document = Document::create($response->getBody()->getContents());
        $cities = $document->find('.popup-cities li a');

        $arCities = [];
        foreach ($cities as $city) {
            $cityDto = new CityDto();

            $cityName = StringHelper::toNorm($city->text());
            $cityDto->setName($cityName);

            $cityUrl = $this->buildUrlCity($city->getAttribute('href'));
            $cityDto->setUrl($cityUrl);
            $arCities[] = $cityDto;
        }

        return $arCities;
    }

    protected function parseCategories($categoryCollection, $cityDto, $html)
    {
        $document = Document::create($html);
        $lis = $document->find('.w-deals-menu .menu-item');

        foreach ($lis as $li) {
            $parentCategoryEl = $li->first('a.main_category');
            $subCategoryEls = $li->find('.menu-submenu a');
            if (!$subCategoryEls) {
                continue;
            }

            $categoryDto = new CategoryDto();

            $name = StringHelper::toNorm($parentCategoryEl->text());
            $categoryDto->setName($name);

            $href = trim($parentCategoryEl->getAttribute('href'));
            $href = $this->buildUrlCategory($cityDto, $href);
            $categoryDto->setUrl($href);
            $categoryDto->setCityIdLocal($cityDto->getCityIdLocal());

            foreach ($subCategoryEls as $k => $subCategoryEl) {

                $name = trim($subCategoryEl->firstChild()->text());

                $href = trim($subCategoryEl->getAttribute('href'));
                $href = $this->buildUrlCategory($cityDto, $href);

                $categoryTagDto = new CategoryTagDto();
                $categoryTagDto->setNameRu($name);
                $categoryTagDto->setUrl($href);

                $categoryDto->setTag($categoryTagDto);
            }

            $categoryCollection->append($categoryDto);
        }
    }

}
