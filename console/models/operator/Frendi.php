<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 13.09.17
 * Time: 21:33
 */

namespace console\models\operator;


use console\models\dtos\category\CategoryCollection;
use console\models\dtos\category\CategoryDto;
use console\models\dtos\category\CategoryTagDto;
use console\models\dtos\CityDto;
use console\models\operator\helpers\StringHelper;
use DiDom\Document;

class Frendi extends AbstractOperator
{
    const URL_BASE = 'http://frendi.ru';

    protected function buildUrlCity($string)
    {
        return static::URL_BASE . $string;
    }

    protected function buildUrlCategory($cityDto, $string)
    {
        return static::URL_BASE . $string;
    }

    protected function getCitiesList()
    {
        $response = $this->httpClient->get(static::URL_BASE);

        $document = Document::create($response->getBody()->getContents());
        $cities = $document->find('.top-cities li a');

        $arCities = [];
        foreach ($cities as $city) {
            $cityDto = new CityDto();

            $cityName = StringHelper::toNorm($city->text());
            $cityDto->setName($cityName);

            $cityUrl = $this->buildUrlCity($city->getAttribute('href'));
            $cityDto->setUrl($cityUrl);
            $arCities[] = $cityDto;
        }

        return $arCities;
    }

    protected function parseCategories($categoryCollection, $cityDto, $html)
    {
        $document = Document::create($html);
        $as = $document->find('.offers-tabs a');

        foreach ($as as $k => $a) {

            $href = $a->getAttribute('href');
            $href = $this->buildUrlCategory($cityDto, $href);
            try {
                $response = $this->httpClient->get($href);
            } catch (\Exception $e) {
                $this->logError($e->getMessage());
                continue;
            }

            $document = Document::create($response->getBody()->getContents());
            $divs = $document->find('.offers-subtabs .placement-category-item-wrapper');

            foreach ($divs as $div) {
                $a = $div->first('a');
                $categoryDto = new CategoryDto();

                $name = StringHelper::toNorm($a->text());
                $href = $a->getAttribute('href');
                $href = $this->buildUrlCategory($cityDto, $href);

                $categoryDto->setName($name);
                $categoryDto->setUrl($href);
                $categoryDto->setCityIdLocal($cityDto->getCityIdLocal());

                $subcats = $div->find('.offers-subcategories-item-link');
                if ($subcats) {
                    foreach ($subcats as $subcat) {
                        $categoryTagDto = new CategoryTagDto();

                        $name = StringHelper::toNorm($subcat->text());
                        $categoryTagDto->setNameRu($name);

                        $href = $subcat->getAttribute('href');
                        $href = $this->buildUrlCategory($cityDto, $href);
                        $categoryTagDto->setUrl($href);

                        $categoryDto->setTag($categoryTagDto);
                    }
                }
                $categoryCollection->append($categoryDto);
            }
        }
    }

}
