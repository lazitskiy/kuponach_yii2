<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 19.09.17
 * Time: 0:07
 */

namespace console\models\exceptions;


use yii\console\Exception;

class ImportException extends Exception
{
    /**
     * @return string the user-friendly name of this exception
     */
    public function getName()
    {
        return 'ImportException';
    }
}
