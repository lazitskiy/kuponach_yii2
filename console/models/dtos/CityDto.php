<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 11.09.17
 * Time: 0:02
 */

namespace console\models\dtos;


class CityDto
{
    protected $name;
    protected $url;
    protected $cityIdLocal;
    protected $alias;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getCityIdLocal()
    {
        return $this->cityIdLocal;
    }

    /**
     * @param mixed $cityIdLocal
     */
    public function setCityIdLocal($cityIdLocal)
    {
        $this->cityIdLocal = $cityIdLocal;
    }

    /**
     * @return mixed
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * @param mixed $alias
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;
    }
}
