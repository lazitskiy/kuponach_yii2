<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 11.09.17
 * Time: 4:30
 */

namespace console\models\dtos\category;


use yii\helpers\Inflector;

class CategoryTagDto
{
    protected $nameRu;
    protected $nameEn;
    protected $url;

    /**
     * @return mixed
     */
    public function getNameRu()
    {
        return $this->nameRu;
    }

    /**
     * @param mixed $nameRu
     */
    public function setNameRu($nameRu)
    {
        $this->nameRu = $nameRu;
        $this->nameEn = Inflector::slug($this->nameRu);
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }
}
