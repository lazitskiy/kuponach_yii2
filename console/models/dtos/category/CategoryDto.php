<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 11.09.17
 * Time: 0:02
 */

namespace console\models\dtos\category;


class CategoryDto
{
    protected $name;
    protected $url;
    protected $cityIdLocal;
    protected $children;
    protected $priority;
    protected $tags;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getCityIdLocal()
    {
        return $this->cityIdLocal;
    }

    /**
     * @param mixed $cityIdLocal
     */
    public function setCityIdLocal($cityIdLocal)
    {
        $this->cityIdLocal = $cityIdLocal;
    }

    /**
     * @return mixed
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param CategoryDto $categoryDto
     */
    public function setChildren($categoryDto)
    {
        $this->children[] = $categoryDto;
    }

    /**
     * @return mixed
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @param mixed $priority
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
    }

    /**
     * @return CategoryTagDto[]
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param CategoryTagDto $tag
     */
    public function setTag($tag)
    {
        $this->tags[] = $tag;
    }
}
