<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 11.09.17
 * Time: 2:21
 */

namespace console\models\dtos\category;


use console\models\dtos\base\BaseCollection;

class CategoryCollection extends BaseCollection
{
    public function getUnique()
    {
        $unique = [];
        /** @var CategoryDto $item */
        foreach ($this as $item) {
            $categoryKey = $item->getName();
            if (!isset($unique[$categoryKey])) {
                $unique[$categoryKey] = [];
            }

            if ($tags = $item->getTags()) {
                foreach ($tags as $tag) {
                    $tagKey = $tag->getNameRu();
                    if (!in_array($tagKey, $unique[$categoryKey])) {
                        $unique[$categoryKey][] = $tagKey;
                    }
                }
            }
        }

        return $unique;
    }
}
