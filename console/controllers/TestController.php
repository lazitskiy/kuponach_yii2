<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 16.09.17
 * Time: 22:05
 */

namespace console\controllers;

use RubtsovAV\YandexWordstatParser\Browser\ReactPhantomJs;
use RubtsovAV\YandexWordstatParser\Captcha\Image;
use RubtsovAV\YandexWordstatParser\CaptchaInterface;
use RubtsovAV\YandexWordstatParser\Parser;
use RubtsovAV\YandexWordstatParser\Query;
use RubtsovAV\YandexWordstatParser\YandexUser;

class TestController extends BaseController
{
    public function actionIndex()
    {
        $runtimeDir = \Yii::getAlias('@runtime');
        $yandexUser = new YandexUser('anikusha88', 'Ghbytcb100Nso', $runtimeDir);
        //$proxy = new HttpProxy('1.179.198.17', 8080);

        $browser = new ReactPhantomJs();
        //$browser->setProxy($proxy); // optional
        $browser->setTimeout(60);   // in seconds (120 by default)
        $browser->setCaptchaSolver(function ($captcha) use ($runtimeDir) {
            /** @var Image $captcha */
            $image = file_get_contents($captcha->getImageUri());
            file_put_contents($runtimeDir . '/captcha.jpg', $image);
            file_put_contents($runtimeDir . '/captchaAnswer.txt', '');

            echo "The captcha image was save to captcha.jpg. Write the answer in captchaAnswer.txt\n";
            $answer = '';
            while (!$answer) {
                $answer = file_get_contents($runtimeDir . '/captchaAnswer.txt');
                $answer = trim($answer);
                sleep(1);
            }
            echo "The captcha answer is '$answer'\n";
            $captcha->setAnswer($answer);

            return true;
        });

        $parser = new Parser($browser, $yandexUser);

        $query = new Query('купон бамбук');
        $result = $parser->query($query);

        vvd($result->toArray());
    }
}
