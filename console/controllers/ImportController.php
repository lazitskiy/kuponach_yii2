<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 14.09.17
 * Time: 21:35
 */

namespace console\controllers;

use common\models\entity\offer\OfferImage;
use console\components\Application;
use console\components\FileMutex;
use console\models\exceptions\ImportException;
use console\models\import\AbstractBaseOperator;
use console\models\import\boombate\Boombate;
use console\models\import\Megakupon;
use console\models\import\Skidkabum;
use console\models\operator\helpers\StringHelper;
use console\traits\ApplicationConsoleAwareTrait;
use console\traits\LoggerHelperAwareTrait;
use GuzzleHttp\Client;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Response;
use yii\base\ErrorException;
use yii\helpers\FileHelper;
use yii\log\FileTarget;

/**
 * Импортит XMLки
 *
 * Class ImportController
 * @package console\controllers
 */
class ImportController extends BaseController
{
    use ApplicationConsoleAwareTrait;
    use LoggerHelperAwareTrait;

    const OPERATORS = [
        'skidkabum' => [
            'className' => Skidkabum::class,
            'import' => [
                'url' => [
                    'default' => 'http://api.skidkabum.ru/actions/get/?request={"userID":14,"pass":"ku.yTBtpnlXB6","format":"json"}',
                    'xml' => 'http://api.skidkabum.ru/actions/get/?request={"userID":14,"pass":"ku.yTBtpnlXB6","cityID":100,"format":"xml"}',
                ],
                'downloadFileDir' => '@runtime/import',
            ],
        ],
        'megakupon' => [
            'className' => Megakupon::class,
            'import' => [
                'url' => [
                    'xml' => 'http://megakupon.ru/dealsxml?agr=default',
                ],
                'downloadFileDir' => '@runtime/import',
            ],
        ],
        'boombate' => [
            'className' => Boombate::class,
            'import' => [
                'url' => [
                    'xml' => 'https://boombate.com/files/kuponator.xml',
                ],
                'downloadFileDir' => '@runtime/import',
            ],
        ],
    ];

    /**
     * @var Application
     */
    protected $app;

    /**
     * @var FileMutex
     */
    protected $mutex;

    public function init()
    {
        $this->app = $this->getApplicationConsole();
        $this->mutex = $this->app->mutex;

        $this->addLogIndexTargets();
    }

    public function actionIndex()
    {

        $this->logIndex('Начало парсинга');

        $mutexFlag = 'import';
        if (!$this->mutex->acquire($mutexFlag)) {
            $this->logIndex("Процесс парсинга уже запущен, дождитесь его завершения.");

            return;
        }

        $this->logIndex('Поиск купонных операторов');

        foreach (static::OPERATORS as $slug => $operator) {
            $this->logIndex('Найден оператор ' . $slug);
            $this->actionImportOperator($slug);
        }
        $this->actionImportImages(1);

        \Yii::$app->runAction('system/process-address');
        \Yii::$app->runAction('system/sphinx-indexer');
        \Yii::$app->runAction('system/update-category-offer-count');
        \Yii::$app->runAction('system/check-offer-untagged');
        \Yii::$app->runAction('sitemap/generate');
    }

    public function actionImportOperator($slug)
    {
        $operatorClassName = static::OPERATORS[$slug]['className'] ?? null;
        $operatorImportParams = static::OPERATORS[$slug]['import'] ?? null;

        $this->addLogIndexTargets();
        $this->addLogTargets($slug);

        /** @var $operator AbstractBaseOperator */
        try {
            $operator = new $operatorClassName($slug, $operatorImportParams);
            $operator->import();
        } catch (ImportException $e) {
            $msg = "{$e->getMessage()} [{$e->getFile()}:{$e->getLine()}]";
            $this->logIndexCritical($msg);
        } catch (\Throwable $e) {
            $msg = "{$e->getMessage()} [{$e->getFile()}:{$e->getLine()}]";
            $this->logIndexCritical($msg);
        }

        if (!empty($msg)) {
            vvd($msg);
        }
    }

    public function actionImportImages($concurrency = 10, $force = false)
    {
        $httpClient = new Client([
            'timeout' => 180,
        ]);

        if ($force) {
            $offerImages = OfferImage::getRepository()->all();
        } else {
            $offerImages = OfferImage::getRepository()->getNotDownloaded();
        }
        if (!$offerImages) {
            return;
        }

        $requests = function () use ($httpClient, $offerImages) {
            foreach ($offerImages as $offerImage) {
                yield function () use ($httpClient, $offerImage) {
                    return $httpClient->getAsync($offerImage->operator_image);
                };
            }
        };

        $pool = new Pool($httpClient, $requests(), [
            'concurrency' => $concurrency,
            'fulfilled' => function (Response $response, $index) use ($offerImages) {
                if ($response->getStatusCode() == 200) {

                    /** @var OfferImage $offer */
                    $offerImage = $offerImages[$index];
                    echo "Image id {$offerImage->id}[{$offerImage->offer_id}] ok" . PHP_EOL;

                    $dir = \Yii::getAlias('@frontend/web/images/offers/' . $offerImage->offer_id);
                    FileHelper::createDirectory($dir);
                    $fileName = $dir . '/' . $offerImage->image;
                    file_put_contents($fileName, $response->getBody()->getContents());

                    $offerImage->status = OfferImage::STATUS_DONE;
                    $offerImage->save();
                }
            },
            'rejected' => function (\Exception $reason, $index) use ($offerImages) {
                /** @var OfferImage $offer */
                $offerImage = $offerImages[$index];
                echo "Image id {$offerImage->id}[{$offerImage->offer_id}] error [{$reason->getMessage()}]" . PHP_EOL;
                $offerImage->status = OfferImage::STATUS_ERROR;
                $offerImage->reason = $reason->getMessage();
                $offerImage->save();
            },
        ]);

        $promise = $pool->promise();
        $promise->wait();

    }

    private function addLogIndexTargets()
    {
        $indexCategory = false;
        $indexCriticalCategory = false;

        $targets = \Yii::getLogger()->dispatcher->targets;
        if ($targets) {
            foreach ($targets as $target) {
                if (in_array('index', $target->categories)) {
                    $indexCategory = true;
                }
                if (in_array('indexCritical', $target->categories)) {
                    $indexCriticalCategory = true;
                }
            }
        }
        $now = new \DateTime('now', new \DateTimeZone('Europe/Moscow'));
        if (!$indexCategory) {
            $targets[] = \Yii::createObject([
                'class' => FileTarget::class,
                'exportInterval' => 1,
                'categories' => ['index'],
                'logFile' => '@runtime/logs/import/' . $now->format('Y-m-d') . '/index.txt',
                'logVars' => [],
            ]);
        }
        if (!$indexCriticalCategory) {
            $targets[] = \Yii::createObject([
                'class' => FileTarget::class,
                'exportInterval' => 1,
                'categories' => ['indexCritical'],
                'logFile' => '@runtime/logs/import/' . $now->format('Y-m-d') . '/index_critical.txt',
                'logVars' => [],
            ]);
        }
        if ($targets) {
            \Yii::getLogger()->dispatcher->targets = $targets;
        }
    }

    /**
     * @param $operatorName string
     */
    private function addLogTargets($operatorName)
    {
        $now = new \DateTime('now', new \DateTimeZone('Europe/Moscow'));
        $targets = \Yii::getLogger()->dispatcher->targets;

        foreach (['info', 'error', 'critical'] as $errorLevel) {
            $targets[] = \Yii::createObject([
                'class' => FileTarget::class,
                'exportInterval' => 1,
                'categories' => [$errorLevel],
                'logFile' => '@runtime/logs/import/' . $now->format('Y-m-d') . '/' . $operatorName . '_' . $errorLevel . '.txt',
                'logVars' => [],
            ]);
        }
        \Yii::getLogger()->dispatcher->targets = $targets;
    }

}
