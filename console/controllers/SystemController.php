<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 29.09.17
 * Time: 2:10
 */

namespace console\controllers;


use common\models\entity\city\City;
use common\models\entity\company\CompanyAddress;
use common\models\entity\offer\Offer;
use common\models\entity\offer\OfferSphinx;
use common\models\entity\tag\Tag;
use common\services\email\EmailService;
use common\services\GeoService;
use common\services\TagMatchService;
use common\traits\base\DadataAwareTrait;
use common\traits\base\EmailServiceAwareTrait;
use common\traits\GeoServiceAwareTrait;
use Dadata\Client;
use skeeks\yii2\dadataSuggestApi\DadataSuggestApi;
use yii\console\Controller;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\sphinx\Query;

/**
 * Class SystemController
 * @package console\controllers
 *
 */
class SystemController extends Controller
{
    use EmailServiceAwareTrait;
    use DadataAwareTrait;
    use GeoServiceAwareTrait;

    /**
     * @var EmailService
     */
    protected $emailService;

    /**
     * @var DadataSuggestApi
     */
    protected $dadata;

    /**
     * @var GeoService
     */
    protected $geoService;

    public function init()
    {
        parent::init();

        $this->emailService = $this->getEmailService();
        $this->dadata = $this->getDadata();
        $this->geoService = $this->getGeoService();
    }

    /**
     * Переиндексация всех индексов
     */
    public function actionSphinxIndexer()
    {
        $configPath = \Yii::getAlias('@common/config/sphinx.conf');

        $command = "ps aux | grep [s]earchd";
        $output = shell_exec($command);

        /**
         * Если уже запущен
         */
        if (preg_match('/searchd/uis', $output)) {

            $command = "indexer --config $configPath --all --rotate";
            $output = shell_exec($command);
            $this->stdout(VarDumper::dump($output) . PHP_EOL);

            return 0;
        }

        /**
         * Иначе сами стартуем
         */
        $command = "indexer --config $configPath --all";
        shell_exec($command);

        $command = "searchd --config $configPath";
        $this->stdout("$command\n");
        $output = shell_exec($command);
        $this->stdout(VarDumper::dump($output) . PHP_EOL);

        return 0;
    }

    /**
     * Обновляем колво купонов у категорий
     */
    public function actionUpdateCategoryOfferCount()
    {
        $now = (new \DateTime());

        $result = (new Query())
            ->from(OfferSphinx::indexName())
            ->addOptions([
                'max_matches' => 1000000,
            ])
            ->limit(1000000)
            ->select(['groupby() tag_id', 'COUNT(*) as offer_count'])
            ->where(['>=', 'date_end', $now->getTimestamp()])
            ->groupBy('tag_ids')
            ->all();
        $offerCounts = ArrayHelper::map($result, 'tag_id', 'offer_count');

        Tag::updateAll(
            ['offer_count' => 0]
        );

        foreach ($offerCounts as $tadIg => $offerCount) {
            $message = "Updating tag[{$tadIg}], count = $offerCount";
            $this->stdout($message . PHP_EOL);
            Tag::updateAll(
                ['offer_count' => $offerCount],
                ['id' => $tadIg]
            );
        }
    }

    public function actionCheckOfferUntagged()
    {
        $sql = "
          SELECT * FROM offer
            WHERE NOT EXISTS (
                SELECT offer_id FROM offer_tag WHERE offer_id = offer.id
            )
        ";

        $offers = Offer::findBySql($sql)->all();
        if (!$offers) {
            return 0;
        }

        $this->emailService->sendOfferUntagged($offers);

    }

    /**
     * Эта штука берет все теги, получает их правила и прицепляет к офферам теги
     *
     * @param string $all
     * @param string $deleteOld
     */
    public function actionLinkOffersTag($all = null, $deleteOld = null)
    {
        $tagMatchService = new TagMatchService();

        $offers = Offer::getRepository();
        if (!$all) {
            $now = (new \DateTime())->format('Y-m-d H:i:s');
            $offers->where(['>', 'date_end', $now]);
        }
        $offers = $offers->all();
        $offerCount = count($offers);

        $deleteOld = !empty($deleteOld);
        Console::startProgress(0, $offerCount, 'Offer process: ', false);
        $i = 1;
        foreach ($offers as $offer) {
            $tagIds = $tagMatchService->getMatchedTagIds($offer);
            if ($tagIds) {
                $offer->setTags($tagIds, $deleteOld);
            }
            Console::updateProgress($i, $offerCount);
            $i++;
        }
        Console::endProgress("done." . PHP_EOL);
    }

    /**
     * Рукописные адреса приводит через дадату к стандартному
     */
    public function actionProcessAddress()
    {
        $this->cleanNewAddress();
    }

    private function cleanNewAddress()
    {
        $client = new Client(new \GuzzleHttp\Client(), [
            'token' => 'e39e431365f7513da6f8bd16d5c8c23f0a0f4394',
            'secret' => '8e2e97724188d769e4537da755f5e0b91f75bb14',
        ]);

        $addresses = CompanyAddress::find()->where(['dadated_at' => null])->all();

        /** @var CompanyAddress $address */
        foreach ($addresses as $address) {
            $response = $client->cleanAddress($address->address);

            $addressJson = Json::encode($response);
            $address->address_data = $addressJson;
            $address->dadated_at = date('Y-m-d H:i:s');

            $this->setAddressData($address);

            $address->save();
        }
    }

    public function actionHandleAddress($all = false)
    {
        $q = CompanyAddress::find();
        /** @var CompanyAddress[] $addresses */
        $addresses = $q->all();

        $addressCount = count($addresses);
        Console::startProgress(0, $addressCount, 'Address process: ', false);
        $i = 1;

        foreach ($addresses as $address) {
            $this->setAddressData($address);
            $address->save();

            Console::updateProgress($i, $addressCount);
            $i++;
        }
        Console::endProgress("done." . PHP_EOL);
    }

    private function setAddressData(CompanyAddress $address)
    {
        $data = Json::decode($address->address_data);

        if (in_array($address->address, ['Вся Россия', 'Россия', 'РФ'])) {
            $address->country_all = 1;

            return;
        }
        /**
         * В дадате для Мск и Питера город находится в $data['region']
         */
        $cityName = $data['city'] ?? $data['region'] ?? null;
        $cityArea = $data['city_area'] ?? null;
        $cityDistrict = $data['city_district'] ?? null;
        $cityDistrictType = $data['city_district_type_full'] ?? null;
        $cityStreet = $data['street'] ?? null;
        $cityStreetType = $data['street_type_full'] ?? null;
        $metros = $data['metro'] ?? [];

        $city = City::getRepository()->getByName($cityName);
        if (!$city) {
            $city = new City();
            $city->name = $cityName;
            $city->is_active = 0;
            $city->save();
            $this->emailService->sendCityNotRecognized($data);
        }

        list($cityAreaId, $cityDistrictId, $cityStreetId, $metroIds) = $this->getGeoService()->mapDadataToLocalIds($city, $cityArea, $cityDistrict, $cityDistrictType, $cityStreet, $cityStreetType, $metros);

        $address->city_area_id = $cityAreaId;
        $address->city_district_id = $cityDistrictId;
        $address->city_street_id = $cityStreetId;
        $address->city_metro_ids = Json::encode($metroIds);
        if (!$cityStreetId) {
            $address->city_all = 1;
        }
    }

    public function actionTest()
    {
        $addresses = CompanyAddress::findAll([513]);

        foreach ($addresses as $address) {
            $data = Json::decode($address->address_data);

            /**
             * В дадате для Мск и Питера город находится в $data['region']
             */
            $cityName = $data['city'] ?? $data['region'] ?? null;
            $cityArea = $data['city_area'] ?? null;
            $cityDistrict = $data['city_district'] ?? null;
            $cityStreet = $data['street'] ?? null;

            $city = City::getRepository()->getByName($cityName);
            if (!$city) {
                $this->emailService->sendCityNotRecognized($data);

                return;
            }

            list($cityAreaId, $cityDistrictId, $cityStreetId) = $this->getGeoService()->mapDadataToLocalIds($city, $cityArea, $cityDistrict, $cityStreet);

            $address->city_area_id = $cityStreetId;
            $address->city_district_id = $cityDistrictId;
            $address->city_street_id = $cityStreetId;

            $address->save();
        }
        vvd('ok');

        $tagIds = (new TagMatchService())->getMatchedTagIds($offer);
        vvd($tagIds);
    }
}
