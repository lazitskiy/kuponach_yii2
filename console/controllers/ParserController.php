<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 10.09.17
 * Time: 13:07
 */

namespace console\controllers;


use console\components\Application;
use console\models\operator\AbstractOperator;
use console\models\operator\Alkupone;
use console\models\operator\Biglion;
use console\models\operator\Boombate;
use console\models\operator\Frendi;
use console\models\operator\Fun2mass;
use console\models\operator\Glavskidka;
use console\models\operator\Kupibonus;
use console\models\operator\Kupikupon;
use console\models\operator\Kuponmania;
use console\models\operator\Ladykupon;
use console\models\operator\Myfant;
use console\models\operator\Vigoda;
use console\models\operator\Weclever;
use console\traits\ApplicationConsoleAwareTrait;
use console\traits\LoggerHelperAwareTrait;
use yii\log\FileTarget;
use yii\mutex\Mutex;

class ParserController extends BaseController
{
    use ApplicationConsoleAwareTrait;
    use LoggerHelperAwareTrait;

    const OPERATORS = [
        'boombate' => Boombate::class, //edit pls
        'fun2mass' => Fun2mass::class, //done
        'myfant' => Myfant::class, //done
        'kuponmania' => Kuponmania::class, //done
        'kupibonus' => Kupibonus::class, //done
        'glavskidka' => Glavskidka::class,
        'ladykupon' => Ladykupon::class, //done
        'alkupone' => Alkupone::class, //done
        'weclever' => Weclever::class, //done
        'frendi' => Frendi::class, //done
        'biglion' => Biglion::class, //edit pls
        'vigoda' => Vigoda::class,//edit pls
        'kupikupon' => Kupikupon::class, //done
    ];

    /**
     * @var Application
     */
    protected $app;

    /**
     * @var Mutex
     */
    protected $mutex;

    public function init()
    {
        $this->app = $this->getApplicationConsole();
        $this->mutex = $this->app->mutex;

        $targets[] = \Yii::createObject([
            'class' => FileTarget::class,
            'exportInterval' => 1,
            'categories' => ['index'],
            'logFile' => '@runtime/logs/' . date('Y-m-d') . '/index.txt',
            'logVars' => [],
        ]);

        foreach (static::OPERATORS as $systemName => $operator) {
            $targets[] = \Yii::createObject([
                'class' => FileTarget::class,
                'exportInterval' => 1,
                'categories' => ['info'],
                'logFile' => '@runtime/logs/' . date('Y-m-d') . '/' . $systemName . '.txt',
                'logVars' => [],
            ]);
            $targets[] = \Yii::createObject([
                'class' => FileTarget::class,
                'exportInterval' => 1,
                'categories' => ['error'],
                'logFile' => '@runtime/logs/' . date('Y-m-d') . '/' . $systemName . '_error.txt',
                'logVars' => [],
            ]);
            $targets[] = \Yii::createObject([
                'class' => FileTarget::class,
                'exportInterval' => 1,
                'categories' => ['critical'],
                'logFile' => '@runtime/logs/' . date('Y-m-d') . '/' . $systemName . '_critical.txt',
                'logVars' => [],
            ]);
        }

        \Yii::getLogger()->dispatcher->targets = $targets;
    }

    public function actionParse()
    {
        $this->logIndex('Начало парсинга');

        $mutexFlag = 'import';
        if (!$this->mutex->acquire($mutexFlag)) {
            $this->logIndex("Процесс парсинга уже запущен, дождитесь его завершения.");

            return;
        }

        $this->logIndex('Поиск купонных операторов');

        foreach (static::OPERATORS as $systemName => $operator) {

            $command = './yii parser/parse-operator ' . $systemName;
            $this->logIndex('Найден оператор ' . $systemName . ' [' . $command . ']');
            exec($command);
        }
    }

    public function actionParseOperator($operatorName)
    {
        $operatorClass = static::OPERATORS[$operatorName] ?? null;

        if (!class_exists($operatorClass)) {
            $this->logError('Класс для оператора не найден [' . $operatorName . ']');

            return 0;
        }
        /** @var AbstractOperator $operator */
        $operator = new $operatorClass($operatorName);
        $operator->parse();
    }

}
