<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 09.10.17
 * Time: 22:45
 */

namespace console\controllers;


use common\helpers\Inflector;
use common\models\entity\seo\Seo;
use common\models\entity\tag\Tag;
use common\services\internal\WordstatService;
use GuzzleHttp\Client;
use yii\helpers\Console;
use yii\helpers\Json;

class OnetimeController extends BaseController
{
    /**
     *  Slug сделать транслитом
     */
    public function actionTransliterateTag()
    {
        foreach (Tag::getRepository()->all() as $tag) {
            Tag::updateAll(
                ['slug' => Inflector::slug($tag->name)],
                ['id' => $tag->id]
            );
        }
    }

    public function actionParseSeo()
    {
        $client = new Client();
        /** @var Tag[] $tags */
        $tags = Tag::find()->orderBy(['id' => SORT_ASC])->all();

        $tagCount = count($tags);

        Console::startProgress(0, $tagCount, 'Tag process: ', false);
        $i = 1;

        foreach ($tags as $tag) {
            Console::updateProgress($i, $tagCount);
            $i++;

            if ($tag->seo) {
                continue;
            }

            $words = ["купон {$tag->name}", "скидка {$tag->name}", "акция {$tag->name}"];

            // 1. Получаем вордстат
            $runtimeDir = \Yii::getAlias('@console/runtime/phantomjs');
            $wordstatService = new WordstatService($runtimeDir);

            $seoStat = [];
            foreach ($words as $word) {
                $seoStat[$word]['wordstat'] = $wordstatService->getStat($word);
                sleep(rand(2, 5));
            }

            // 2. Лезем https://arsenkin.ru/tools/check-top/
            // 3. Лезем https://arsenkin.ru/tools/check-h/

            $response = $client->post('https://arsenkin.ru/tools/check-top/', [
                'form_params' => [
                    'yes' => 1,
                    'keys' => implode("\n", $words),
                    'city' => 213,
                    'depth' => 10,
                ],
            ]);

            $data = $response->getBody()->getContents();

            if (preg_match_all('/<table.*?>(.*?)<\/table>/uis', $data, $matches)) {
                $tables = $matches[0];
                foreach ($tables as $k => $table) {
                    $keyword = $words[$k];

                    if (preg_match_all('/<tr.*?td>(\d)?<.*?(#.*?);.*?href=["\'](.*?)["\'].*?tr>/uis', $table, $matches, PREG_SET_ORDER)) {
                        foreach ($matches as $match) {
                            $url = $match[3];
                            $url = preg_replace('/(search_)(\%.*)/', '$1', $url);
                            $url = urldecode($url);

                            $seoStat[$keyword]['info'][$url]['top'] = [
                                'position' => $match[1],
                                'color' => $match[2],
                                'url' => $url,
                            ];
                        }

                        $response = $client->post('https://arsenkin.ru/tools/check-h/index.php', [
                            'form_params' => [
                                'a_mode' => 'getThis',
                                'ajax' => 'Y',
                                'key' => $keyword,
                                'urls' => '',
                                'mode' => 'false',
                                'city' => 213,
                                'depth' => 5,
                                'pause' => 100,
                                'spektr' => 'false',
                                'metacheck' => 'true',
                            ],
                        ]);
                        $data = $response->getBody()->getContents();

                        if (preg_match_all('/<tr.*?(.*?href=["\'](.*?)["\']).*?title(.*?)<\/span.*?description(.*?)<\/span.*?<\/tr>/uis', $data, $matches, PREG_SET_ORDER)) {
                            foreach ($matches as $match) {
                                $url = $match[2];
                                $url = preg_replace('/(search_)(\%.*)/', '$1', $url);
                                $url = urldecode($url);

                                $title = strip_tags($match[3]);
                                $title = mb_convert_encoding($title, "UTF-8", mb_detect_encoding($title));
                                $title = preg_replace('/[\x{10000}-\x{10FFFF}]/u', '', $title);

                                $description = strip_tags($match[4]);
                                $description = mb_convert_encoding($description, "UTF-8", mb_detect_encoding($description));
                                $description = preg_replace('/[\x{10000}-\x{10FFFF}]/u', '', $description);

                                $seoStat[$keyword]['info'][$url]['meta'] = [
                                    'title' => $title,
                                    'description' => $description,
                                    'url' => $url,
                                ];
                            }
                        }
                    }
                }
            }

            $seo = new Seo();
            $seo->seo_stat = Json::encode($seoStat);
            $seo->save();

            Tag::updateAll(
                ['seo_id' => $seo->id],
                ['id' => $tag->id]
            );
        }

        sleep(3);
        Console::endProgress("done." . PHP_EOL);
    }

}
