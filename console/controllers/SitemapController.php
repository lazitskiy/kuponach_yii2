<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 29.10.17
 * Time: 14:58
 */

namespace console\controllers;


use common\models\entity\city\City;
use common\models\entity\company\CompanyAddressCityArea;
use common\models\entity\company\CompanyAddressCityDistrict;
use common\models\entity\company\CompanyAddressCityMetro;
use common\models\entity\company\CompanyAddressCityStreet;
use common\models\entity\offer\Offer;
use common\models\entity\offer\OfferSphinx;
use common\models\entity\tag\Tag;
use console\services\SitemapGeneratorService;
use samdark\sitemap\Index;
use samdark\sitemap\Sitemap;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;
use yii\helpers\FileHelper;

class SitemapController extends BaseController
{
    public $withRobots;

    public function options($actionID)
    {
        return array_merge(parent::options($actionID), ['withRobots']);
    }

    public function actionIndex()
    {
        echo 'run sitemap/generate';
    }

    const MAX_URLS_COUNT = 50000;

    public function actionGenerate()
    {
        /** @var City[] $cities */
        $cities = City::findAll(['is_active' => 1]);
        $cityCount = count($cities);
        /** @var Tag[] $tags */
        $tags = Tag::find()->where(['>', 'id', 0])->all();

        Console::startProgress(0, $cityCount, 'City sitemap process: ', false);
        $i = 1;

        foreach ($cities as $city) {
            $areas = CompanyAddressCityArea::getRepository()->getByCityId($city->id);
            $districts = CompanyAddressCityDistrict::getRepository()->getByCityId($city->id);
            $streets = CompanyAddressCityStreet::getRepository()->getByCityId($city->id);
            $metros = CompanyAddressCityMetro::getRepository()->getByCityId($city->id);

            $siteUrl = 'https://' . ($city->slug ? $city->slug . '.' : '') . 'kuponach.ru';

            $path = \Yii::getAlias("@frontend/web/sitemaps/{$city->slug}/");
            FileHelper::createDirectory($path);

            $sitemap = new Sitemap($path . 'sitemap.xml');
            $sitemap->setBufferSize(static::MAX_URLS_COUNT);
            //$sitemap->setUseGzip(true);
            $sitemap->addItem("$siteUrl", time(), 'always', '1');

            foreach ($tags as $tag) {
                $sitemap->addItem($siteUrl . $tag->getUrl(), time(), 'daily', '0.9');
                $sitemap->addItem($siteUrl . $tag->getUrl() . '/nedorogo', time(), 'daily', '0.9');
                $sitemap->addItem($siteUrl . $tag->getUrl() . '/besplatno', time(), 'daily', '0.9');

                foreach ($areas as $area) {
                    $sitemap->addItem("{$siteUrl}{$tag->getUrl()}/nedorogo/{$area->slug}-o", time(), 'daily', '0.9');
                    $sitemap->addItem("{$siteUrl}{$tag->getUrl()}/besplatno/{$area->slug}-o", time(), 'daily', '0.9');
                    $sitemap->addItem("{$siteUrl}/{$area->slug}-o/nedorogo", time(), 'daily', '0.9');
                    $sitemap->addItem("{$siteUrl}/{$area->slug}-o/besplatno", time(), 'daily', '0.9');
                    $sitemap->addItem("{$siteUrl}{$tag->getUrl()}/{$area->slug}-o", time(), 'daily', '0.9');
                    $sitemap->addItem("{$siteUrl}/{$area->slug}-o", time(), 'daily', '0.9');
                }
                foreach ($districts as $district) {
                    $sitemap->addItem("{$siteUrl}{$tag->getUrl()}/nedorogo/rayon-{$district->slug}", time(), 'daily', '0.9');
                    $sitemap->addItem("{$siteUrl}{$tag->getUrl()}/besplatno/rayon-{$district->slug}", time(), 'daily', '0.9');
                    $sitemap->addItem("{$siteUrl}/rayon-{$district->slug}/nedorogo", time(), 'daily', '0.9');
                    $sitemap->addItem("{$siteUrl}/rayon-{$district->slug}/besplatno", time(), 'daily', '0.9');
                    $sitemap->addItem("{$siteUrl}{$tag->getUrl()}/rayon-{$district->slug}", time(), 'daily', '0.9');
                    $sitemap->addItem("{$siteUrl}/rayon-{$district->slug}", time(), 'daily', '0.9');
                }
                foreach ($streets as $street) {
                    /*$generator->addUrl("{$siteUrl}{$tag->getUrl()}/nedorogo/ulitsa-{$street->slug}", date('Y-m-d'), 'daily', '0.9');
                    $generator->addUrl("{$siteUrl}{$tag->getUrl()}/besplatno/ulitsa-{$street->slug}", date('Y-m-d'), 'daily', '0.9');
                    $generator->addUrl("{$siteUrl}/ulitsa-{$street->slug}/nedorogo", date('Y-m-d'), 'daily', '0.9');
                    $generator->addUrl("{$siteUrl}/ulitsa-{$street->slug}/besplatno", date('Y-m-d'), 'daily', '0.9');
                    $generator->addUrl("{$siteUrl}{$tag->getUrl()}/ulitsa-{$street->slug}", date('Y-m-d'), 'daily', '0.9');
                    $generator->addUrl("{$siteUrl}/ulitsa-{$street->slug}", date('Y-m-d'), 'daily', '0.9');*/
                }
                foreach ($metros as $metro) {
                    $sitemap->addItem("{$siteUrl}{$tag->getUrl()}/nedorogo/metro-{$metro->slug}", time(), 'daily', '0.9');
                    $sitemap->addItem("{$siteUrl}{$tag->getUrl()}/besplatno/metro-{$metro->slug}", time(), 'daily', '0.9');
                    $sitemap->addItem("{$siteUrl}/metro-{$metro->slug}/nedorogo", time(), 'daily', '0.9');
                    $sitemap->addItem("{$siteUrl}/metro-{$metro->slug}/besplatno", time(), 'daily', '0.9');
                    $sitemap->addItem("{$siteUrl}{$tag->getUrl()}/metro-{$metro->slug}", time(), 'daily', '0.9');
                    $sitemap->addItem("{$siteUrl}/metro-{$metro->slug}", time(), 'daily', '0.9');
                }
            }

            $query = OfferSphinx::getRepository()->findByCityId($city->id);
            foreach ($query->batch(1000) as $batch) {
                $offerIds = ArrayHelper::getColumn($batch, 'id');
                $offers = Offer::find()
                    ->select(['slug'])
                    ->where(['id' => $offerIds])
                    ->indexBy('id')
                    ->column();
                if ($offers) {
                    foreach ($offers as $slug) {
                        $sitemap->addItem("$siteUrl/kupon-na/" . $slug, time(), 'daily', '0.8');
                    }
                }
            }

            $sitemapFileUrls = $sitemap->getSitemapUrls($siteUrl . '/');
            if (count($sitemapFileUrls) == 1) {
                $sitemap->write();
            } else {
                $sitemap->write();

                rename($path . 'sitemap.xml', $path . 'sitemap_1.xml');
                $index = new Index($path . 'sitemap.xml');
                // add URLs
                foreach ($sitemapFileUrls as $sitemapUrl) {
                    $sitemapUrl = str_replace('sitemap.xml', 'sitemap_1.xml', $sitemapUrl);
                    $index->addSitemap($sitemapUrl);
                }
                $index->write();
            }

            if ($this->withRobots) {
                $this->generateRobots($siteUrl, "$siteUrl/sitemap.xml", $path);
            }
            Console::updateProgress($i, $cityCount);
            $i++;
        }

        Console::endProgress("done." . PHP_EOL);
    }

    public function generateRobots($host, $sitemap, $path)
    {
        $robots = <<<ROBOTS
User-agent: Yandex
Disallow:*&
Disallow:/search=
Disallow:/*page=
Disallow:/out
Disallow:/suggest
Disallow:/*iframe
Disallow:*utm_source=*
Disallow:/*.php
Host: $host

User-agent: *
Disallow:*&
Disallow:/search=
Disallow:/*page=
Disallow:/out
Disallow:/suggest
Disallow:/*iframe
Disallow:*utm_source=*
Disallow:/*.php

Sitemap: $sitemap
ROBOTS;

        $path = $path . 'robots.txt';
        file_put_contents($path, $robots);
    }
}
