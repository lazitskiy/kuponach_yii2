<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 10.09.17
 * Time: 16:10
 */

namespace console\traits;


trait LoggerHelperAwareTrait
{
    protected function logIndex($message)
    {
        \Yii::info($message, 'index');
    }

    protected function logIndexCritical($message)
    {
        \Yii::info($message, 'indexCritical');
    }

    protected function logInfo($message)
    {
        \Yii::info($message, 'info');
    }

    protected function logError($message)
    {
        \Yii::error($message, 'error');
    }

    protected function logCritical($message)
    {
        \Yii::error($message, 'critical');
    }

}
