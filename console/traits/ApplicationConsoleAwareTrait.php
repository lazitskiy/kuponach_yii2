<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 10.09.17
 * Time: 13:51
 */

namespace console\traits;


use console\components\Application;

trait ApplicationConsoleAwareTrait
{
    /**
     * @return \yii\console\Application|\yii\web\Application|Application
     */
    public function getApplicationConsole()
    {
        return \Yii::$app;
    }
}
