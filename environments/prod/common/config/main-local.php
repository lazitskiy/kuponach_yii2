<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=kuponach',
            'username' => 'root',
            'password' => 'BTG0vCXenfen',
            'charset' => 'utf8',
        ],
        'sphinx' => [
            'class' => \yii\sphinx\Connection::class,
            'dsn' => 'mysql:host=127.0.0.1;port=9307;',
            'username' => 'root',
            'password' => 'BTG0vCXenfen',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
        ],
    ],
];
